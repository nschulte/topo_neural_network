import numpy as np

TwoBody_sanity_config = {
    "TwoBody_M" : {
        "EDGES" : [0,4000],
        "xlabel" : r"Mass [MeV$/c$]",
        "bins" : 45
  },
    "TwoBody_MM" : {
        "EDGES" : [0,4000],
        "xlabel" : r"Measured Mass [MeV$/c$]",
        "bins" : 45
  },
    "TwoBody_PT" : {
        "EDGES" : [0,15000],
        "xlabel" : r"Beauty $p_{T}$ [MeV$/c$]",
        "bins" : 45
  },
    "TwoBody_P" : {
        "EDGES" : [0,300000],
        "xlabel" : r"Beauty $p_{T}$ [MeV$/c$]",
        "bins" : 45
  },
    "TwoBody_ENDVERTEX_DOCAMAX" : {
        "EDGES" : [0,0.5],
        "xlabel" : r"DOCA Endvertex [mm]",
        "bins" : 45
  },
    "TwoBody_IPCHI2_OWNPV" : {
        "EDGES" : [-5,12],
        "xlabel" : r"$\chi^2_{IP}$",
        "bins" : 45
  },
    "TwoBody_FDCHI2_OWNPV" : {
        "EDGES" : [2,14],
        "xlabel" : r"$\chi^2_{FD}$",
        "bins" : 45
  },
    "TwoBody_OWNPV_CHI2" : {
        "EDGES" : [0,8],
        "xlabel" : r"$\chi^2_{OWNPV}$",
        "bins" : 45
  },
    "TwoBody_ENDVERTEX_CHI2" : {
        "EDGES" : [-10,4],
        "xlabel" : r"$\chi^2_{ENDVERTEX}$",
        "bins" : 45
  },
    "Track1_P" : {
        "EDGES" : [0,350000],
        "xlabel" : r"Track1 $p$ [MeV$/c$]",
        "bins" : 45
  },
    "Track1_PT" : {
        "EDGES" : [0,20000],
        "xlabel" : r"Track1 $p_{T}$ [MeV$/c$]",
        "bins" : 45
  },
    "Track1_IPCHI2_OWNPV" : {
        "EDGES" : [0,12],
        "xlabel" : r"$\chi^2_{IP} Track1$",
        "bins" : 45
  },
    "Track1_ORIVX_DOCAMAX" : {
        "EDGES" : [0,0.5],
        "xlabel" : r"Track1 DOCA Original Vertex [mm]",
        "bins" : 45
  },
}

ThreeBody_sanity_config = {
    "ThreeBody_M" : {
        "EDGES" : [0,4000],
        "xlabel" : r"Mass [MeV$/c$]",
        "bins" : 45
  },
    "ThreeBody_MM" : {
        "EDGES" : [0,4000],
        "xlabel" : r"Measured Mass [MeV$/c$]",
        "bins" : 45
  },
    "ThreeBody_PT" : {
        "EDGES" : [0,20000],
        "xlabel" : r"Beauty $p_{T}$ [MeV$/c$]",
        "bins" : 45
  },
    "ThreeBody_P" : {
        "EDGES" : [0,350000],
        "xlabel" : r"Beauty $p_{T}$ [MeV$/c$]",
        "bins" : 45
  },
    "ThreeBody_ENDVERTEX_DOCAMAX" : {
        "EDGES" : [0,0.5],
        "xlabel" : r"DOCA Endvertex [mm]",
        "bins" : 45
  },
    "ThreeBody_IPCHI2_OWNPV" : {
        "EDGES" : [-5,12],
        "xlabel" : r"$\chi^2_{IP}$",
        "bins" : 45
  },
    "ThreeBody_FDCHI2_OWNPV" : {
        "EDGES" : [2,14],
        "xlabel" : r"$\chi^2_{FD}$",
        "bins" : 45
  },
    "ThreeBody_OWNPV_CHI2" : {
        "EDGES" : [0,8],
        "xlabel" : r"$\chi^2_{OWNPV}$",
        "bins" : 45
  },
    "ThreeBody_ENDVERTEX_CHI2" : {
        "EDGES" : [-8,8],
        "xlabel" : r"$\chi^2_{ENDVERTEX}$",
        "bins" : 45
  },
    "Track1_P" : {
        "EDGES" : [0,350000],
        "xlabel" : r"Track1 $p$ [MeV$/c$]",
        "bins" : 45
  },
    "Track1_PT" : {
        "EDGES" : [0,20000],
        "xlabel" : r"Track1 $p_{T}$ [MeV$/c$]",
        "bins" : 45
  },
    "Track1_IPCHI2_OWNPV" : {
        "EDGES" : [0,12],
        "xlabel" : r"$\chi^2_{IP} Track1$",
        "bins" : 45
  },
    "Track1_ORIVX_DOCAMAX" : {
        "EDGES" : [0,0.5],
        "xlabel" : r"Track1 DOCA Original Vertex [mm]",
        "bins" : 45
  },
}
