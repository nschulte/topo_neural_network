""" container of the truth-matching components and latex labels"""

from particle.pdgid import literals as lid
from particle import Particle

# Nicole looked into this and the lid IDs should all be for positively charged particles
channels = {
  "DsPi": {
    "label" : r"$B_s^0 \to [K^-\pi^+\pi^-]_{D_s^-} \, \pi^+$",
    "truth" : [lid.D_s_plus, lid.pi_plus, lid.pi_plus, lid.pi_plus, lid.K_plus],
  },
  "3MuNu": {
    "label" : r"$B^+ \to \mu^+ \mu^- \mu^+ \nu_{\mu}$",
    "truth" : [lid.mu_minus, lid.mu_minus, lid.mu_minus, lid.nu_mu],
  },
  "DstTauNu": {
    "label" : r"$B^0 \to D^{*-}\tau^+ \nu_{\tau}$ hadronic",
    "truth" : [lid.Dst_2010_plus, lid.tau_minus, lid.nu_tau, lid.D_0, lid.pi_plus, lid.pi_plus, lid.pi_plus, lid.pi_plus,
      lid.nu_tau, lid.K_plus, lid.pi_plus],
  },
  "D0Ds": {
    "label" : r"$B^+ \to \overline{D}^0 D_s^+$",
    "truth" : [lid.D_s_plus, lid.D_0, lid.K_plus, lid.K_plus, lid.pi_plus, lid.K_plus, lid.pi_plus],
  },
  "KPiPi": {
    "label" : r"$B^+ \to K^+\pi^+\pi^-$",
    "truth" : [lid.pi_plus, lid.pi_plus, lid.K_plus],
  },
  "K6Mu": {
    "label" : r"$B^+ \to K^+6\mu$",
    "truth" : [lid.K_plus, lid.mu_minus, lid.mu_minus, lid.mu_minus, lid.mu_minus, lid.mu_minus, lid.mu_minus,],
  },
  "JpsiPhiKs": {
    "label" : r"$B^0 \to J/\psi \phi K_s^0$",
    "truth" : [lid.Jpsi_1S, lid.phi_1020, lid.K_S_0, lid.mu_minus, lid.mu_minus, lid.K_plus, lid.K_minus],
  },
  "D0KsPi": {
    "label" : r"$B^+ \to \overline{D}^0 K_s^0 \pi^+$",
    "truth" : [lid.D_0, lid.K_S_0, lid.pi_plus, lid.K_plus, lid.pi_plus],
  },
  "DDKPi": {
    "label": r"$B^0 \to (\overline{D}^0 \to K^+ \pi^-) (D^0 \to K^- \pi^+ \pi^+ \pi^-) K^+ \pi^-$", #needs to be checked again
    "truth": [lid.D_0, lid.D_0, lid.K_plus, lid.pi_minus, lid.K_minus, lid.pi_plus, lid.K_plus, lid.pi_minus, lid.pi_plus, lid.pi_minus],
  },
  "DPiPiPi": {
    "label": r"$B^+ \to (D^0 \to K^+ \pi^-) \pi^+ \pi^- \pi^+]$",
    "truth": [lid.a_1_1260_plus, lid.D_0, lid.rho_770_0, lid.pi_plus, lid.K_plus, lid.pi_minus, lid.pi_plus, lid.pi_minus],
  },
  "DsMuNu": {
    "label": r"$B_s^0 \to (D_s- \to K^+ K^- \pi^-) \mu^+ \nu_\mu ]$",
    "truth": [lid.D_s_plus, lid.mu_plus, lid.nu_mu, lid.K_plus, lid.K_minus, lid.pi_minus], #unsure about the D
  },
  "DstD0": {
    "label": r"$B^+ \to (D^{\star +}(2010) \to (\overline{D}^0 \to \pi^- K^+) (D^0 \to K^- \pi^+) \pi^+)$",
    "truth": [lid.Dst_2010_plus, lid.D_0, lid.D_0, lid.pi_plus, lid.K_minus, lid.pi_minus, lid.K_plus, lid.pi_plus],
  },
  "KstGamma": {
    "label": r"$B^0 \to (K^{\star 0}(892) \to K^+ \pi^-) \gamma$",
    "truth": [lid.Kst_892_0, lid.K_plus, lid.pi_minus],
  },
  "Lc2625MuNu": {
    "label": r"$\Lambda_b^0 \to (\Lambda_c(2625)^+ \to (\Lambda_c^+ \to p^+ K^- \pi^+) \pi^+ \pi^-) \mu^- \overline{\nu}_\mu]$",
    "truth": [104124, lid.mu_minus, lid.nu_mu, lid.Lambda_c_plus, lid.pi_plus, lid.pi_minus, lid.p, lid.K_minus, lid.pi_plus],
  },
  "LcPPi": {
    "label": r"$B^+ \to (\overline{Lambda}_c^- \to p^- K^+ \pi^-) p^+ pi^+$",
    "truth": [lid.Lambda_c_plus, lid.p, lid.pi_plus, lid.p, lid.K_plus, lid.pi_minus],
  },
  "PhiKst": {
    "label": r"$B^0 \to (\phi(1020) \to K^+ K^-) (K^{\star 0}(892) \to K^+ \pi^-)$",
    "truth": [lid.phi_1020, lid.Kst_892_0, lid.K_plus, lid.K_minus, lid.K_plus, lid.pi_minus],
  },
  "PiMuNu": {
    "label": r"$B^0 \to \pi^- \mu^+ \nu_\mu$",
    "truth": [lid.pi_minus, lid.mu_plus, lid.nu_mu],
  },
  "LcMuNu": {
    "label": r"$\Lambda_b^0 \to (\Lambda_c^+ \to p^+  K^- \pi^+) \mu^-  \overline{\nu}_\mu$",
    "truth": [lid.Lambda_c_plus, lid.mu_minus, lid.nu_mu, lid.p, lid.K_minus, lid.pi_plus],
  },
  "PMuNu": {
    "label": r"$\Lambda_b^0 \to p^+ \mu^- \overline{\nu}_\mu$",
    "truth": [lid.p, lid.mu_minus, lid.nu_mu],
  },
  "JpsiTauNu": {
    "label": r"$B_c^+ \to (J/psi(1S) \to \mu^+ \mu^-) (\tau^+ \to \mu^+ \nu_\mu \nu_\tau) \nu_\tau$",
    "truth": [lid.Jpsi_1S, lid.tau_plus, lid.nu_tau, lid.mu_plus, lid.mu_minus, lid.mu_plus, lid.nu_mu, lid.nu_tau],
  },
  "JpsiMuNu": {
    "label": r"$B_c^+ \to (J/psi(1S) \to \mu^+ \mu^-) \mu^+ \nu_\mu$",
    "truth": [lid.Jpsi_1S, lid.mu_plus, lid.nu_mu, lid.mu_plus, lid.mu_minus],
  },
  "KMuNu": {
    "label": r"$B_s^0 \to K^- \mu^+ \nu_\mu$",
    "truth": [lid.K_minus, lid.mu_plus, lid.nu_mu],
  },
  "KTauNu": {
    "label": r"$B_s^0 \to K^- (\tau^+ \to \mu^+ \nu_\mu \overline{nu}_\tau) \nu_\tau$",
    "truth": [lid.K_minus, lid.tau_plus, lid.nu_tau, lid.mu_plus, lid.nu_mu, lid.nu_tau],
  },
  "D0XMuNu": {
    "label": r"$B^- \to \mu^- \overline{\nu}_\mu (D^{\star 0} \to (D^0 \to K^- \pi^+) \pi^0)$",
    "truth": [lid.mu_minus, lid.nu_mu, lid.Dst_2007_0, lid.D_0, lid.pi_0, lid.K_minus, lid.pi_plus], #might be the wrong Dstar zero, confused here
  },
  "D0TauNu": {
    "label": r"$B^- \to (D^0 \to K^- \pi^+) (\tau^- \to \mu^- \nu_\tau \overline{\nu}_\mu) \overline{\nu}_\tau$",
    "truth": [lid.D_0, lid.tau_minus, lid.nu_tau, lid.K_minus, lid.pi_plus, lid.mu_minus, lid.nu_tau, lid.nu_mu],
  },
  "Dst0TauNu": {
    "label": r"$B^+ \to (\overline{D}^{\star 0} \to \pi^0 (\overline{D}^0 \to K^+ \pi^-)) (\tau^+ \to \pi^+ \pi^- \pi^+ \overline{\nu}_\tau) \nu_\tau$",
    "truth": [lid.Dst_2007_0, lid.tau_plus, lid.nu_tau ,lid.pi_0, lid.D_0, lid.pi_plus, lid.pi_minus, lid.pi_plus, lid.nu_tau, lid.K_plus, lid.pi_minus],
  },
  "PPTauNu": {
    "label": r"$B^- \to p^+ \overline{p}^- (\tau^- \to \mu^- \overline{\nu}_\mu \nu_\tau) \overline{\nu}_\tau$",
    "truth": [lid.p, lid.p, lid.tau_minus, lid.nu_tau, lid.mu_minus, lid.nu_mu, lid.nu_tau],
  },
  "PPMuNu": {
    "label": r"$B^- \to p^+ \overline{p}^-  \mu^- \overline{\nu}_\mu$",
    "truth": [lid.p, lid.p, lid.mu_minus, lid.nu_mu],
  }
}