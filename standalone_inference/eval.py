"""Evaluate the trained NN on exclusive MC"""

__author__   = "Blaise Delaney, Nicole Schulte"
__email__    = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
__modelpath__= "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/Lip_M_NN" 
__rootpath__ = "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/scratchN" 
__mainpath__ = "/home/blaised/private/hlt2_topo/topo_neural_network" # comment out as needed
#__mainpath__ = f"{__mainpath__}" # comment out as needed

from builtins import breakpoint
from cgi import test
import os, yaml
from matplotlib.pyplot import broken_barh
from argparse import ArgumentParser
from pathlib import Path
import numpy as np
import pandas as pd
import torch
from sklearn.metrics import roc_auc_score
from hist import Hist
import hist
import warnings 

# from /pipeline/ directory
import sys
sys.path.insert(1, __mainpath__)
from pipeline.utils import read_config, read_id_map, plt_config_ThreeBody, plt_config_TwoBody
#from pipeline.mpl_config import *
from pipeline.train import get_model
from Utils.channel_config import channels
from matplotlib import pyplot as plt 
plt.style.use('mpl-config')
import mplhep as hep

def to_hist2d(
    data_12:"list/array",
    data_2b:"list/array",
    edges_12:"variable edges",
    edges_2b:"variable edges",
    weights=None,
)->"hist":
    """returns: 2D hist, edges, centers, bin height, bin err"""
    if weights is None:
        h = Hist( 
            hist.axis.Variable(edges_12, name="m12"),
            hist.axis.Variable(edges_2b, name="m2b"),
        )
        h.fill( data_12, data_2b )
        
    if weights is not None:
        h = Hist( 
            hist.axis.Variable(edges_12, name=r"$m^2_{12}$"),
            hist.axis.Variable(edges_2b, name=r"$m^2_{2b}$"), 
            storage=hist.storage.Weight(),
        )
        h.fill( data_12, data_2b, weight = weights )
         
    return h

def to_hist(
    data:"list/array",
    edges:"variable edges",
    weights=None,
    maxNorm=False, # normalise by max bin
    sumNorm=False, # normalise by sum(bins)
)->"hist":
    """returns: hist, edges, centers, bin height, bin err"""
    if weights is None:
        h = Hist( hist.axis.Variable(edges, name="obs") )
        h.fill( data )
        nh  = h.view()
        err = h.view()**0.5
    if weights is not None:
        h = Hist( hist.axis.Variable(edges, name="obs"), storage=hist.storage.Weight())
        h.fill( data, weight = weights )
        nh  = h.view().value
        err = h.view().variance**0.5

    xe  = h.axes[0].edges
    cx  = h.axes[0].centers
    frac_err = err/nh

    if maxNorm:
        integral = np.max(nh)
        nh = nh/integral
        err = err/integral
        scaled_frac_err = err/nh
        assert(np.max(nh)==1)
        assert(frac_err.all() == scaled_frac_err.all())

    if sumNorm:
        integral = np.sum(nh)
        nh = nh/integral
        err = err/integral
        scaled_frac_err = err/nh
        assert(frac_err.all() == scaled_frac_err.all())

    return h, xe, cx, nh, err


def cut_given_eff(
    df #name sample
)->"Get Cut Values Given Efficiency":
    response_cut = {
        #"0.6": None,
        #"0.7": None,
        "0.8": None,
        "0.9": None,
    }
    signal = df[df.label== 1] # needed as not-truth matched candidates still get label = 0
    for cut_value in np.arange(0.90, 1.000, 0.0001):  # for constrained
    #for cut_value in np.arange(0.990, 1.000, 0.00001): # for unconstrained. Takes a bit, but satisfies the 1% window for the constrained (0.97, 1.000, 0.0005)
        eff = len(signal[signal.preds_per_cand > cut_value])/len(signal)
        #if abs(eff - 0.6) < 0.01: response_cut["0.6"] = cut_value
        #if abs(eff - 0.7) < 0.01: response_cut["0.7"] = cut_value
        warnings.warn(" ===== Removing 60% and 70% efficiency cut values for speedup =====", stacklevel=2 )
        if abs(eff - 0.8) < 0.01: response_cut["0.8"] = cut_value
        if abs(eff - 0.9) < 0.01: response_cut["0.9"] = cut_value
        #print(cut_value, eff)

    for value in response_cut.values(): assert(value is not None)
    assert(response_cut["0.9"] < response_cut["0.8"]) #< response_cut["0.7"] < response_cut["0.6"])
    print(f"The right cut values are: {response_cut}")

    return response_cut

def std_eff(
    _pass:"integrer or array of integers",
    _tot:"integrer or array of integers"
)->"integrer or array of integers":

    # from Paterno, sec 2.2 - https://lss.fnal.gov/archive/test-tm/2000/fermilab-tm-2286-cd.pdf
    return (1/_tot) * np.sqrt( _pass*(1-_pass/_tot) )

def plot_obs_eff(df, branch, _edges, _xlabel, cuts_dict, 
    channel_label, _man=False, _label=1, outputpath=None):

    sig = df[df.label==_label]
    EDGES = _edges+[sig[branch].max()]
    # allow also manually set bins
    if _man:
        EDGES=_edges # hacky, would be nice to make this more streamlined
    print(branch)
    fig, ax = plt.subplots()

    H, xe, cx, nh, err = to_hist(
        data = sig[branch].to_numpy(),
        edges=EDGES,
        maxNorm=True
    )

    ax2 = ax.twinx()
    ax2.set_ylabel("Arbitrary Units", color="grey")
    ax2.tick_params(axis='y', colors='grey', which="both")
    hep.histplot(
        nh,
        bins=xe,
        ax=ax2,
        color="lightgrey",
        alpha=.75,
        histtype="fill"
    )

    cosmetics = {
        # "0.6" : {
        #     "color": "#3288bd",
        #     "fmt"  : ".",
        #     "label": r"Cut @ $\varepsilon^{TOS}=60\%$",
        #     "markersize": 10,
        # },
        # "0.7" : {
        #     "color": "#66c2a5",
        #     "fmt"  : ".",
        #     "label": r"Cut @ $\varepsilon^{TOS}=70\%$",
        #     "markersize": 10,
        # },
        "0.8" : {
            "color": "#f46d43",
            "fmt"  : ".",
            "label": r"Cut @ $\varepsilon^{TOS}=80\%$",
            "markersize": 10,
        },
        "0.9" : {
            "color": "#9e0142",
            "fmt"  : ".",
            "label": r"Cut @ $\varepsilon^{TOS}=90\%$",
            "markersize": 10,
        },
    }

    for nncut in list(cosmetics.keys()):
        h_sel, _, _, _, _ = to_hist(
            data = sig[sig.preds_per_cand>float(cuts_dict[nncut])][branch].to_numpy(),
            edges=EDGES
        )

        # =======================================================
        # APPLY Clopper-Pearson confidence interval 
        # https://hist.readthedocs.io/en/latest/reference/hist.html?highlight=plot_ratio#hist.intervals.ratio_uncertainty
        # -------------------------------------------------------
        # # test hist package for eff
        # import matplotlib
        # matplotlib.rcParams.update(matplotlib.rcParamsDefault)
        # fig = plt.figure(figsize=(10, 8))
        # main_ax_artists, sublot_ax_arists = h_sel.plot_ratio(
        #     H,
        #     rp_num_label="h sel",
        #     rp_denom_label="truth",
        #     rp_uncert_draw_type="line",
        #     rp_uncertainty_type="efficiency",
        # )
        # plt.savefig(f"test_{nncut}.png")
        # =======================================================

        ax.errorbar(
            x = H.axes[0].centers,
            # NaN mapped to 0
            #y = np.nan_to_num(h_sel.project("obs").view() / H.project("obs").view()), # removed, as it can generate bugs
            y = h_sel.project("obs").view() / H.project("obs").view(),
            xerr = H.axes[0].widths/2.,
            yerr = std_eff(
                _pass = h_sel.project("obs").view(),
                _tot = H.project("obs").view()
            ),
            elinewidth=2,
            capsize=2,
            markeredgewidth=2,
            **cosmetics[nncut]
        )

    ax.set_zorder(ax2.get_zorder()+1)
    ax.patch.set_visible(False)
    ax.axhline(1., color="black", ls=":", lw=2,)
    ax.set_xlabel(_xlabel)
    ax.set_ylabel("Efficiency Ratio")
    ax.text(0.7, 0.2, channels[opts.channel]["label"], 
        horizontalalignment='center',
        verticalalignment='center', transform=ax.transAxes, fontsize=30)
    ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
    ax2.set_title(r"$\lambda = %s$"%(opts.Lambda), loc="right", fontsize=25)
    ax.legend(fontsize=25, ncol=2, loc='upper center', bbox_to_anchor=(0.5, -.15),)

    ax.set_ylim(bottom=0, top=1.05)
    ax2.set_ylim(bottom=0, top=1.05)
    #plt.savefig(f"{opts.plots_path}/unconstrained/unconstrained_{branch}.png")
    #plt.savefig(f"{opts.plots_path}/unconstrained/unconstrained_{branch}.pdf")
    Path(f"{opts.plots_path}/{opts.Lambda}/{opts.channel}").mkdir(parents=True, exist_ok=True)
    plt.savefig(f"{opts.plots_path}/{opts.Lambda}/{opts.channel}/{opts.Lambda}_{branch}.png")
    plt.savefig(f"{opts.plots_path}/{opts.Lambda}/{opts.channel}/{opts.Lambda}_{branch}.pdf")
    plt.close()

def plot_eff_prompt_charm(df, _label=1, outputpath=None):

    sig = df[df.label==_label]
    prompt_charm = sig.query(
            f"{opts.nbody}_FromSameB==False and {opts.nbody}_FromSameD==1" # not a beauty grandmother
        )

    # denominators
    _tot = len(sig)
    _tot_prompt_charm = len(prompt_charm)

    # containers
    sig_effs = []
    sig_errs = []
    pch_effs_c = []
    pch_errs_c = []

    NNcuts = np.array([0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99])
    for nncut in NNcuts:
        # total sig effs
        _pass = len(sig[sig.preds_per_cand>nncut])
        eff = _pass/_tot
        err = std_eff(_tot=_tot, _pass=_pass)
        sig_effs.append(eff)
        sig_errs.append(err)

        # prompt charm effs
        try:
            pch_pass = len(prompt_charm[prompt_charm.preds_per_cand>nncut])
            pch_eff = pch_pass / _tot_prompt_charm
            pch_err = std_eff(_tot=_tot_prompt_charm, _pass=pch_pass)
            pch_effs_c.append(pch_eff)
            pch_errs_c.append(pch_err)
        except:
            pch_effs_c.append(0)
            pch_errs_c.append(0)

    fig, ax = plt.subplots()

    ax.errorbar(
        x = NNcuts,
        y = sig_effs,
        #xerr = (NNcuts[:-1] + NNcuts[1:])/2.,
        yerr = sig_errs,
        elinewidth=1.5,
        capsize=3,
        markeredgewidth=1.5,
        color= "tab:blue",
        fmt=".",
        markersize= 8,
        label="Inclusive Beauty"
    )

    ax.errorbar(
        x = NNcuts,
        y = pch_effs_c,
        yerr = pch_errs_c,
        elinewidth=1.5,
        capsize=0,
        markeredgewidth=0,
        color= "tab:blue",
        fmt=".",
        markersize= 0.,
    )

    ax.bar(
        x = NNcuts,
        height = pch_effs_c,
        #width = (NNcuts[:-1] + NNcuts[1:])/2.,
        color="tab:red",
        alpha=.75,
        label="Prompt Charm"
    )


    ax.axhline(1.0, color="darkorange", ls="--", lw=1)
    ax.set_xlabel("NN response cut [Arbitrary Units]")
    ax.set_ylabel("Efficiency Ratio")
    ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
    ax.set_title(r"$\lambda = %s$"%(opts.Lambda), loc="right", fontsize=25)
    ax.legend(fontsize=23, ncol=2, bbox_to_anchor=(0.5, -1.1))

    ax.set_ylim(bottom=0, top=1.05)
    #plt.savefig(f"{opts.plots_path}/unconstrained/{opts.nbody}_prompt_charm.png")
    #plt.savefig(f"{opts.plots_path}/unconstrained/{opts.nbody}_prompt_charm.pdf")
    plt.savefig(f"{opts.plots_path}/{opts.Lambda}/{opts.nbody}_prompt_charm.png")
    plt.savefig(f"{opts.plots_path}/{opts.Lambda}/{opts.nbody}_prompt_charm.pdf")
    plt.close()

def plot_response(sample):
    plt.figure()
    plt.hist(sample[sample.label == 1].preds_per_cand, alpha=0.5, bins=100, log=True, label="Signal Predictions")
    plt.legend(loc='best')
    #plt.savefig(f"{opts.plots_path}/unconstrained/unconstrained_{opts.nbody}_Response.pdf")
    #plt.savefig(f"{opts.plots_path}/unconstrained/unconstrained_{opts.nbody}_Response.png")
    plt.savefig(f"{opts.plots_path}/{opts.Lambda}/{opts.Lambda}_{opts.nbody}_Response.pdf")
    plt.savefig(f"{opts.plots_path}/{opts.Lambda}/{opts.Lambda}_{opts.nbody}_Response.png")
    plt.close()

if __name__ == '__main__':
    parser = ArgumentParser(description="exclusive MC eval configuration")
    parser.add_argument("-e","--exec",
        default=f"{__mainpath__}/pipeline/exec",
        help="path to /exec/ parent directory where the trained model can be fetched")
    parser.add_argument('-b','--nbody',     required=True, choices=["TwoBody", "ThreeBody"],
        help="n-body implementation ['TwoBody', 'ThreeBody']")
    parser.add_argument('-l','--Lambda',    required=True, 
        help="Lipschitz constant value")
    parser.add_argument('-M','--monotonic', action="store_false",
        help="engage monotonicity? [default: True]")
    parser.add_argument('-R','--robust', action="store_false",
        help="engage robustness? [default: True]")
    parser.add_argument('-f','--features',  default=f"{__mainpath__}/pipeline/features.yml",
        help="features of the model")
    parser.add_argument('-c','--channel', required=True,
        choices = list(channels.keys()),
        help="Signal MC channel")
    parser.add_argument('-v','--plots_path',  default=f"{__mainpath__}/standalone_inference/plots/eval",
        help="path to dir where the preprocessing is visualised")
    parser.add_argument('-W','--notLip',  action="store_true",
        help="Unconstrained NN [default:False]")
    parser.add_argument('-q','--stop',  default=-1, type=int,
        help="max number of candidates to read in [default: -1]")
    parser.add_argument('-S','--sigonly',  
        action="store_false",
        help="enforce upstream label==1 [default = true]")
    opts = parser.parse_args()


    if opts.Lambda!="None": # ugly hack
        opts.Lambda = float(opts.Lambda)

    # if available, run on GPU
    if(torch.cuda.is_available()):
        device = torch.device("cuda")
        print("Running on cuda")
    else:
        device = torch.device("cpu")
        print("Running on cpu")

    FEATURES = read_config(opts.features, _nbody=opts.nbody)
    nn_config = {
        "robust"    : opts.robust,
        "monotonic" : opts.monotonic,
        "_nbody"    : opts.nbody,
        "_features" : FEATURES,
        "LIP"       : opts.Lambda,
    }
    MODEL = get_model(
        **nn_config
    ).to(device)

    # load trained model from the nominal Lip value
    #model_path = f"{opts.exec}/{opts.nbody}/unconstrained/train/trained_model_unconstr.pt"
    if opts.notLip: 
        lip_suffix = "_Unc"
    if not opts.notLip: 
        lip_suffix = "_LipM"
    model_path = f"{opts.exec}_{opts.nbody}{lip_suffix}/{opts.nbody}/lambda{opts.Lambda}/train/trained_model.pt"
    if(device == torch.device('cpu')):
        MODEL.load_state_dict(torch.load(model_path, map_location=torch.device('cpu'))) #Map Location needed if running on cpu
    else:
        MODEL.load_state_dict(torch.load(model_path))
    MODEL.eval()

    # load mc & perform inference
    phys_sample = pd.read_pickle(f"{__mainpath__}/standalone_inference/scratch/{opts.channel}/{opts.nbody}_µscaled.pkl")[:opts.stop]

    # enforce all signal b-hadron
    if opts.sigonly: 
        phys_sample = phys_sample[phys_sample.label==1]

    # === INFERENCE ===
    X = torch.tensor(phys_sample[FEATURES].to_numpy().astype(np.float32)).to(device)
    #prints monotonic contrains on features and the model
    phys_sample["preds_per_cand"] = torch.sigmoid(MODEL(X)).cpu().detach().numpy()

    dalitz = to_hist2d(
        data_12 = np.square(phys_sample.M12), 
        data_2b = np.square(phys_sample.M2B),
        edges_12= list(np.arange(0,5_000_000, 1_00_000)), 
        edges_2b= list(np.arange(0,20_000_000, 1_00_000)), 
    )

    
    fig, d_ax = plt.subplots()
    hep.hist2dplot(dalitz, ax=d_ax, cmin=1)
    d_ax.set_title("This should be Dalitz!")
    plt.savefig("dalitz_temp.png")
    
    breakpoint()

    print('plotting effcies')
    probe_cuts = cut_given_eff(phys_sample)
    
    if opts.nbody=="ThreeBody": upper_PT = 5
    if opts.nbody=="TwoBody": upper_PT   = 15
    eff_obs = {
        # f"{opts.nbody}_FDCHI2_OWNPV" : {
        #     "_edges" : list(np.arange(0, 6, 1)),
        #     "_xlabel" : r"log(Flight Distance $\chi^2$) [Arbitrary Units]"
        # },
        f"{opts.nbody}_TAU" : {
            "_edges" : (list(np.arange(0,7,.5))+[15]),
            "_xlabel" : r"Beauty $\tau$ [ps]",
            "_man" : True,
        },
        f"{opts.nbody}_PT" : {
            "_edges" : list(np.arange(2, 15, 1)),
            "_xlabel" : r"Beauty $p_{T}$ [GeV$/c$]"
        },
    #     f"{opts.nbody}_Mcorr" : {
    #         "_edges" : [1,2,3,4,5,7,10,12], # gev/c^2
    #         "_xlabel" : r"Beauty $m_{corr}$ [GeV$/c^2$]",
    #         "_man"   : True # manually set bins
    #     },
    }

    for p in eff_obs.keys():
        plot_obs_eff(
            df=phys_sample,
            branch=p,
            outputpath = f"{opts.plots_path}/{opts.nbody}/{opts.Lambda}_plots/eff",
            cuts_dict=probe_cuts, 
            channel_label = channels[opts.channel]["label"],
            **eff_obs[p]
            )

    plot_response(phys_sample)

    print(f"Minimal Response from the NN: {phys_sample.preds_per_cand.min()}")
    #plot_eff_prompt_charm(
    #    df = phys_sample,
    #    outputpath = f"{opts.plots_path}/{opts.nbody}/plots/eff",
    #)