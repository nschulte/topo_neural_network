"""Convert ROOT files to pkl dataframe, with training variables and those necessay to understand the efficiency behaviour of the NN"""

__author__   = "Blaise Delaney, Nicole Schulte"
__email__    = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
__modelpath__= "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/Lip_M_NN" 
__rootpath__ = "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/scratchN" 
__mainpath__ = "/home/blaised/private/hlt2_topo/topo_neural_network" # comment out as needed
#__mainpath__ = f"{__mainpath__}" # comment out as needed

from builtins import breakpoint
import pandas as pd
import pickle
import time
import uproot
import numpy as np
from argparse import ArgumentParser
from pathlib import Path
import matplotlib.pyplot as plt
import yaml
import sys
sys.path.insert(1, f"{__mainpath__}")
from pipeline.utils import read_config
from Utils.channel_config import channels
from pathlib import Path
from termcolor2 import c
import warnings

parser = ArgumentParser(description="ROOT -> pkl")
parser.add_argument('-f','--features',  default=f"{__mainpath__}/standalone_inference/boi.yml",
    help="features of the model")
parser.add_argument('-i','--input', 
    default =f"{__mainpath__}/Tupling/scratch",
    help="path to input ROOT file")
parser.add_argument('-c','--channel', required=True,
    choices = list(channels.keys()),
    help="Signal MC channel")
parser.add_argument('-b','--nbody', required=True, choices=["TwoBody", "ThreeBody"],
    help="n-body implementation ['TwoBody', 'ThreeBody']")
parser.add_argument('-T','--not_transform',
    action="store_true",
    help="if true, not apply scaling to O(1) rangege [default:False]")
opts = parser.parse_args()

def read_tuple(
    rootfile:"ROOT file",
    directory:"[TwoBody|ThreeBody]",
    boi:"branches of interest",
    glob:"glob for additional branches (necessary of effs observables)"=None,
    _cut:"branch cuts"=None,
    _stop=None,
    _lib="pd",
)->"pandas dataframe":
    """convert read in the input ROOT file, trimming branches"""
    events = uproot.open(f"{rootfile}:{directory}/DecayTree")
    df = events.arrays(
        cut=_cut,
        expressions=boi,
        filter_name=glob,
        entry_stop=_stop,
        library=_lib
    )
    return df

def read_pickledf(
    picklefile:"pickle file",
    directory:"[TwoBody|ThreeBody]",
    boi:"branches of interest",
    channel:"exclusive signal MC channel",
)->"pandas dataframe":
    """read whole dataframe from pickle, then trim to desired branches"""
    df = pd.read_pickle(f"{picklefile}/{channel}/{directory}_calcobs.pkl")
    df = df.filter(boi)
    return df

BOI = list(read_config(opts.features, _nbody=opts.nbody)+[
        f"{opts.nbody}_FromSignalB",
        f"{opts.nbody}_FromSameB",
        f"{opts.nbody}_FromSameD",
        "TwoBody_DAUGHTERS",
        "TwoBody_ENDVERTEX_X",
        "TwoBody_ENDVERTEX_Y",
        "TwoBody_ENDVERTEX_Z",
        "TwoBody_OWNPV_X",
        "TwoBody_OWNPV_Y",
        "TwoBody_OWNPV_Z",
        "TwoBody_PX",
        "TwoBody_PY",
        "TwoBody_PZ",
        "TwoBody_PE",
        "TwoBody_M",
        "Track1_PX",
        "Track1_PY",
        "Track1_PZ",
        "Track1_PE",
        "Track1_M",
        "Track2_PX",
        "Track2_PY",
        "Track2_PZ",
        "Track2_PE",
        "Track2_M",
        "runNumber",
        "eventNumber",
        f"{opts.nbody}_TAU"
    ])

if opts.nbody=="ThreeBody":
    BOI = BOI+ [
        "TrackB_PX",
        "TrackB_PY",
        "TrackB_PZ",
        "TrackB_PE",
        "TrackB_M",
        "ThreeBody_DAUGHTERS",
        "ThreeBody_ENDVERTEX_X",
        "ThreeBody_ENDVERTEX_Y",
        "ThreeBody_ENDVERTEX_Z",
        "ThreeBody_OWNPV_X",
        "ThreeBody_OWNPV_Y",
        "ThreeBody_OWNPV_Z",
        "ThreeBody_PX",
        "ThreeBody_PY",
        "ThreeBody_PZ",
        "ThreeBody_PE",
        "ThreeBody_M",
        "M12", 
        "M2B",
    ]

df = read_pickledf(
    picklefile=opts.input,
    directory=opts.nbody,
    channel=opts.channel, 
    boi=BOI
)

# ============================
#       scale to O(1)
# ============================
if opts.nbody=="TwoBody":
    print(c("Create training observables for the TwoBody NN").magenta)
    from pipeline.preliminary_TwoBody import _gev_vars, _log_vars
    # establish the branches used for the training (2body, as a start)
    df["min_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV"]].min(axis=1)
    df["max_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV"]].max(axis=1)
    assert(df.min_FS_IPCHI2_OWNPV.all()<=df.max_FS_IPCHI2_OWNPV.all()) # sanity check

    # seems to be missing from the balanced samples, need to benchmark against the max
    df["min_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT"]].min(axis=1)
    df["max_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT"]].max(axis=1)
    df["sum_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT"]].sum(axis=1)
    
    if not opts.not_transform:
        for g in _gev_vars:
            df[g] /= 1000.
        for l in _log_vars:
            df.loc[df[l] <= 0, l] = 1e-5
            assert(df[l].all()>0)
            df[l] = np.log(df[l].to_numpy())
        df[f"{opts.nbody}_ENDVERTEX_DOCAMAX"] *= 10.
        df[f"{opts.nbody}_TAU"] *= 1000. # show plots in ps, not ns
    print(c("SUCCESS").white.on_green)
    

if opts.nbody=="ThreeBody":
    print(c("Create training observables for the ThreeBody NN").cyan) 
    from pipeline.preliminary_ThreeBody import _gev_vars, _log_vars

    # ignore 3body mcorr
    warnings.warn("Neglecting TwoBody_Mcorr in 3body decay -- this needs to be computed in the future, especially if used in the training feature set!", DeprecationWarning)
    _gev_vars.remove("TwoBody_Mcorr")

    df["min_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV","TrackB_IPCHI2_OWNPV"]].min(axis=1)
    df["max_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV","TrackB_IPCHI2_OWNPV"]].max(axis=1)
    assert(df.min_FS_IPCHI2_OWNPV.all()<=df.max_FS_IPCHI2_OWNPV.all()) # sanity check

    # seems to be missing from the balanced samples, need to benchmark against the max
    df['sum_PT_TRACK12'] = df[['Track1_PT', 'Track2_PT']].sum(axis=1)
    df['max_PT_TRACK12'] = df[['Track1_PT', 'Track2_PT']].max(axis=1)
    df["min_PT_TRACK12"] = df[["Track1_PT","Track2_PT"]].min(axis=1)

    df["min_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].min(axis=1)
    df["max_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].max(axis=1)
    df["sum_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].sum(axis=1)

    if not opts.not_transform:
        for g in _gev_vars:
            df[g] /= 1000. 
        for l in _log_vars:
            df.loc[df[l] <= 0, l] = 1e-5
            assert(df[l].all()>0)
            df[l] = np.log(df[l].to_numpy()) 
        df["TwoBody_ENDVERTEX_DOCAMAX"] *= 10.
        df["ThreeBody_ENDVERTEX_DOCAMAX"] *= 10.
        df[f"{opts.nbody}_TAU"] *= 1000. # show plots in ps, not ns
  

breakpoint()
Path(f"{__mainpath__}/standalone_inference/scratch/{opts.channel}").mkdir(parents=True, exist_ok=True)
df.to_pickle(f"{__mainpath__}/standalone_inference/scratch/{opts.channel}/{opts.nbody}.pkl")