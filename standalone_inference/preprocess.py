"""Implement preprocessing on raw MC"""

__author__   = "Blaise Delaney, Nicole Schulte"
__email__    = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
__modelpath__= "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/Lip_M_NN" 
__rootpath__ = "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/scratchN" 
__mainpath__ = "/home/blaised/private/hlt2_topo/topo_neural_network" # comment out as needed
#__mainpath__ = f"{__mainpath__}" # comment out as needed

from builtins import breakpoint
import pandas as pd
from sklearn.preprocessing import QuantileTransformer
import pickle
import time
import uproot
import numpy as np
from argparse import ArgumentParser
from pathlib import Path
import matplotlib.pyplot as plt
import yaml
import sys
from scipy.stats import median_abs_deviation
sys.path.insert(1, f"{__mainpath__}")
from pipeline.utils import read_config, read_id_map, plt_config_ThreeBody, plt_config_TwoBody
from pipeline.mpl_config import *
from Utils.channel_config import channels

parser = ArgumentParser(description="training configuration")
parser.add_argument('-f','--features',  default=f"{__mainpath__}/pipeline/features.yml",
    help="features of the model")
parser.add_argument('-v','--plots_path',  default=f"{__mainpath__}/standalone_inference/plots/preprocess",
    help="path to dir where the preprocessing is visualised")
parser.add_argument('-i','--input',
    default=f"{__mainpath__}/standalone_inference/scratch",
    help="path to raw signal+bkg MC to be pre-processed")
parser.add_argument('-c','--channel', required=True,
    choices = list(channels.keys()),
    help="Signal MC channel")
parser.add_argument('-o','--out_path', default=f"{__mainpath__}/standalone_inference/scratch",
    help="path to where preprocessed dataframes get written to file")
parser.add_argument('-b','--nbody', required=True, choices=["TwoBody", "ThreeBody"],
    help="n-body implementation ['TwoBody', 'ThreeBody']")
parser.add_argument('-C','--not_constrain', action="store_true",
    help="impose a retention window in the variables preseved in the preprocessing? [default:False]")
parser.add_argument('-r','--rm_mcorr',
    action="store_true",
    help="if true, omit MCORR from the training features [default:False]")
opts = parser.parse_args()

#Use this part if you need to upload at the data from Root File. Takes longer
def dataframe(path,name, columns=None, **kwargs):
    f = uproot.open(path)
    if name != 'minbias':
        t = f[f"{opts.nbody}"]
    else:
        t = f[f"{opts.nbody}/DecayTree"]
    df = t.pandas.df(columns or '*', **kwargs)
    return df

def constrain(
    feat:"feature/column/branch",
    n_sigma=5, #"+/- n std dev of retention window",
)->"dataframe[features]":
    """retain if falling within a window about mean, post scaling / logging if needed"""

    mean = np.mean(feat)
    std  = np.std(feat)

    retained = feat[(feat>=mean-n_sigma*std) & (feat<=mean+n_sigma*std)]
    feat[(feat<mean-n_sigma*std)] = np.min(retained)
    feat[(feat>mean+n_sigma*std)] = np.max(retained)

    return feat

event_ids = ['runNumber', 'eventNumber']
#label_name = [F"{opts.nbody}_fromSignalB"]
same_Bs = [f"{opts.nbody}_FromSameB"]
same_Ds = [f"{opts.nbody}_FromSameD"]
#multiplicity = [f"{opts.nbody}_nCharged", f"{opts.nbody}_nNeutral"]
train_features=read_config(opts.features, _nbody=opts.nbody)

# run on the slimmed-down versions
print("Reading in un-transformed train/test dataframes...")
phys_sample = pd.read_pickle(f"{opts.input}/{opts.channel}/{opts.nbody}.pkl")
breakpoint()
print("SUCCESS")

# preprocessing segment
Path(f"{opts.plots_path}").mkdir(parents=True, exist_ok=True)

print("preprocessing with SIMPLIFIED / PHYSICS-DRIVEN preprocessing")
# hack, relic from deprecated code
if not opts.not_constrain:
    print("NOTE:running preprocessing with RENTION WINDOW ON")
    phys_sample[train_features] = phys_sample[train_features].apply(lambda x: constrain(x), axis=0)
if opts.not_constrain:
    pass
if opts.rm_mcorr:
    phys_sample[f"{opts.nbody}_Mcorr"] = np.zeros(len(phys_sample)) # null placeholder
print("SUCCESS")

# define nrows and ncols
if len(train_features) % 3==0:
    _ncols = 3 # preference
else:
    _ncols = 2

if opts.nbody=="ThreeBody": 
    fig_x = 30; fig_y = 30;
if opts.nbody=="TwoBody": 
    fig_x = 20; fig_y = 24;

train_features=read_config(opts.features, _nbody=opts.nbody)
Path(f"{opts.plots_path}/{opts.channel}/{opts.nbody}").mkdir(parents=True, exist_ok=True)
for t in [phys_sample,]:
    # visualise the effect of the transform
    fig, axs = plt.subplots(ncols=_ncols, nrows = round(len(train_features)/_ncols),
        figsize=(fig_x, fig_y), facecolor='w', edgecolor='k')
    fig.tight_layout()
    axs = axs.ravel()

    if opts.nbody=="ThreeBody": plt_config = plt_config_ThreeBody
    if opts.nbody=="TwoBody": plt_config = plt_config_TwoBody
    for i in range(len(train_features)):

        feat_name = train_features[i]
        axs[i].set_xlabel(f"{plt_config[train_features[i]]['xlabel']}")

        _min = np.min(t[feat_name])
        _max = np.max(t[feat_name])

        _bins = 10

        axis_config = {
            "range" : [_min, _max],
            "bins"  : _bins
        }

        feat = t[feat_name]
        axs[i].plot([],[],"", label=r"\textbf{LHCb Unofficial}", color="white")
        axs[i].hist(
            feat,
            color="tab:blue",
            histtype="stepfilled", alpha=.75,
            label=channels[opts.channel]["label"]+" MC",
            **axis_config
        )

        axs[i].set_ylabel("Candidates")
        legend = axs[i].legend(fontsize=25)
        legend.get_frame().set_edgecolor("white")

        print(f"{feat_name} plot done.")

    if len(axs)>len(train_features):
        for i in np.arange(len(train_features), len(axs), 1):
            axs[i].plot([], [], "", label="Dummy canvas", color="white")
            axs[i].legend(loc="center", fontsize=30)

    plt.savefig(f"{opts.plots_path}/{opts.channel}/{opts.nbody}/inference_µscaled.pdf")
    plt.savefig(f"{opts.plots_path}/{opts.channel}/{opts.nbody}/inference_µscaled.png")

#Make Label column
phys_sample['label'] = phys_sample[f"{opts.nbody}_FromSignalB"].astype(int)
breakpoint()
#Save Samples
Path(f"{opts.out_path}/{opts.channel}").mkdir(parents=True, exist_ok=True)
phys_sample.to_pickle(f"{opts.out_path}/{opts.channel}/{opts.nbody}_µscaled.pkl")