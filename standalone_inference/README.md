# Standalone NN performance evaluation on exclusive signal MC samples, post training 

The trained NN must be run on the exclusive MC samples, following the worflow below:

1. The ROOT files produced using `MooreAnalysis` must be (a) truth-matched, (b) must contain observables of interest, for which the efficiencies must be extracted.
2. The ROOT files must be converted into pkl format; all variables used in the training must also be generated. To this end, run
```python
python to_pkl.py
```
3. Subsequently, the µ-scaling ($`5\sigma`$ _clamping_ about the mean, µ) is applied vis `preprocess.py`, and sanity plots are also produced. Please inspect that all trainig variables of $`\mathcal{O}(1)`$. 
4. Inference: `eval.py`. This fetches the trained model, in `model.pt`, and runs it on the exclusive mode.
5. [Missing] Efficiency plots. This amounts to virtually using the same code as in `train.py`.

---

Ideally we should implement a cleanup, such that we simply import code from the relevant sections of `/pipeline/`.