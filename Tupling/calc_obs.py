"""Compute observables of interest for the Hlt2 B topo efficiency evaluation
Additionally, perform conversion ROOT -> pkl"""

__author__   = "Blaise Delaney, Nicole Schulte"
__email__    = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
__modelpath__= "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/Lip_M_NN" 
__rootpath__ = "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/scratch" 
__mainpath__ = "/home/blaised/private/hlt2_topo/topo_neural_network" # comment out as needed
#__mainpath__ = f"{__mainpath__}" # comment out as needed

import uproot, vector
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import mplhep as mpl
from tqdm import tqdm
import awkward as ak
from particle.pdgid import literals as lid
from particle import Particle
import numba as nb
from argparse import ArgumentParser
from termcolor2 import c
import pathlib

# import cosmetics
import sys, os
sys.path.insert(1, os.path.realpath(f"{__mainpath__}"))
from pipeline.mpl_config import *
from pipeline.utils import read_config
from Utils.channel_config import channels

#@nb.njit # https://vector.readthedocs.io/en/develop/usage/intro.html#Compiling-your-Python-with-Numba
def build_4vec(
    pX:float,
    pY:float,
    pZ:float,
    pE:float,
)->"lorentz vector":
    """build and check"""
    fourvec = vector.array({
    "x" : pX,
    "y" : pY,
    "z" : pZ,
    "E" : pE
    }); assert(isinstance(fourvec, vector.Lorentz))

    return fourvec

#@nb.njit
def build_3vec(
    pX:float,
    pY:float,
    pZ:float,
)->"three-vector":
    """build and check"""
    three_vec = vector.array({
    "x" : pX,
    "y" : pY,
    "z" : pZ,
    }); assert(isinstance(three_vec, vector.VectorNumpy3D))

    return three_vec

#@nb.njit
def calc_pointing_obs(
    pv:"PV",
    dv:"decay vertex",
)->"pointing, unit pointing, theta, phi":
    pointing = dv - pv
    pointing_u = pointing.unit() # unit vector
    assert(pointing_u.mag.all()==1.)

    theta = pointing_u.theta
    phi = pointing_u.phi

    return pointing, pointing_u, theta, phi

#@nb.njit
def calc_DIRA(
    vis_sys_3v:"three-vector",
    pointing_u:"unit vector"
)->float:
    dira = (vis_sys_3v @ pointing_u) / (vis_sys_3v.mag * pointing_u.mag)

    return dira

#@nb.njit
def calc_mcorr(
    vis_sys_lv:"four-vector",
    phi:float,
    theta:float,
    visM,
)->"float":
    rvs = vis_sys_lv.rotateZ(-phi).rotateY(-theta)
    miss_pt = rvs.pt
    mcorr = np.sqrt(visM**2 + miss_pt**2) + miss_pt
    
    return mcorr
    
#@nb.njit
def calc_ltime(
    vis_sys_lv:"four-vector",
    pointing:"three-vector, not unit",
)->"float":
    ltime = vis_sys_lv.mass * (pointing @ vis_sys_3v) / vis_sys_3v.mag2  
    c_light =  2.99792458e+2 # same cval as gaudipython but e2 instead of e8
    ltime /= c_light
    
    return ltime

def sanity_plot(
    nom_var:"var in tuple",
    man_var:"var computed manually",
    df:"control dataframe",
    NBODY:"b-hadron branch name",
    truth_match=False,
    **kwargs
    )->"plot":
    """sanity check that variables are sensible"""
    fig, ax = plt.subplots()
    ax.hist(
        df[f"{NBODY}_{nom_var}"],
        histtype="stepfilled",
        color="tab:blue", alpha = .5,
        label = "DTT",
        **kwargs
    )
    ax.hist(
        man_var,
        histtype="step",
        color="firebrick", lw = 1.5,
        label = "Blaise",
        **kwargs
    )
    ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
    ax.set_xlabel(nom_var.replace("_",""))
    if truth_match is False:
        ax.set_ylabel("Candidates, pending truth-matching")
    if truth_match is True:
        ax.set_ylabel("Candidates, truth-matched")
    ax.legend()
    plt.savefig(f"scratch/check_{nom_var}.png")

# implement Greg's truthmatching 
# note: numba does not seem to work with this
# might be worth replacing with masking with subindex
def truth_match(
    target_pids:"skhep.Particle PID list",
    events:"events",
    NBODY:"(TwoBody|ThreeBody)"
    )->"dataframe":
    pid_array = events[f"{opts.nbody}_DAUGHTERS"].array()
    breakpoint()
    #print(df[f"{NBODY}_DAUGHTERS"])
    matching = [] # container of events with correct number of candidates
    for i in range(len(pid_array)): 
        match = (np.array_equiv(pid_array[i] , target_pids)) # consistent shape & content check
        matching.append(match) # save bool index 
        #print(pid_array[i], match)

    print(c(f"Total Events in sample: {len(matching)}, matched: {sum(matching)}").yellow)
    matching = pd.DataFrame(matching).rename(columns={0: f"{NBODY}_FromSignalB"})
    assert(len(matching)==events.num_entries)

    return matching

def assign_truth(
        matching:"df",
        df:"df",
        NBODY:"(TwoBody|ThreeBody)"
    )->"MC df":
    """once we have the truth, save it with appropriate index"""
    matching = matching.reindex(df.index, level=0)
    df[f"{NBODY}_FromSignalB"] = matching[f"{NBODY}_FromSignalB"]

    return df 

def pid_plot(
    nom_var:"var in tuple",
    truth_var:"truth_variable",
    df:"control dataframe",
    NBODY:"b-hadron branch name",
    target_pids:"set the dimension",   
    channel:"string",
    **kwargs   
)->"plot":
    for i in range(0,len(target_pids),1):

        particle_level = df[f"{NBODY}_{nom_var}"].xs(i, axis=0, level=1, drop_level=False)
        particle_level = particle_level.replace({20413.0:"D1(H)+", 43.0:"Xu0", 104124.0:"Lambda_c(2625)+", 44.0:"Xu+", 4432.0:"Omega_cc+", 4434.0:"Omega*_cc+"}) #manually add particle that is not in the particle package
        for particle in particle_level.unique():
            try:
                particle_level = particle_level.replace({particle : Particle.from_pdgid(particle).name}) #skip particles not in the package
            except Exception:  # for some reason ValueError does not catch all errors
                particle_level = particle_level.replace({particle : str(particle)}) # count how many times a dodgy entry occurs instead of just pass. Errors may go unscrutinised otherwise.

        daughters = df[f"{NBODY}_{nom_var}"]
        truth = df[f"{NBODY}_{truth_var}"]
        selected_particle_level = (daughters[truth == True]).xs(i, axis=0, level=1, drop_level=False)

        selected_particle_level = selected_particle_level.replace({20413.0:"D1(H)+",  43.0:"Xu0", 104124.0:"Lambda_c(2625)+", 44.0:"Xu+", 4432.0:"Omega_cc+", 4434.0:"Omega*_cc+"}) #manually add particle that is not in the particle package
        for particle in selected_particle_level.unique():
            try:
                selected_particle_level = selected_particle_level.replace({particle : Particle.from_pdgid(particle).name}) #skip particles not in the package
            except ValueError: 
                print(c("WARNING: dodgy selected PID").yellow.on_red)
                continue # here we just flag this, but we don't expect anything
     
        fig, ax = plt.subplots(figsize=(30, 15))
        ax.bar(particle_level.value_counts(sort=False).index, particle_level.value_counts(sort=False).values, align="center", color="deeppink", alpha=0.7, label=f"Particle{i} in Sample")
        ax.bar(selected_particle_level.value_counts(sort=False).index, 
            selected_particle_level.value_counts(sort=False).values, 
            align="center", 
            facecolor=None, edgecolor="tab:blue", hatch="\\", fill=False, 
            label=f"Truth Matched Particle{i}"
        )
        ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
        plt.xticks(
            rotation=45,
            horizontalalignment='right',
            fontweight='light',
            fontsize='x-large'
        )
        plt.legend()
        
        # create mode-specific dir
        pathlib.Path(f"scratch/{channel}/plots").mkdir(parents=True, exist_ok=True)
        plt.savefig(f"scratch/{channel}/plots/{NBODY}_check_truth_particle{i}.png")
        ax.set_yscale("log")
        plt.savefig(f"scratch/{channel}/plots/{NBODY}_check_truth_particle{i}_log.png")


if __name__ == '__main__':
    parser = ArgumentParser(description="ROOT -> pkl, with obs")
    parser.add_argument('-c','--channel', required=True,
        choices = list(channels.keys()),
        help="Signal MC channel")
    parser.add_argument('-b','--nbody', required=True, choices=["TwoBody", "ThreeBody"],
        help="n-body implementation ['TwoBody', 'ThreeBody']")
    parser.add_argument('-f','--features',  default=f"{__mainpath__}/standalone_inference/boi.yml",
        help="features of the model")
    parser.add_argument('-w','--skip_truth',  action="store_true",
        help="skip truth-matching [default:False]")
    opts = parser.parse_args()

    # load test sample: DsPi ROOT file with placeholder decay descriptor and no truth-matching
    events = uproot.open(
        f"{__rootpath__}/{opts.channel}.root:{opts.nbody}/DecayTree"
    )

    # book truthmatching
    print(c("Commence truthmatching...").cyan)
    target_pids = np.array(channels[f"{opts.channel}"]["truth"]) #all plus because otherwise they take a negative PID value
    breakpoint()
    tm = truth_match( # can import this into the iterate loop, if it's too slow
            target_pids=target_pids,
            events = events,
            NBODY=opts.nbody
        )
    _stop = None # max cands read in
    if _stop is not None: 
        print(c("WARNING: _stop is not None. Matching and data dimensions adjusted accordingly").red.on_yellow)
        tm = tm[:_stop]
        assert(len(tm)==_stop)
    print(c("Truthmatching container complete").cyan.on_blue)


    # curate what to read in
    boi=read_config(opts.features, _nbody=opts.nbody)+[
            f"{opts.nbody}_DAUGHTERS",
            "TwoBody_ENDVERTEX_X",
            "TwoBody_ENDVERTEX_Y",
            "TwoBody_ENDVERTEX_Z",
            "TwoBody_OWNPV_X",
            "TwoBody_OWNPV_Y",
            "TwoBody_OWNPV_Z",
            "TwoBody_PX",
            "TwoBody_PY",
            "TwoBody_PZ",
            "TwoBody_PE",
            "TwoBody_M",
            "Track1_PX",
            "Track1_PY",
            "Track1_PZ",
            "Track1_PE",
            "Track1_M",
            "Track2_PX",
            "Track2_PY",
            "Track2_PZ",
            "Track2_PE",
            "Track2_M",
            "runNumber",
            "eventNumber",
        ]

    if opts.nbody=="ThreeBody": 
        print(c("Detected a 3-body final state. Adding ThreeBody and TrackB observables.").on_yellow)
        boi += [
            "ThreeBody_ENDVERTEX_X",
            "ThreeBody_ENDVERTEX_Y",
            "ThreeBody_ENDVERTEX_Z",
            "ThreeBody_OWNPV_X",
            "ThreeBody_OWNPV_Y",
            "ThreeBody_OWNPV_Z",
            "ThreeBody_PX",
            "ThreeBody_PY",
            "ThreeBody_PZ",
            "ThreeBody_PE",
            "ThreeBody_M",
            "TrackB_PX",
            "TrackB_PY",
            "TrackB_PZ",
            "TrackB_PE",
            "TrackB_M", 
        ]

    # read into df in batches & assign truth
    batch_size = "1 MB"
    bevs = events.num_entries_for(batch_size, entry_stop=_stop)
    tevs = events.num_entries
    nits = round(tevs/bevs+0.5)
    _container = []
    for batch in tqdm(events.iterate( library='pd', step_size=batch_size, entry_stop=_stop ), total=nits,ascii=True,desc=f"Input sig excl MC"):
        _container.append(batch)
    df = pd.concat(_container)

    if opts.skip_truth:
        print(c("WARNING: skipping truth-matching. Proceed at your own risk.").red.bold)

    if not opts.skip_truth:
        df = assign_truth(matching=tm, df=df, NBODY=opts.nbody)
        print(c("Truth info assigned").cyan.on_blue)

        # assert we have read in everything, accounting for multi-index
        if _stop is None: 
            assert(df.index.max()[0] == events.num_entries-1)
        else:
            print(c("Bypassing sanity check: assert(df.index.max()[0] == events.num_entries-1").on_red)
        # routine to check that the truthmatching is correct
        assert(df[df[f"{opts.nbody}_FromSignalB"]==True].index.get_level_values('subentry').max() == len(target_pids)-1)
        print(c("WARNING: no check in place to verify the lower bound of #candidates").magenta.blink)
        print(c("Truth sanity checks passed").white.on_green)

        # sanity plot  
        plt_conf = {
            "df" : df,
            "NBODY" : opts.nbody,
            "bins": 2000
        } 
        pid_plot(nom_var = "DAUGHTERS",truth_var="FromSignalB", 
            target_pids=target_pids, channel=opts.channel,
            **plt_conf)

    # b-hadron PV
    pv = build_3vec( pX = df[f"{opts.nbody}_OWNPV_X"], 
        pY = df[f"{opts.nbody}_OWNPV_Y"], pZ = df[f"{opts.nbody}_OWNPV_Z"])
    
    # b-hadron decay vertex
    dv = build_3vec(pX = df[f"{opts.nbody}_ENDVERTEX_X"], 
        pY = df[f"{opts.nbody}_ENDVERTEX_Y"], pZ = df[f"{opts.nbody}_ENDVERTEX_Z"])

    # pointing verctor & related obs
    pointing = dv - pv
    pointing_u = pointing.unit() # unit vector
    assert(pointing_u.mag.all()==1.)

    theta = pointing_u.theta
    phi = pointing_u.phi
    #pointing, pointing_u, theta, phi = calc_pointing_obs

    # visible system (b-hadron reconstructed childer) 4-vec
    vis_sys_lv = build_4vec( pX = df[f"{opts.nbody}_PX"], pY = df[f"{opts.nbody}_PY"],
        pZ = df[f"{opts.nbody}_PZ"], pE = df[f"{opts.nbody}_PE"])
    # spatial info only
    vis_sys_3v = build_3vec(pX = vis_sys_lv.x,
        pY = vis_sys_lv.y, pZ = vis_sys_lv.z) 
    eta = vis_sys_lv.eta

    # angle wrt to pointing: dot product / (product of magnitudes)
    dira = calc_DIRA(vis_sys_3v, pointing_u)

    # corrected mass
    rvs = vis_sys_lv.rotateZ(-phi).rotateY(-theta)
    miss_pt = rvs.pt
    df[f"{opts.nbody}_Mcorr"] = calc_mcorr(vis_sys_lv, phi, theta, 
        df[f"{opts.nbody}_M"])
    
    # === THIS NEEDS TO BE EXTENDED TO DALITZ & Q2 ===
    # # check operation of adding lorentz vectors and getting the correct mass of the system
    t1 = build_4vec(df.Track1_PX, df.Track1_PY, df.Track1_PZ, df.Track1_PE)
    t2 = build_4vec(df.Track2_PX, df.Track2_PY, df.Track2_PZ, df.Track2_PE)
    vism = (t1 + t2).mass
    
    # attempt to conventional dalitz
    if opts.nbody=="ThreeBody":
        print(c("Computing Dalitz observables m12 m23").bold)
        tB = build_4vec(df.TrackB_PX, df.TrackB_PY, df.TrackB_PZ, df.TrackB_PE)
        vism12 = (t1 + t2).mass
        vism2B = (t2 + tB).mass

        # assign to df
        df["M12"] = vism12; assert(df.M12.min()>0)
        df["M2B"] = vism2B; assert(df.M2B.min()>0)
    # ================================================

    # sanity check
    plt_conf = {
        "df" : df,
        "NBODY" : opts.nbody,
        "bins" : 100,
    }
    #sanity_plot(nom_var="MCORR", man_var=mcorr, **plt_conf)
    sanity_plot(nom_var="M", man_var=vis_sys_lv.mass, **plt_conf)

    # example of how to get q2 evaluation -> try for rare decays for the time being, ie the inv mass of the dilepton system, squared
    # fill code here

    # # attempt to compute lifetime; this seems to not work - tried to replicate the MCKinematicTT
    #mc_4mom = build_4vec(
    #    df["B_plus_TRUEP_X"],
    #    df["B_plus_TRUEP_Y"],
    #    df["B_plus_TRUEP_Z"],
    #    df["B_plus_TRUEP_E"]
    #)
    # spatial info only
    #mc_3v = vector.array({
    #    "x" : mc_4mom.x,
    #    "y" : mc_4mom.y,
    #    "z" : mc_4mom.z,
    #});
    #mc_dv = build_3vec(
    #    df["B_plus_TRUEENDVERTEX_X"],
    #    df["B_plus_TRUEENDVERTEX_Y"],
    #    df["B_plus_TRUEENDVERTEX_Z"],
    #)
    #mc_ov = build_3vec(
    #    df["B_plus_TRUEORIGINVERTEX_X"],
    #    df["B_plus_TRUEORIGINVERTEX_Y"],
    #    df["B_plus_TRUEORIGINVERTEX_Z"],
    #)
    #mc_pointing = mc_dv - mc_ov

    df[f"{opts.nbody}_TAU"] = calc_ltime(vis_sys_lv, pointing)
    df = df[df[f"{opts.nbody}_TAU"]>0] # positive lifetime
    assert(df[f"{opts.nbody}_TAU"].min()>0)
    
    #ch_4vev = build_4vec(
    #    df.D0_PX,
    #    df.D0_PY,
    #    df.D0_PZ,
    #    df.D0_PE
    #)
    # not sure how to proceed to get q2

    #sanity_plot(nom_var="TAU", man_var=ltime, **plt_conf,
    #        range=[0, 0.01],
    #    )

    pathlib.Path("scratch").mkdir(parents=True, exist_ok=True)
    print(c(f"SUCCESS: truth-matched pkl file written to: scratch/{opts.channel}/{opts.nbody}_calcobs.pkl").green.underline)
    df.to_pickle(f"scratch/{opts.channel}/{opts.nbody}_calcobs.pkl")