from builtins import breakpoint
import os
import glob
from argparse import ArgumentParser

parser = ArgumentParser(description="training configuration")
parser.add_argument('-b','--nbody',     
    choices=["ThreeBody", "TwoBody"], required=True,
    help="n-body implementation [choices:('ThreeBody','TwoBody')]")    
opts = parser.parse_args()

html = """<!DOCTYPE html>
<html>
<head>
<style>
* {
  box-sizing: border-box;
}

.img-container {
  float: left;
  width: 33.33%;
  padding: 5px;
}

.clearfix::after {
  content: "";
  clear: both;
  display: table;
}
</style>
</head>
<body>

<h2>Monotonic Robust Topological NN</h2>
<p><a href="https://gitlab.cern.ch/nschulte/topo_neural_network/-/tree/preprocess_optim">https://gitlab.cern.ch/nschulte/topo_neural_network/-/tree/preprocess_optim</a></p>

"""

PARENT_DIR = f"exec/{opts.nbody}"
PREP_DIR = "plots"
lambdas = os.listdir(PARENT_DIR)

html+="""
<h2>Feature distributions</h2>""" 

distrs = [file for file in os.listdir(f"{PREP_DIR}/pre/{opts.nbody}/dist") if file.endswith('.png')]
for d in distrs:
    html+=f"""
        <img src='{PREP_DIR}/pre/{opts.nbody}/dist/{d}' alt='{d}' style='width:50%'>
    """

# correlation among training variables
sig_corr_plot = f"{PREP_DIR}/pre/{opts.nbody}/corr/sig.png" 
bkg_corr_plot = f"{PREP_DIR}/pre/{opts.nbody}/corr/minbias.png"

html+="""<div class="clearfix">
<h2>Training features correlations</h2>""" 
html+=f"""<div class='img-container'>
    <img src='{sig_corr_plot}' alt='sig_corr' style='width:150%'>
    <figcaption>Inclusive beauty</figcaption>
  </div>
  <div class='img-container'>
    <img src='{bkg_corr_plot}' alt='bkg_corr' style='width:150%'>
    <figcaption>Mininum bias</figcaption>
  </div>
</div>

"""

html+=f"""<h2>Results of the training</p>"""
for l in lambdas:
    html+=f"""<div class='clearfix'>
    <h4>Lipschitz constant: {l.replace('lambda','')}</h4>""" 
    
    pt = f"{PARENT_DIR}/{l}/train/plots/eff_{opts.nbody}_PT.png"
    fd = f"{PARENT_DIR}/{l}/train/plots/eff_{opts.nbody}_FDCHI2_OWNPV.png"
    roc = f"{PARENT_DIR}/{l}/train/plots/roc_per_combination.png"

    html+=f"""<div class='img-container'>
    <img src='{pt}' alt='PT' style='width:100%'>
  </div>
  <div class='img-container'>
    <img src='{fd}' alt='FD' style='width:100%'>
  </div>
  <div class='img-container'>
    <img src='{roc}' alt='ROC' style='width:100%'>
  </div>
</div>

"""

# unconstr_lambdas = os.listdir("exec_unconstr/TwoBody")
# html+=f"""<h2>Results of the training [unconstrained network]</p>"""
# for l in unconstr_lambdas:
#     html+=f"""<div class='clearfix'>
#     <h4>Lipschitz constant: {l.replace('lambda','')} [dummy placeholder]</h4>""" 
    
#     pt = f"exec_unconstr/TwoBody/{l}/train/plots/eff_TwoBody_PT.png"
#     fd = f"exec_unconstr/TwoBody/{l}/train/plots/eff_TwoBody_FDCHI2_OWNPV.png"
#     roc = f"exec_unconstr/TwoBody/{l}/train/plots/roc_per_combination.png"

#     html+=f"""<div class='img-container'>
#     <img src='{pt}' alt='PT' style='width:100%'>
#   </div>
#   <div class='img-container'>
#     <img src='{fd}' alt='FD' style='width:100%'>
#   </div>
#   <div class='img-container'>
#     <img src='{roc}' alt='ROC' style='width:100%'>
#   </div>
# </div>

# """

html+="""

</body>
</html>
"""

html+="""
<h2>Appendix</h2>

<h2>Preprocessing enacted</h2>""" 



html+=f"""
    <img src='{PREP_DIR}/preprocess/{opts.nbody}/train_Mscaled.png' alt='{d}' style='width:100%'>
    <figcaption>Training sample (preprocessed separately from test)</figcaption>
"""


# html+="""<h2>2D histograms</h2>""" 

# distrs = [file for file in os.listdir(f"{PREP_DIR}/pre/2d") if file.endswith('.png')]
# for d in distrs:
#     html+=f"""
#         <img src='{PREP_DIR}/pre/2d/{d}' alt='{d}' style='width:50%'>
#     """

with open("index.html", "w") as f:
    print(html, file=f)
