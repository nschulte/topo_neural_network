"""Train/Test viz of BDT/NN training"""

__author__ = "Blaise Delaney & Nicole Schulte"
__email__  = "blaise.delaney@cern.ch"

from cgi import test
import os, yaml
from argparse import ArgumentParser
from pathlib import Path
import torch 
import numpy as np
import pandas as pd
import time
import pickle
from torch.utils.data import TensorDataset, DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn import functional as F
from torch.nn.functional import binary_cross_entropy_with_logits
from sklearn.metrics import roc_auc_score 
import matplotlib.pyplot as plt
import sklearn.metrics
from monotonenorm import SigmaNet, GroupSort, direct_norm
from hist import Hist
import hist
from mpl_config import *

parser = ArgumentParser(description="training configuration")
parser.add_argument('-f','--features',  
    default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/features.yml", 
    help="features of the model")    
parser.add_argument('-o','--outdir',    
    default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/exec",  required=False, 
    help='directory of sh files for train/eval [default: exec]')
parser.add_argument('-b','--nbody',     required=True, choices=["TwoBody", "ThreeBody"], 
    help="n-body implementation ['TwoBody', 'ThreeBody']")    
parser.add_argument('-I','--id_map',   
    default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/id_name_map.yml",
    help="signal ID map path")   
parser.add_argument('-R','--train_results',   required=True, 
    help="path to results from the training")
opts = parser.parse_args()

# decorator to check input path exists
def check_argpath(func):
   def wrapper(feats_path, **kwargs):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path)
        return features 
   return wrapper

# read in 2-/3- body feature list
@check_argpath
def read_config(
    feats_path:str
)->"list":
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, 'r') as stream:  
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    
    try:
        opts.nbody in in_dict
        feature_list = in_dict[opts.nbody]
    except ValueError:
        print("'features' key not in dict")
    
    return feature_list

@check_argpath
def read_id_map(
    id_path:str,
)->"dict":
    """map id names to MC ID"""
    with open(id_path, 'r') as stream:  
        id_dict = yaml.load(stream, Loader=yaml.FullLoader)
    
    return id_dict

def to_hist(
    data:"list/array",
    edges:"variable edges",
    weights=None,
    maxNorm=False, # normalise by max bin
    sumNorm=False, # normalise by sum(bins)
)->"hist":
    """returns: hist, edges, centers, bin height, bin err"""
    if weights is None:
        h = Hist( hist.axis.Variable(edges, name="obs") )
        h.fill( data )
        nh  = h.view()
        err = h.view()**0.5
    if weights is not None:
        h = Hist( hist.axis.Variable(edges, name="obs"), storage=hist.storage.Weight())
        h.fill( data, weight = weights )
        nh  = h.view().value
        err = h.view().variance**0.5

    xe  = h.axes[0].edges
    cx  = h.axes[0].centers
    frac_err = err/nh
    
    if maxNorm: 
        integral = np.max(nh)
        nh = nh/integral
        err = err/integral
        scaled_frac_err = err/nh
        assert(np.max(nh)==1)
        assert(frac_err.all() == scaled_frac_err.all())
    
    if sumNorm:
        integral = np.sum(nh)
        nh = nh/integral
        err = err/integral
        scaled_frac_err = err/nh
        assert(frac_err.all() == scaled_frac_err.all())

    return h, xe, cx, nh, err

def std_eff(
    _pass:"integrer or array of integers",
    _tot:"integrer or array of integers"
)->"integrer or array of integers":
    
    # from Paterno, sec 2.2 - https://lss.fnal.gov/archive/test-tm/2000/fermilab-tm-2286-cd.pdf
    return (1/_tot) * np.sqrt( _pass*(1-_pass/_tot) )

def plot_obs_eff(df, branch, _edges, _xlabel, _label=1, outputpath=None): 

    sig = df[df.label==_label]
    EDGES = _edges+[df[branch].max()]
    
    fig, ax = plt.subplots()

    H, xe, cx, nh, err = to_hist(
        data = sig[branch].to_numpy(),
        edges=EDGES,
        maxNorm=True
    )

    
    ax2 = ax.twinx()
    ax2.set_ylabel("Arbitrary Units", color="grey")
    ax2.tick_params(axis='y', colors='grey', which="both")
    hep.histplot(
        nh, 
        bins=xe, 
        ax=ax2,
        color="lightgrey",
        alpha=.75,
        histtype="fill"
    )

    cosmetics = {
        "0.50" : {
            "color": "black",
            "fmt"  : ".",
            "label": r"$0.50$",
            "markersize": 8,
        },
        "0.75" : {
            "color": "#1a9850",
            "fmt"  : "2",
            "label": r"$0.75$",
            "markersize": 9,
        },
        "0.90" : {
            "color": "tab:red",
            "fmt"  : "^",
            "label": r"$0.90$",
            "markersize": 5,
        },
        "0.99" : {
            "color": "tab:blue",
            "fmt"  : "x",
            "label": r"$0.99$",
            "markersize": 8,
        },
    }

    for nncut in list(cosmetics.keys()):
        h_sel, _, _, _, _ = to_hist(
            data = sig[sig.preds_per_cand>float(nncut)][branch].to_numpy(),
            edges=EDGES
        )

        ax.errorbar(
            x = H.axes[0].centers,
            # NaN mapped to 0
            y = np.nan_to_num(h_sel.project("obs").view() / H.project("obs").view()),
            xerr = H.axes[0].widths/2.,
            yerr = std_eff(
                _pass = h_sel.project("obs").view(),
                _tot = H.project("obs").view() 
            ),
            elinewidth=1.5,
            capsize=3, 
            markeredgewidth=1.5,
            **cosmetics[nncut]
        )
    ax.set_zorder(ax2.get_zorder()+1)
    ax.patch.set_visible(False) 
    ax.axhline(1.0, color="darkorange", ls="--", lw=1)
    ax.set_xlabel(_xlabel)
    ax.set_ylabel("Efficiency Ratio")
    ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
    ax2.set_title(r"$\lambda = %s$"%(_lambda), loc="right", fontsize=25)
    ax.legend(fontsize=23)

    ax.set_ylim(bottom=0, top=1.05)
    ax2.set_ylim(bottom=0, top=1.05)
    plt.savefig(f"{outputpath}_{branch}.png")
    plt.savefig(f"{outputpath}_{branch}.pdf")

def plot_eff_prompt_charm(df, _label=1, outputpath=None): 

    sig = df[df.label==_label]
    prompt_charm = sig.query(
            f"{opts.nbody}_FromSameB==False and {opts.nbody}_FromSameD==1" # not a beauty grandmother
        )

    # denominators
    _tot = len(sig)
    _tot_prompt_charm = len(prompt_charm)
    
    # containers
    sig_effs = []
    sig_errs = []
    pch_effs_c = []
    pch_errs_c = []
    
    NNcuts = np.array([0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99])
    for nncut in NNcuts:
        # total sig effs
        _pass = len(sig[sig.preds_per_cand>nncut])
        eff = _pass/_tot
        err = std_eff(_tot=_tot, _pass=_pass)
        sig_effs.append(eff)
        sig_errs.append(err)

        # prompt charm effs 
        try:
            pch_pass = len(prompt_charm[prompt_charm.preds_per_cand>nncut])
            pch_eff = pch_pass / _tot_prompt_charm
            pch_err = std_eff(_tot=_tot_prompt_charm, _pass=pch_pass)
            pch_effs_c.append(pch_eff)
            pch_errs_c.append(pch_err)
        except:
            pch_effs_c.append(0)
            pch_errs_c.append(0)

    fig, ax = plt.subplots()

    ax.errorbar(
        x = NNcuts,
        y = sig_effs,
        #xerr = (NNcuts[:-1] + NNcuts[1:])/2.,
        yerr = sig_errs,
        elinewidth=1.5,
        capsize=3, 
        markeredgewidth=1.5,
        color= "tab:blue",
        fmt=".",
        markersize= 8,
        label="Inclusive Beauty" 
    )

    ax.errorbar(
        x = NNcuts,
        y = pch_effs_c,
        yerr = pch_errs_c,
        elinewidth=1.5,
        capsize=0, 
        markeredgewidth=0,
        color= "tab:blue",
        fmt=".",
        markersize= 0., 
    )

    ax.bar(
        x = NNcuts,
        height = pch_effs_c,
        #width = (NNcuts[:-1] + NNcuts[1:])/2.,
        color="tab:red",
        alpha=.75,
        label="Prompt Charm"
    )


    ax.axhline(1.0, color="darkorange", ls="--", lw=1)
    ax.set_xlabel("NN response cut [Arbitrary Units]")
    ax.set_ylabel("Efficiency Ratio")
    ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
    ax.set_title(r"$\lambda = %s$"%(_lambda), loc="right", fontsize=25)
    ax.legend(fontsize=23)

    ax.set_ylim(bottom=0, top=1.05)
    plt.savefig(f"{outputpath}_prompt_charm.png")
    plt.savefig(f"{outputpath}_prompt_charm.pdf")


def plot_roc(truth, preds, outputpath=None):
    roc_auc = sklearn.metrics.roc_auc_score(truth, preds)
    fpr, tpr, _ = sklearn.metrics.roc_curve(truth, preds)
    fig, ax = plt.subplots()
    ax.plot(fpr, tpr, label=f'ROC AUC: {roc_auc:.3f}')
    ax.plot([0, 1], [0, 1], color='grey', linestyle='--', label='Random choice')
    ax.set_xlabel('False positive rate')
    ax.set_ylabel('True positive rate')
    ax.set_xlim(right=1)
    ax.set_ylim(bottom=0)
    ax.set_xticks(np.arange(0, 1, 0.05), minor=True)
    ax.set_yticks(np.arange(0, 1, 0.05), minor=True)
    ax.grid(linestyle='--')
    ax.legend(loc='lower right')
    if outputpath:
        plt.savefig(outputpath + '.pdf')
        plt.savefig(outputpath + '.png')
    plt.close()

def plot_loss(train_loss, test_loss, outputpath=None):
    fig, ax = plt.subplots()
    ax.plot(Train_loss_array, color="dodgerblue", label="Training")
    ax.plot(Test_loss_array, color="deeppink", label="Testing")
    ax.set_xlabel('Epoch')
    ax.set_ylabel('Loss')
    ax.legend(loc='best')
    if outputpath:
        #!mkdir outputpath
        plt.savefig(outputpath + 'loss.pdf')
        plt.savefig(outputpath + 'loss.png')
    plt.close()

def plot_response(predictions, outputpath=None):
    plt.hist(predictions[predictions.label == 1].preds_per_cand, alpha=0.5, bins=100, log=True, label="Signal Predictions")
    plt.hist(predictions[predictions.label == 0].preds_per_cand, alpha=0.5, bins=100, log=True, label="Background Predictions")
    plt.legend(loc='best')
    if outputpath:
        plt.savefig(outputpath + '.pdf')
        plt.savefig(outputpath + '.png')
    plt.close()


test_sample = pd.read_pickle(f"{opts.train_results}")
print("Sucessfully imported Dataframes")
_,_, _lambda, _ , _ = os.path.normpath(f"{opts.train_results}").split(os.sep)
_lambda = _lambda.replace("lambda", "")



main_path = os.path.dirname(opts.train_results)
Parameterfile = open(f"{main_path}/{opts.nbody}_performance_parameters.npy", "rb")
print("Unpacking Numpy Arrays")
params=np.load(Parameterfile, allow_pickle=True)
print("Unpacking complete")
Parameterfile.close()
print("Successfully loaded Parameterfile")

Train_loss_array = params[0]
Test_loss_array = params[1]
ROC_Train_array = params[2]
ROC_Test_array = params[3]
ACC_Test = params[4]
ACC_train = params[5]

train_features = read_config(opts.features)
event_ids = ['runNumber', 'eventNumber']
label_name = [f"{opts.nbody}_fromSignalB"]
same_Bs = ["{opts.nbody}_FromSameB"]
multiplicity = [f"{opts.nbody}_nCharged", f"{opts.nbody}_nNeutral"]
id_name_map = read_id_map(opts.id_map)


print('plotting loss')
plot_loss(Train_loss_array, Test_loss_array, 
    outputpath = f"{main_path}/plots/"
    )
print('plotting response')
plot_response(test_sample, 
    outputpath = f"{main_path}/plots/response"
    )
print('plotting effcies')

if opts.nbody=="ThreeBody": upper_PT = 20
if opts.nbody=="TwoBody": upper_PT   = 14
eff_obs = {
    f"{opts.nbody}_FDCHI2_OWNPV" : {
        "EDGES" : list(np.arange(2, 14, 1)),
        "xlabel" : r"log(Flight Distance $\chi^2$) [Arbitrary Units]"
    },
    f"{opts.nbody}_PT" : {
        "EDGES" : list(np.arange(2, upper_PT, 1)),
        "xlabel" : r"Beauty $p_{T}$ [GeV$/c$]"
    },
}

for p in eff_obs.keys():
    plot_obs_eff(
        df=test_sample,  
        _edges = eff_obs[p]["EDGES"],
        branch=p,
        outputpath = f"{main_path}/plots/eff",
        _xlabel = eff_obs[p]["xlabel"],
        )

plot_eff_prompt_charm(
    df = test_sample,
    outputpath = f"{main_path}/plots/eff",
)

#per candidate
print('per candidate test set')
plot_roc(test_sample.label, test_sample.preds_per_cand,
    outputpath = f"{main_path}/plots/roc_per_combination"
    )

# per event
print('per event test set')
plot_roc(test_sample.groupby('unique_event').label.max(), test_sample.groupby('unique_event').preds_per_cand.max(),
    outputpath = f"{main_path}/plots/roc_per_event"
)


# for name in test_sample.name.unique():
#     try:
#         print(name)
#         pe_x = test_sample[(((test_sample.name == name) & (test_sample.event_label == 1)) | ((test_sample.name == 'minbias') & (test_sample.event_label == 0)))]
#         #per event
#         print('per event')
#         plot_roc(pe_x.groupby('unique_event').label.max(), pe_x.groupby('unique_event').preds_per_cand.max(),
#         outputpath = f"{main_path}/plots/{opts.nbody}_"+name+"_per_event_Unconstrained")
#     except:
#         pass


# for name in test_sample.name.unique():
#     try:
#         print(name)
#         pe_x = test_sample[(((test_sample.name == name) & (test_sample.event_label == 1)) | ((test_sample.name == 'minbias') & (test_sample.event_label == 0)))]
#         # per candidate
#         print('per candidate')
#         plot_roc(pe_x.label, pe_x.preds_per_cand,
#         outputpath = f"{main_path}/plots/{opts.nbody}_"+name+"_per_combination_Unconstrained")
#     except:
#         pass