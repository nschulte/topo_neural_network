"""Utility functions commonly used in the pipeline"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

import pandas as pd
import pickle
import time
import uproot
import numpy as np
from argparse import ArgumentParser
import matplotlib.pyplot as plt
from pathlib import Path
import yaml

# decorator to check input path exists
def check_argpath(func):
   def wrapper(feats_path, **kwargs):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path, **kwargs)
        return features 
   return wrapper

# read in 2-/3- body feature list
@check_argpath
def read_config(
    feats_path:str,
    _nbody:str
)->"list":
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, 'r') as stream:  
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    
    try:
        _nbody in in_dict
        feature_list = in_dict[_nbody]
    except ValueError:
            print("'features' key not in dict")
    
    return feature_list

@check_argpath
def read_id_map(
    id_path:str,
)->"dict":
    """map id names to MC ID"""
    with open(id_path, 'r') as stream:  
        id_dict = yaml.load(stream, Loader=yaml.FullLoader)
    
    return id_dict

plt_config_TwoBody = {
    "min_PT_final_state_tracks": {
        "range" : [0, 5],
        "xlabel" : "$p_T^{min}$ [GeV$/c$]"
    },
    "sum_PT_final_state_tracks": {
        "range" : [0, 15],
        "xlabel" : "$\sum{p_T}$ [GeV$/c$]"
    },
    "min_FS_IPCHI2_OWNPV": {
        "range" : [0, 15],
        "xlabel" : "log(IP $\chi^2_{min}$) [Arbitrary Units]"
    },
    "max_FS_IPCHI2_OWNPV": {
        "range" : [0, 15],
        "xlabel" : "log(IP $\chi^2_{max}$) [Arbitrary Units]"
    },
    "TwoBody_ENDVERTEX_DOCAMAX": {
        "range" : [0, 5],
        "xlabel" : "DOCA$_{max}$ [cm]"
    },
    "TwoBody_ENDVERTEX_CHI2": {
        "range" : [0, 10],
        "xlabel" : "$B$ $\chi^2_{vtx}$ [Arbitrary Units]"
    },
    "TwoBody_FDCHI2_OWNPV": {
        "range" : [0, 17],
        "xlabel" : "log($B$ FD $\chi^2$) [Arbitrary Units]"
    },
    "TwoBody_Mcorr": {
        "range" : [0, 25],
        "xlabel" : "$m_{corr}$ [GeV$/c^2$]"
    },
    "TwoBody_PT": {
        "range" : [0, 15],
        "xlabel" : "$p_T(B)$ [GeV$/c$]"
    },
}

plt_config_ThreeBody = {
    "min_PT_final_state_tracks": {
        "range" : [0, 5],
        "xlabel" : "$p_T^{min}$ [GeV$/c$]"
    },
    "max_PT_final_state_tracks": {
        "range" : [0, 8],
        "xlabel" : "$p_T^{max}$ [GeV$/c$]"
    },
    "sum_PT_final_state_tracks": {
        "range" : [0, 16],
        "xlabel" : "$\sum{p_T}$ [GeV$/c$]"
    },
    "min_PT_TRACK12": {
        "range" : [0, 5],
        "xlabel" : "$p_{T,12}^{min}$ [GeV$/c$]"
    },
    "max_PT_TRACK12": {
        "range" : [0, 8],
        "xlabel" : "$p_{T,12}^{max}$ [GeV$/c$]"
    },
    "sum_PT_TRACK12": {
        "range" : [0, 15],
        "xlabel" : "$\sum{p_T}_{12}$ [GeV$/c$]"
    },
    "min_FS_IPCHI2_OWNPV": {
        "range" : [0, 5],
        "xlabel" : "log(IP $\chi^2_{min}$) [Arbitrary Units]"
    },
    "max_FS_IPCHI2_OWNPV": {
        "range" : [0, 14],
        "xlabel" : "log(IP $\chi^2_{max}$) [Arbitrary Units]"
    },
    "TwoBody_IPCHI2_OWNPV": {
        "range" : [-2, 15],
        "xlabel" : "log($X$ IP $\chi^2_{max}$) [Arbitrary Units]"
    },
    "ThreeBody_IPCHI2_OWNPV": {
        "range" : [-2, 15],
        "xlabel" : "log($B$ IP $\chi^2_{max}$) [Arbitrary Units]"
    },
    "TwoBody_ENDVERTEX_DOCAMAX": {
        "range" : [0, 6],
        "xlabel" : "DOCA$_{max}^{X}$ [cm]"
    },
    "ThreeBody_ENDVERTEX_DOCAMAX": {
        "range" : [0, 8],
        "xlabel" : "DOCA$_{max}^{B}$ [cm]"
    },
    "TwoBody_ENDVERTEX_CHI2": {
        "range" : [0, 10],
        "xlabel" : "$X$ $\chi^2_{vtx}$ [Arbitrary Units]"
    },
    "ThreeBody_ENDVERTEX_CHI2": {
        "range" : [-10, 15],
        "xlabel" : "log($B$ $\chi^2_{vtx}$) [Arbitrary Units]"
    },
    "TwoBody_FDCHI2_OWNPV": {
        "range" : [0, 17],
        "xlabel" : "log($X$ FD $\chi^2$) [Arbitrary Units]"
    },
    "ThreeBody_FDCHI2_OWNPV": {
        "range" : [0, 17],
        "xlabel" : "log($B$ FD $\chi^2$) [Arbitrary Units]"
    },
    "TwoBody_Mcorr": {
        "range" : [0, 25],
        "xlabel" : "$m_{corr}^{X}$ [GeV$/c^2$]"
    },
    "ThreeBody_Mcorr": {
        "range" : [0, 25],
        "xlabel" : "$m_{corr}$ [GeV$/c^2$]"
    },
    "TwoBody_PT": {
        "range" : [0, 15],
        "xlabel" : "$p_T(X)$ [GeV$/c$]"
    },
    "ThreeBody_PT": {
        "range" : [0, 15],
        "xlabel" : "$p_T(B)$ [GeV$/c$]"
    },
}