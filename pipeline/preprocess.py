"""Implement preprocessing on raw MC"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

from builtins import breakpoint
import pandas as pd
from sklearn.preprocessing import QuantileTransformer
import pickle
import time
import uproot
import numpy as np
from argparse import ArgumentParser
from pathlib import Path
from utils import read_config, read_id_map, plt_config_ThreeBody, plt_config_TwoBody
import matplotlib.pyplot as plt
import yaml
from scipy.stats import median_abs_deviation
from mpl_config import *
from tabulate import tabulate

parser = ArgumentParser(description="training configuration")
parser.add_argument('-f','--features',  default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/features.yml", 
    help="features of the model")    
parser.add_argument('-v','--plots_path',  default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/plots/preprocess", 
    help="path to dir where the preprocessing is visualised")    
parser.add_argument('-i','--input', 
    default="/home/blaised/private/hlt2_topo/topo_neural_network/scratch/pre", 
    help="path to raw signal+bkg MC, pre-processing") 
parser.add_argument('-o','--out_path', default="/home/blaised/private/hlt2_topo/topo_neural_network/scratch/train", 
    help="path to where preprocessed dataframes get written to file") 
parser.add_argument('-b','--nbody', required=True, choices=["TwoBody", "ThreeBody"], 
    help="n-body implementation ['TwoBody', 'ThreeBody']")
parser.add_argument('-P','--preprocess', choices=["Q", "M"], required=True, 
    help="preprocessing mode ['Q','M']s: Q:quantile, M:scaling+centering around mean within a window")
parser.add_argument('-I','--id_map',   default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/id_name_map.yml",
    help="signal ID map path")
parser.add_argument('-C','--not_constrain', action="store_true",
    help="impose a retention window in the variables preseved in the preprocessing? [default:False]")
opts = parser.parse_args()

#Use this part if you need to upload at the data from Root File. Takes longer
def dataframe(path,name, columns=None, **kwargs):
        f = uproot.open(path)
        if name != 'minbias':
            t = f[f"{opts.nbody}"]
        else:
            t = f[f"{opts.nbody}/DecayTree"]
        df = t.pandas.df(columns or '*', **kwargs)
        return df

def constrain(
    feat:"feature/column/branch",
    n_sigma=5, #"+/- n std dev of retention window",
)->"dataframe[features]":
    """retain if falling within a window about mean, post scaling / logging if needed"""

    mean = np.mean(feat)
    std  = np.std(feat)
    
    retained = feat[(feat>=mean-n_sigma*std) & (feat<=mean+n_sigma*std)]
    feat[(feat<mean-n_sigma*std)] = np.min(retained)
    feat[(feat>mean+n_sigma*std)] = np.max(retained)
    
    return feat

event_ids = ['runNumber', 'eventNumber']
label_name = [F"{opts.nbody}_fromSignalB"]
same_Bs = [f"{opts.nbody}_FromSameB"]
same_Ds = [f"{opts.nbody}_FromSameD"]
multiplicity = [f"{opts.nbody}_nCharged", f"{opts.nbody}_nNeutral"]
train_features=read_config(opts.features, _nbody=opts.nbody)
id_name_map = read_id_map(opts.id_map)

# run on the slimmed-down versions
print("Reading in un-transformed train/test dataframes...")
train_sample = pd.read_pickle(f"{opts.input}/{opts.nbody}_Train.pkl")
test_sample  = pd.read_pickle(f"{opts.input}/{opts.nbody}_Test.pkl")
print("SUCCESS") 

# preprocessing segment
Path(f"{opts.plots_path}").mkdir(parents=True, exist_ok=True)

if opts.preprocess=="Q":
    print("preprocessing with QUANTILE transform...")
    #Scale the Data to a Gaussian
    scaler = QuantileTransformer(n_quantiles=1000,
                                output_distribution="normal")
    scaler.fit(train_sample[train_features])
    train_sample[train_features] = scaler.fit_transform(train_sample[train_features])

    scaler = QuantileTransformer(n_quantiles=1000,
                                output_distribution="normal")
    scaler.fit(test_sample[train_features])
    test_sample[train_features] = scaler.fit_transform(test_sample[train_features])
    print("SUCCESS")

if opts.preprocess=="M":
    print("preprocessing with SIMPLIFIED / PHYSICS-DRIVEN preprocessing")
    
    # hack, relic from deprecated code
    tn_scaled = train_sample
    tt_scaled = test_sample
    
    _table = []
    if not opts.not_constrain:
        print("NOTE:running preprocessing with RENTION WINDOW ON")
        train_sample[train_features] = tn_scaled[train_features].apply(lambda x: constrain(x), axis=0)
        test_sample[train_features] = tt_scaled[train_features].apply(lambda x: constrain(x), axis=0)
        for tf in train_features:
            _table.append([tf,  train_sample[tf].min(), train_sample[tf].max(), test_sample[tf].min(), test_sample[tf].max()])
        print(tabulate(_table, headers=["Training feature", "min(train)", "max(train)", "min(test)", "max(test)"], 
                tablefmt="fancy_grid", numalign="left"))
    if opts.not_constrain:
        train_sample[train_features] = tn_scaled[train_features]
        test_sample[train_features] = tt_scaled[train_features]
    
    print("SUCCESS")

# define nrows and ncols
if len(train_features) % 3==0: 
    _ncols = 3 # preference
else: 
    _ncols = 2

if opts.nbody=="ThreeBody": 
    fig_x = 30; fig_y = 30;
if opts.nbody=="TwoBody": 
    fig_x = 20; fig_y = 24;
Path(f"{opts.plots_path}/{opts.nbody}").mkdir(parents=True, exist_ok=True)
for t in (train_sample, test_sample):
    # visualise the effect of the transform
    fig, axs = plt.subplots(ncols=_ncols, nrows = round(len(train_features)/_ncols),
        figsize=(fig_x, fig_y), facecolor='w', edgecolor='k')
    fig.tight_layout()
    axs = axs.ravel()

    if opts.nbody=="ThreeBody": plt_config = plt_config_ThreeBody 
    if opts.nbody=="TwoBody": plt_config = plt_config_TwoBody 
    for i in range(len(train_features)):

        feat_name = train_features[i]
        axs[i].set_xlabel(f"{plt_config[train_features[i]]['xlabel']}")

        _min = np.min(t[feat_name])
        _max = np.max(t[feat_name])
        
        _bins = 30

        axis_config = {
            "range" : [_min, _max],
            "bins"  : _bins
        }
        
        feat = t.query("label==1")[feat_name]
        axs[i].plot([],[],"", label=r"\textbf{LHCb Unofficial}", color="white")
        axs[i].hist(
            feat,
            color="tab:blue",
            histtype="stepfilled", alpha=.75,
            label="Inclusive beauty",
            **axis_config
        )

        feat = t.query("label==0")[feat_name]
        axs[i].hist(
            feat,
            color="firebrick",
            histtype="step", hatch="///",
            label="Minimum bias",
            **axis_config
        )

        axs[i].set_ylabel("Candidates")
        legend = axs[i].legend(fontsize=18)
        legend.get_frame().set_edgecolor("white")

        print(f"{feat_name} plot done.")
    
    if len(axs)>len(train_features):
        for i in np.arange(len(train_features), len(axs), 1):
            axs[i].plot([], [], "", label="Dummy canvas", color="white")
            axs[i].legend(loc="center", fontsize=30)
        
    if t is train_sample: 
        plt.savefig(f"{opts.plots_path}/{opts.nbody}/train_{opts.preprocess}scaled.pdf")
        plt.savefig(f"{opts.plots_path}/{opts.nbody}/train_{opts.preprocess}scaled.png")
    if t is test_sample: 
        plt.savefig(f"{opts.plots_path}/{opts.nbody}/test_{opts.preprocess}scaled.pdf")
        plt.savefig(f"{opts.plots_path}/{opts.nbody}/test_{opts.preprocess}scaled.png")

#Save Samples
Path(f"{opts.out_path}").mkdir(parents=True, exist_ok=True)
train_sample.to_pickle(f"{opts.out_path}/{opts.nbody}_Balanced_Train.pkl")
test_sample.to_pickle(f"{opts.out_path}/{opts.nbody}_Balanced_Test.pkl")