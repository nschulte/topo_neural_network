"""Prep the per-mode of the test performance eval of the n-body topo BDT/NN
Spawn the per-mode evaluation scripts
"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

import os
from argparse import ArgumentParser
from pathlib import Path
import yaml

parser = ArgumentParser(description="eval configuration")
parser.add_argument('-M','--modelpath', required=True, help="path to trained model")
parser.add_argument('-f','--features',  default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/features.yml", 
    help="features of the model")    
parser.add_argument('-m','--modes',     default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/modes.yml",
    help="path to yml file of decay modes used for evaluation (including 'balanced')")    
parser.add_argument('-v','--evalpath',  default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/eval.py", 
    help="path to per-mode eval executable") 
parser.add_argument('-b','--nbody', 
    choices=["TwoBody", "ThreeBody"], 
    help="n-body implementation ['TwoBody', 'ThreeBody']")       
parser.add_argument('-e','--entrystop', default=-1, type=int,
    help="integer specifying the max number of candidates that we read in [default: -1, ie max number of entries]")   
parser.add_argument('-t','--test', action='store_true',  
    help="test mode, run with 1_000 candidates only")
opts = parser.parse_args()

# load the BDT/NN 
# FILL 

# check & read in the modes yaml file
# decorator to check input path exists
def check_argpath(func):
   def wrapper(feats_path):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path)
        return features 
   return wrapper

@check_argpath
def read_config(
    feats_path:str
)->"list":
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, 'r') as stream:  
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    
    try:
        opts.nbody in in_dict
        feature_list = in_dict[opts.nbody]
    except ValueError:
            print("'features' key not in dict")
    
    return feature_list


MODES = read_config(opts.modes)

# need to generate the per-mode folder; 
# this will contain a bash script for testing, plots dir, and performance summary directory [log file with AUC and pkl dataframe for eff curves]
for mode in list(MODES):
    # generate the eval directory
    modepath = f"{os.path.dirname(opts.modelpath)}/eval/{mode}"

    Path(f"{modepath}").mkdir(parents=True, exist_ok=True)
    # eval sh script
    sh_file = f"{modepath}/run.sh" 
    print(f"python {opts.evalpath} \
        --modelpath {opts.modelpath} \
        --mode {mode} \
        --entrystop {opts.entrystop}", 
        file=open(sh_file, "w"))
    os.system(f"chmod +x {sh_file}")
    