#cosmetics
import mplhep as hep
import matplotlib.pyplot as plt
plt.style.use(hep.style.LHCb2)
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    #"font.serif": ["Palatino"],
    "font.weight": "heavy",
    "axes.labelsize": 25,
    "axes.titlesize": 30,
    "xaxis.labellocation": "center",
    "yaxis.labellocation": "center",
    "legend.fontsize": 18,
    "ytick.labelsize": 25,
    "xtick.labelsize": 25,
})