"""Train the n-body topo BDT/NN"""

__author__ = "Blaise Delaney & Nicole Schulte"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
from cgi import test
import os, yaml
from argparse import ArgumentParser
from pathlib import Path
import numpy as np
import pandas as pd
import time
import pickle
import torch
from torch.utils.data import TensorDataset, DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn import functional as F
from torch.nn.functional import binary_cross_entropy_with_logits
from torch.nn import HingeEmbeddingLoss
from sklearn.metrics import roc_auc_score
from monotonenorm import SigmaNet, GroupSort, direct_norm


# decorator to check input path exists
def check_argpath(func):
    def wrapper(feats_path, **kwargs):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path)
        return features

    return wrapper


# read in 2-/3- body feature list
@check_argpath
def read_config(feats_path: str) -> "list":
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, "r") as stream:
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)

    try:
        opts.nbody in in_dict
        feature_list = in_dict[opts.nbody]
    except ValueError:
        print("'features' key not in dict")

    return feature_list


# config model
def get_model(
    robust: bool,
    monotonic: bool,
    _nbody: "keyword: [TwoBody|ThreeBody]",
    _features: "training feature list",
    LIP: "Lipschitz const value",
) -> "torch.model":
    def lipschitz_norm(module, is_norm=False, _kind="one"):
        if not robust:
            print("Running with uncostrained NN")
            return module  # condition to call this only if robust
        else:
            print("Booked Lip NN")
            print(is_norm, _kind)
            return direct_norm(
                module,  # the layer to constrain
                always_norm=is_norm,
                kind=_kind,  # |W|_1 constraint type
                alpha=LIP ** (1 / 4),  # norm of the layer (LIP ** (1/nlayers))
            )

    model = torch.nn.Sequential(
        lipschitz_norm(torch.nn.Linear(len(_features), 32)),  # , _kind="one-inf"),
        GroupSort(32 // 2),
        lipschitz_norm(torch.nn.Linear(32, 64)),  # _kind="inf"),
        GroupSort(64 // 2),
        lipschitz_norm(torch.nn.Linear(64, 32)),  # _kind="inf"),
        GroupSort(32 // 2),
        lipschitz_norm(torch.nn.Linear(32, 1)),  # _kind="inf"),
    )

    if robust:
        print("NOTE: running with monotonicity/robustness in place")
        print(f"Lambda: {LIP}")
        if monotonic:
            if _nbody == "TwoBody":
                _monotone_constraints = [1, 1, 1, 1, 1, 0, 1, 0]
            if _nbody == "ThreeBody":
                _monotone_constraints = [1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0]
            for i in range(len(_features)):
                print(_features[i], _monotone_constraints[i])
        if not monotonic:
            _monotone_constraints = np.zeros(len(_features))
        assert len(_monotone_constraints) == len(_features)
        model = SigmaNet(model, sigma=LIP, monotone_constraints=_monotone_constraints)
    print(model)
    return model


if __name__ == "__main__":
    parser = ArgumentParser(description="training configuration")
    parser.add_argument(
        "-l", "--Lambda", required=True, type=float, help="Lipschitz constant value"
    )
    parser.add_argument(
        "-f",
        "--features",
        default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/features.yml",
        help="features of the model",
    )
    parser.add_argument(
        "-o",
        "--outdir",
        default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/exec",
        required=False,
        help="directory of sh files for train/eval [default: exec]",
    )
    parser.add_argument(
        "-b",
        "--nbody",
        required=True,
        choices=["TwoBody", "ThreeBody"],
        help="n-body implementation ['TwoBody', 'ThreeBody']",
    )
    parser.add_argument(
        "-e",
        "--entrystop",
        default=-1,
        type=int,
        help="integer specifying the max number of candidates that we read in [default: -1, ie max number of entries]",
    )
    parser.add_argument(
        "-p",
        "--eospath",
        default="/home/blaised/private/hlt2_topo/topo_neural_network/scratch/train",
        help="path to input pkl dataframes (minbias, balanced and individual modes)",
    )
    parser.add_argument(
        "-M",
        "--monotonic",
        action="store_true",
        help="engage monotonicity? [default: False]",
    )
    parser.add_argument(
        "-R",
        "--robust",
        action="store_true",
        help="engage robustness? [default: False]",
    )
    parser.add_argument(
        "-E", "--epochs", default=120, type=int, help="training epochs [int]"
    )
    parser.add_argument(
        "-B",
        "--batchsize",
        default=15_000,
        type=int,
        help="number of candidates in batch size [int]",
    )
    parser.add_argument("-r", "--lrate", default=0.00338098, help="learning rate")
    parser.add_argument(
        "-t",
        "--test",
        action="store_true",
        help="test mode, run with 1_000 candidates only",
    )
    parser.add_argument(
        "-H", "--hinge", action="store_true", help="run with Hinge loss [default:False]"
    )
    parser.add_argument(
        "-z",
        "--hack",
        action="store_true",
        help="HACK: just touch model.pt to work around latency issues",
    )
    parser.add_argument(
        "-Z",
        "--seed",
        default=42,
        type=int,
        help="manual seed initialisation for reproducibilty of results",
    )
    opts = parser.parse_args()

    # @check_argpath
    def read_samples(
        pkl_path: str,
        isTest: bool,
        useHinge=opts.hinge,
    ) -> "dataframe":
        """read in the training and test samples (balanced)"""
        if isTest:
            print("WARNING: running in TEST mode (1_000 candidates)")
            train_sample = pd.read_pickle(f"{pkl_path}_Balanced_Train.pkl")[:100_000]
            test_sample = pd.read_pickle(f"{pkl_path}_Balanced_Test.pkl")[:100_000]
        else:
            train_sample = pd.read_pickle(f"{pkl_path}_Balanced_Train.pkl")
            test_sample = pd.read_pickle(f"{pkl_path}_Balanced_Test.pkl")

        if useHinge:
            print("WARNING: using HINGE LOSS")
            if 0 in train_sample.label.to_numpy():
                train_sample.label.replace(to_replace=0.0, value=-1.0, inplace=True)
                test_sample.label.replace(to_replace=0.0, value=-1.0, inplace=True)
            assert all(p == -1 or p == 1 for p in train_sample.label)

        return train_sample, test_sample

    torch.manual_seed(opts.seed)  # reproducibility

    # if available, run on GPU
    if torch.cuda.is_available():
        device = torch.device("cuda")
        print("Running on cuda")
    else:
        device = torch.device("cpu")
        print("Running on cpu")

    # ------------------------------------------------------
    #                   global variables
    if opts.test:
        train_sample, test_sample = read_samples(
            f"{opts.eospath}/{opts.nbody}", isTest=opts.test
        )
        assert len(train_sample) == 100_000
        assert len(test_sample) == 100_000
    if not opts.test:
        train_sample, test_sample = read_samples(
            f"{opts.eospath}/{opts.nbody}", isTest=opts.test
        )
        assert len(train_sample) > 100_000
        assert len(test_sample) > 100_000
    print("Sucessfully imported Dataframes\n")

    features = read_config(opts.features)
    event_ids = ["runNumber", "eventNumber"]
    label_name = [f"{opts.nbody}_fromSignalB"]
    same_Bs = [f"{opts.nbody}_FromSameB"]
    multiplicity = [f"{opts.nbody}_nCharged", f"{opts.nbody}_nNeutral"]
    LIP = opts.Lambda
    EPOCHS = opts.epochs
    BATCH_SIZE = opts.batchsize
    LR = opts.lrate
    # ------------------------------------------------------

    print("Making Data to Tensors...")
    X_train = torch.tensor(train_sample[features].values.astype(np.float32))
    y_train = torch.tensor(train_sample.label.values.astype(np.float32))[:, None]
    X_test = torch.tensor(test_sample[features].values.astype(np.float32))
    y_test = torch.tensor(test_sample.label.values.astype(np.float32))[:, None]
    print("SUCESS\n")

    print("Moving Data to Device...")
    X_train = X_train.to(device)
    y_train = y_train.to(device)
    X_test = X_test.to(device)
    y_test = y_test.to(device)
    print("SUCESS\n")

    print("Moving Model to Device...")
    model = get_model(
        monotonic=opts.monotonic,
        robust=opts.robust,
        _nbody=opts.nbody,
        _features=features,
        LIP=LIP,
    ).to(device)
    print("SUCCESS\n")

    train_data = TensorDataset(X_train, y_train)
    train_loader = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=False)
    optimizer = torch.optim.Adam(model.parameters(), lr=LR)
    scheduler = ReduceLROnPlateau(optimizer, "min", patience=10)

    Train_loss_array = []
    Test_loss_array = []
    ROC_Train_array = []
    ROC_Test_array = []
    ACC_Test = []
    ACC_train = []

    # generate the directory where I can dump the pkld trained model and any training performance plot
    out_path = f"{opts.outdir}/{opts.nbody}/lambda{opts.Lambda}/train"
    Path(f"{out_path}/plots").mkdir(parents=True, exist_ok=True)

    with open(f"{out_path}/logfile.txt", "w") as logfile:
        logfile.seek(0)

        print("Start Training")
        start = time.time()
        for i in range(EPOCHS):
            epoch_train_loss = 0
            epoch_auc_train = 0
            total_batch = len(train_loader)
            print(
                f"Epoch Number {i} ---------------------------------------------------",
                file=logfile,
            )
            model.train()
            for data, target in train_loader:
                if device == "cuda":
                    torch.cuda.synchronize()
                optimizer.zero_grad()
                output = model(data)
                if device == "cuda":
                    torch.cuda.synchronize()
                if opts.hinge:
                    loss_train = torch.relu(1e2 - target * output).mean()
                if not opts.hinge:
                    loss_train = binary_cross_entropy_with_logits(output, target)
                loss_train.backward()
                epoch_train_loss += loss_train
                auc_train = roc_auc_score(
                    target.cpu().detach().numpy(), output.cpu().detach().numpy()
                )
                epoch_auc_train += auc_train
                epoch_time = time.time() - start
                curr_lr = optimizer.param_groups[0]["lr"]
                optimizer.step()
                print(
                    f"{i}:  AUC Score:{auc_train}   Loss: {loss_train}, time needed {epoch_time}, LR: {curr_lr}",
                    file=logfile,
                )
            scheduler.step(loss_train)
            Train_loss_array.append(
                epoch_train_loss.cpu().detach().numpy() / total_batch
            )
            ROC_Train_array.append(epoch_auc_train / total_batch)
            acc_train = np.mean(
                (output.cpu().detach().numpy() > 0.5) == target.cpu().detach().numpy()
            )
            print(f"Epoche {i} Mean AUC: {epoch_auc_train/ total_batch}")
            ACC_train.append(acc_train)

            model.eval()
            epoch_eval_loss = 0
            with torch.no_grad():
                prediction = model(X_test)
                truth = y_test
                if opts.hinge:
                    loss_eval = torch.relu(1e2 - target * output).mean()
                if not opts.hinge:
                    loss_eval = binary_cross_entropy_with_logits(prediction, truth)
                epoch_eval_loss += loss_eval
                auc_test = roc_auc_score(
                    truth.cpu().detach().numpy(), prediction.cpu().detach().numpy()
                )
                acc_test = np.mean(
                    (prediction.cpu().detach().numpy() > 0.5)
                    == truth.cpu().detach().numpy()
                )
            Test_loss_array.append(epoch_eval_loss.cpu().detach().numpy())
            ROC_Test_array.append(auc_test)
            ACC_Test.append(acc_test)

    end = time.time() - start
    print(f"Total running time: {end}")

    if opts.hinge:
        scaled_predictions = prediction
        # scaled_predictions = 1./(1.+torch.exp(-100.*prediction)) # steeper sigmoid
    if not opts.hinge:
        scaled_predictions = torch.sigmoid(prediction)

    predictions = np.zeros(len(test_sample))
    predictions = scaled_predictions.cpu().detach().numpy()
    test_preds = test_sample
    test_preds["preds_per_cand"] = predictions

    # ===== persist to next stage =====
    print("Move Testframe to Pickle")
    test_preds.to_pickle(f"{out_path}/{opts.nbody}_Train_Results.pkl")

    performance_params = [
        Train_loss_array,
        Test_loss_array,
        ROC_Train_array,
        ROC_Test_array,
        ACC_Test,
        ACC_train,
    ]

    # there seems to be a latency issue on crosby + snakemake? HACK: simple touch
    # save model for inference
    print("Writing trained model to file...")
    if not opts.hack:
        torch.save(
            model.state_dict(),
            f"{opts.outdir}/{opts.nbody}/lambda{opts.Lambda}/train/trained_model.pt",
        )
        print(
            f"SUCCESS: written to {opts.outdir}/{opts.nbody}/lambda{opts.Lambda}/train/trained_model.pt"
        )
    if opts.hack:
        Path(
            f"{opts.outdir}/{opts.nbody}/lambda{opts.Lambda}/train/trained_model.pt"
        ).touch()
        print(
            f"HACK: touched {opts.outdir}/{opts.nbody}/lambda{opts.Lambda}/train/trained_model.pt"
        )

    print("Dumping Parameters to Numpy")
    with open(
        f"{out_path}/{opts.nbody}_performance_parameters.npy", "wb"
    ) as outputfile:
        np.save(outputfile, performance_params)
