"""If necessary, generate the training variables.

Plots:
- mean of overall distribution, compared with the signal and bkg populations
- correlations (overall, sig, bkg)

Output:
- train pkl dataframe with the training features (pre-scaling)
- test pkl dataframe with the training features (pre-scaling)
"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

    
_gev_vars = ["sum_PT_final_state_tracks", "min_PT_final_state_tracks", "TwoBody_Mcorr", "TwoBody_PT"]
_log_vars = ["min_FS_IPCHI2_OWNPV", "max_FS_IPCHI2_OWNPV", "TwoBody_FDCHI2_OWNPV"]

# hacky refactoring for standalone inference inheritance
if __name__ == '__main__':
    import re
    import numpy as np
    import pandas as pd
    from mpl_config import *
    from argparse import ArgumentParser
    from utils import *
    from pathlib import Path
    import seaborn as sn
    import matplotlib 
    import scipy
    from tabulate import tabulate

    parser = ArgumentParser(description="training configuration")
    parser.add_argument("-p","--path",  
        default="/home/blaised/private/hlt2_topo/topo_neural_network/scratch", 
        help="path to directory with raw train and test pkl [default:/home/blaised/private/hlt2_topo/topo_neural_network/scratch]")
    parser.add_argument("-S","--not_slim",  
        action="store_true",
        help="NOT remove unused branches [default:False]")
    parser.add_argument('-f','--features', 
        default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/features.yml", 
        help="features of the model")    
    parser.add_argument('-P','--plotdir',  
        default="/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/plots/pre", 
        help="directory where to save plots")   
    parser.add_argument('-b','--nbody',     
        default="TwoBody", 
        help="n-body implementation [default:'TwoBody']")    
    parser.add_argument('-D','--dev',     
        action="store_true",
        help="if true, work with 10K candidate per sample [default:False]")    
    parser.add_argument('-T','--not_transform',     
        action="store_true",
        help="if true, not apply scaling to O(1) rangege [default:False]")    
    opts = parser.parse_args()

    tn = pd.read_pickle(f"{opts.path}/{opts.nbody}_Balanced_Train_nopreprocess.pkl")
    ts = pd.read_pickle(f"{opts.path}/{opts.nbody}_Balanced_Test_nopreprocess.pkl")
    FEATURES = read_config(feats_path=opts.features, _nbody=opts.nbody)

    if opts.dev:
        tn = tn[:10_000]
        ts = ts[:10_000]

    # generate missing features, before preprocessing
    for df in [tn, ts]:

        df["min_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV"]].min(axis=1)
        df["max_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV"]].max(axis=1)
        assert(df.min_FS_IPCHI2_OWNPV.all()<=df.max_FS_IPCHI2_OWNPV.all()) # sanity check

        if not opts.not_slim:
            to_remove = [
                "log(Track1_IPCHI2_OWNPV)",
                "log(Track2_IPCHI2_OWNPV)",
                "Track1_IPCHI2_OWNPV",
                "Track2_IPCHI2_OWNPV",
            ]
            [df.pop(b) for b in to_remove]
            for b in to_remove: assert(b not in df.keys())

        if not opts.not_transform: 
            for g in _gev_vars:
                df[g] /= 1000. 
            for l in _log_vars:
                df.loc[df[l] <= 0, l] = 1e-5
                assert(df[l].all()>0)
                df[l] = np.log(df[l].to_numpy()) 
            df["TwoBody_ENDVERTEX_DOCAMAX"] *= 10.

    plt_config = {
        "min_PT_final_state_tracks": {
            "range" : [0, 5],
            "xlabel" : "$p_T^{min}$ [GeV$/c$]"
        },
        "sum_PT_final_state_tracks": {
            "range" : [0, 15],
            "xlabel" : "$\sum{p_T}$ [GeV$/c$]"
        },
        "min_FS_IPCHI2_OWNPV": {
            "range" : [0, 15],
            "xlabel" : "log(IP $\chi^2_{min}$) [Arbitrary Units]"
        },
        "max_FS_IPCHI2_OWNPV": {
            "range" : [0, 15],
            "xlabel" : "log(IP $\chi^2_{max}$) [Arbitrary Units]"
        },
        "TwoBody_ENDVERTEX_DOCAMAX": {
            "range" : [0, 5],
            "xlabel" : "DOCA$_{max}$ [cm]"
        },
        "TwoBody_ENDVERTEX_CHI2": {
            "range" : [0, 10],
            "xlabel" : "$B$ $\chi^2_{vtx}$ [Arbitrary Units]"
        },
        "TwoBody_FDCHI2_OWNPV": {
            "range" : [0, 17],
            "xlabel" : "log($B$ FD $\chi^2$) [Arbitrary Units]"
        },
        "TwoBody_Mcorr": {
            "range" : [0, 25],
            "xlabel" : "$m_{corr}$ [GeV$/c^2$]"
        },
        "TwoBody_PT": {
            "range" : [0, 15],
            "xlabel" : "$p_T(B)$ [GeV$/c$]"
        },
    }

    # tally the KS scores as a proxy for the feature selection
    KS_container = []
    for v in FEATURES:
        ks = scipy.stats.ks_2samp(
        tn.query("label==0")[v], tn.query("label==1")[v] 
        )[0]
        KS_container.append([v, np.float(ks)])

    KS_container = np.array(KS_container)
    # sort in descending order features to maximise KS score
    KS_container = KS_container[np.argsort(KS_container[:,1])[::-1]]
    print(tabulate(KS_container, headers=["Variable","KS score"], tablefmt="fancy_grid"))

    # generate distribution plots
    from mpl_config import *
    Path(f"{opts.plotdir}/{opts.nbody}/dist").mkdir(parents=True, exist_ok=True)
    for v in FEATURES:
        fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(20,8))
        fig.tight_layout()
        _weights = np.ones_like(tn[v])/float(len(tn[v]))
        ax0.hist(
            tn[v],
            bins=30, 
            histtype="stepfilled",
            color="lightgrey", alpha=1.,
            label="Signal + Background",
            range=plt_config[v]["range"],
            weights=_weights
        )
        ax0.axvline(
            np.mean(tn[v]), 
            color="black", ls=":", lw=3.,
            label="Global mean"
        )
        ax0.axvline(
            np.median(tn[v]), 
            color="purple", ls="-.", lw=2.5,
            label="Global median"
        )
        ax0.plot([], [], label=r"$\sigma=$%0.1f"%(np.std(tn[v])), color="white")
        ax0.text(s="LHCb Unofficial", x=0.0, y=1.01, transform=ax0.transAxes, fontsize=40)
        ax0.legend(fontsize=30)
        ax0.set_xlabel(plt_config[v]["xlabel"], fontsize=35) 
        ax0.set_ylabel("Candidates, normalised", fontsize=35)


        bw= np.ones_like(tn.query("label==0")[v])/float(len(tn.query("label==0")[v]))
        sw= np.ones_like(tn.query("label==1")[v])/float(len(tn.query("label==1")[v]))
        ax1.hist(
            tn.query("label==0")[v],
            bins=30, 
            histtype="stepfilled",
            color="tab:red", alpha=.3,
            label="Minimum bias",
            range=plt_config[v]["range"], 
            weights=bw
        )
        ax1.hist(
            tn.query("label==1")[v],
            bins=30, 
            histtype="step",
            color="tab:blue", lw=3.,
            label="Inclusive beauty",
            range=plt_config[v]["range"],
            weights=sw
        )
        ax1.axvline(
            np.mean(tn[v]), 
            color="black", ls=":", lw=3,
            label="Global mean"
        )
        ax1.plot([], [], label=r"Global $\sigma=$ %0.1f"%(np.std(tn[v])), color="white")

        ax1.text(s="LHCb Unofficial", x=0.0, y=1.01, transform=ax1.transAxes, fontsize=40)
        ax1.legend(fontsize=25)
        ax1.set_xlabel(plt_config[v]["xlabel"], fontsize=35) 
        ax1.set_ylabel("Candidates, normalised", fontsize=35)  

        plt.savefig(f"{opts.plotdir}/{opts.nbody}/dist/{v}.pdf")
        plt.savefig(f"{opts.plotdir}/{opts.nbody}/dist/{v}.png")


    sig_corr = tn.query("label==1")[FEATURES]
    sig_corr.name = "sig"
    bkg_corr = tn.query("label==0")[FEATURES]
    bkg_corr.name = "minbias"

    # 2D plots to elucidate FD behaviour
    Path(f"{opts.plotdir}/{opts.nbody}/2d").mkdir(parents=True, exist_ok=True)
    for v in FEATURES:
        fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(20,8))
        fig.tight_layout()
        ax0.hist2d(
            x = sig_corr.TwoBody_FDCHI2_OWNPV,
            y = sig_corr[v],
            bins=30,
            range = [ 
                plt_config["TwoBody_FDCHI2_OWNPV"]["range"],
                plt_config[v]["range"],
            ],
            cmap="inferno"
        )
        ax0.set_ylabel(plt_config[v]["xlabel"])
        ax0.set_xlabel(plt_config["TwoBody_FDCHI2_OWNPV"]["xlabel"])
        ax0.set_title("Inclusive Beauty")
        
        ax1.hist2d(
            x = bkg_corr.TwoBody_FDCHI2_OWNPV,
            y = bkg_corr[v],
            bins=30,
            range=[
                plt_config["TwoBody_FDCHI2_OWNPV"]["range"],
                plt_config[v]["range"],
            ],
            cmap="inferno"
        )
        ax1.set_ylabel(plt_config[v]["xlabel"])
        ax1.set_xlabel(plt_config["TwoBody_FDCHI2_OWNPV"]["xlabel"])
        ax1.set_title("Minimum Bias")
        
        for ax in (ax0, ax1):
            ax.text(s=r"LHCb Unofficial", x=0.02, y=0.93, 
                transform=ax.transAxes, 
                color="white", fontsize=35)

        plt.savefig(f"{opts.plotdir}/{opts.nbody}/2d/{v}.pdf")
        plt.savefig(f"{opts.plotdir}/{opts.nbody}/2d/{v}.png")

    # GENERATE CORR MATRICES AND STUDY CORRELATION WITH FD
    matplotlib.rcParams.update(matplotlib.rcParamsDefault)
    matplotlib.rcParams.update({
        "text.usetex": True,
        "font.family": "serif",
        "font.weight": 1000,
    })
    Path(f"{opts.plotdir}/{opts.nbody}/corr").mkdir(parents=True, exist_ok=True)
    for d in [sig_corr, bkg_corr]:
        short_labels = [re.sub(r'\[.+\]', '', plt_config[v]["xlabel"]) for v in d.keys()]
        with sn.axes_style("white"):
            mask = np.zeros_like(d.corr())
            mask[np.triu_indices_from(mask)] = True
            f, ax = plt.subplots(figsize=(8, 7))
            ax = sn.heatmap(d.corr(), annot=True, cmap="Spectral",
                fmt=".2f", vmin=-1.0, vmax=1.0, mask=mask, square=True,
                cbar_kws={'label': 'Correlation [Arbitrary Units]'})
            ax.set_xticklabels(short_labels, rotation=75, fontsize=10)
            ax.set_yticklabels(short_labels, fontsize=10)
            ax.text(s=r"LHCb Unofficial", x=0.0, y=1.05, transform=ax.transAxes, fontsize=25)
            ax.figure.axes[-1].yaxis.label.set_size(15)
        plt.savefig(f"{opts.plotdir}/{opts.nbody}/corr/{d.name}.pdf")
        plt.savefig(f"{opts.plotdir}/{opts.nbody}/corr/{d.name}.png")

    Path(f"{opts.path}/pre").mkdir(parents=True, exist_ok=True)
    tn.to_pickle(f"{opts.path}/pre/{opts.nbody}_Train.pkl")
    ts.to_pickle(f"{opts.path}/pre/{opts.nbody}_Test.pkl")

    tn_prep = pd.read_pickle(f"{opts.path}/{opts.nbody}_Balanced_Train_nopreprocess.pkl")
    ts_prep = pd.read_pickle(f"{opts.path}/{opts.nbody}_Balanced_Test_nopreprocess.pkl")

    assert(len(tn)==len(tn_prep))
    assert(len(ts)==len(ts_prep))