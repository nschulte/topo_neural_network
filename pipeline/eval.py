"""Train the n-body topo BDT/NN"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

import os
from argparse import ArgumentParser
from pathlib import Path
import re

parser = ArgumentParser(description="eval configuration")
parser.add_argument('-M','--modelpath', required=True, help="path to trained model")
parser.add_argument('-f','--features',  default="~/private/hlt2_topo/topo_neural_network/pipeline/features.yml", 
    help="features of the model")    
parser.add_argument('-m','--mode',      required=True,
    help="which MC to run on")    
parser.add_argument('-e','--entrystop', default=-1, type=int,
    help="integer specifying the max number of candidates that we read in [default: -1, ie max number of entries]")   
opts = parser.parse_args()

# directory where results are stored + /plots/ subdirectory
out_path = f"{os.path.dirname(opts.modelpath)}/eval/{opts.mode}"
Path(f"{out_path}/plots").mkdir(parents=True, exist_ok=True)

# results
os.system(f"touch {out_path}/results.json")
os.system(f"touch {out_path}/eff.pkl") # dataframe with efficiency evaluation
os.system(f"touch {out_path}/plots/eff.pdf")