"""Script to generate the train and evaluation script for a given BDT/NN config"""

__author__ = "Blaise Delaney"
__email__  = "blaise.delaney@cern.ch"

from argparse import ArgumentParser
from pathlib import Path
import yaml
import os

parser = ArgumentParser(
            description="training+eval configuration")
parser.add_argument('-p','--hyperpars', default="model.yml",    
    help="model hyperparameters path")   
parser.add_argument('-b','--nbody',     choices=["TwoBody", "ThreeBody"], 
    help="n-body implementation ['twobody', 'threebody']")    
parser.add_argument('-o','--outdir',    default="exec",  required=False, 
    help='directory of sh files for train/eval [default: exec]')
parser.add_argument('-t','--trainpath', default="train.py",
    help='path to the train_eval.py executable')
parser.add_argument('-T','--test', action='store_true',  
    help="test mode, run with 1_000 candidates only")
parser.add_argument('-M','--monotonic', action="store_true",
    help="engage monotonicity? [default: False]")
parser.add_argument('-R','--robust', action="store_true",
    help="engage robustness? [default: False]")
parser.add_argument('-H','--hinge', action='store_true',  
    help="run with Hinge loss [default:False]")
opts = parser.parse_args()

# decorator to check input path exists
def check_argpath(func):
   def wrapper(feats_path):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path)
        return features 
   return wrapper

# decorator to log execution time
def log_time(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        ts = time()
        scaled_data = func(*args, **kwargs)
        te = time()
        with open(f"{opts.logdir}/{opts.timelog}", "a") as f:
            print("func:%r args:[%r] took: %2.4f sec" % \
                (func.__name__, kwargs.get("scaler"), te-ts),
                file=f)
        return scaled_data
    return wrapper

@check_argpath
def read_config(
    feats_path:str
)->"list":
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, 'r') as stream:  
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    
    try:
        opts.nbody in in_dict
        feature_list = in_dict[opts.nbody]
    except ValueError:
            print("'features' key not in dict")
    
    return feature_list

LAMBDAS  = read_config(opts.hyperpars)["lambda"]

# establish the shell scripts used to run train_eval.py and write 
for l in LAMBDAS:
    out_path =f"{opts.outdir}/{opts.nbody}/lambda{l}/train" 
    Path(f"{out_path}").mkdir(parents=True, exist_ok=True)
    sh_file = f"{out_path}/run.sh" 
    base_sh = f"python {opts.trainpath} --nbody {opts.nbody} --Lambda {l}"
    
    # flags
    if opts.test:
        base_sh += " --test"
    if opts.monotonic:
        base_sh += " --monotonic"
    if opts.robust:
        base_sh += " --robust"
    if opts.hinge:
        base_sh += " --hinge"

    # write to executable
    print(f"{base_sh}", file=open(sh_file, "w"))
    os.system(f"chmod +x {out_path}/run.sh")