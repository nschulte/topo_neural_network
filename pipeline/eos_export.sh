# Move the model.pt by to eos

twobody_path=/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/exec_2Body_LipM/TwoBody/lambda1.75/train
threebody_path=/home/blaised/private/hlt2_topo/topo_neural_network/pipeline/exec_3Body_LipM/ThreeBody/lambda1.75/train

scp ${twobody_path}/trained_model.pt bldelane@lxplus.cern.ch:/eos/lhcb/user/b/bldelane/public/Hlt2BTopo/Lip_M_NN/2body
scp ${threebody_path}/trained_model.pt bldelane@lxplus.cern.ch:/eos/lhcb/user/b/bldelane/public/Hlt2BTopo/Lip_M_NN/3body