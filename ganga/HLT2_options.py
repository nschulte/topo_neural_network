###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Script to run the Hlt1 track MVA filtering and produce DSTs with
the two- and three-body candidates.

This follows the follwing tutorial and scripts:
    - https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_analysis.html?highlight=decreport#analysing-hlt1-decision-reports-after-hlt2
    - https://gitlab.cern.ch/amathad/bachelors-project-uzh-2022/-/tree/master/Hlt1_Hlt2_DV_options
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from Moore import options, run_moore
from Moore.lines import Hlt2Line
from Moore.config import register_line_builder
from Hlt2Conf.lines.topological_b import (
    make_unfiltered_topo_twobody,
    make_unfiltered_topo_threebody,
    line_prefilters,
)
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import upfront_reconstruction
from PyConf import configurable
from typing import List, Union
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.protoparticles import make_charged_protoparticles
from Moore.tcks import dump_hlt2_configuration
from Configurables import ConfigCDBAccessSvc
from RecoConf.hlt1_tracking import default_ft_decoding_version
from RecoConf.hlt1_allen import allen_gaudi_node_barriers as allen_sequence

# assign key to allen TCK - generated by running Hlt1 upstream & steps detailed in https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_analysis.html#analysing-hlt1-decision-reports-after-hlt2
ConfigCDBAccessSvc().File = "config.cdb"

all_lines = {}


@register_line_builder(all_lines)
@configurable
def hlt1filtered_twobody_line(
    name: str = "Hlt2TwoBodyTopo_Hlt1FilteredLine",
    prescale: Union[int, float] = 1,
    persistreco: bool = True,
) -> Hlt2Line:
    """Two-body topo line with Hlt1*Line MVA filtering.

    Construct the beauty candidates without any MVA selection,
    and with the hlt1 filtering in place.

    Parameters
    ----------
    name : str
        Name of the line - be sure to follow the convention.
    prescale : int or float
        Prescale for the line.
    persistreco : bool
        Whether to persist the reco objects.

    Returns
    -------
    Hlt2Line: The hlt1-filtered two-body topo line building the candidates, with no
        MVA selection.
    """
    candidates = make_unfiltered_topo_twobody()  # relax any mva selection
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,  # see stack/Moore/Hlt/Moore/python/Moore/lines.py
        hlt1_filter_code=["Hlt1.*Decision"],  # filter on HLT1 decisions
    )


@register_line_builder(all_lines)
@configurable
def hlt1filtered_threebody_line(
    name: str = "Hlt2ThreeBodyTopo_Hlt1FilteredLine",
    prescale: Union[int, float] = 1,
    persistreco: bool = True,
) -> Hlt2Line:
    """Three-body topo line with Hlt1*Line filtering.

    Construct the beauty candidates without any MVA selection,
    and with the hlt1 filtering in place.

    Parameters
    ----------
    name : str
        Name of the line - be sure to follow the convention.
    prescale : int or float
        Prescale for the line.
    persistreco : bool
        Whether to persist the reco objects.

    Returns
    -------
    Hlt2Line: The hlt1-filtered three-body topo line building the candidates, with no
        MVA selection.
    """
    candidates = make_unfiltered_topo_threebody()  # relax any mva selection
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        persistreco=persistreco,  # see stack/Moore/Hlt/Moore/python/Moore/lines.py
        hlt1_filter_code=["Hlt1.*Decision"],  # filter on HLT1 decisions
    )


def make_lines() -> List[Hlt2Line]:
    """Make the lines."""
    return [hlt1filtered_twobody_line(), hlt1filtered_threebody_line()]


# .dst over which hlt1 was run
# options.input_files = ["myhlt1.dst"] # comment out to run on ganga

# config
options.data_type = "Upgrade"
options.simulation = True
options.input_type = "ROOT"

# set the ft decoding from file used upstream in hlt1
# https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/different_samples.html?highlight=default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)

options.conddb_tag = "sim-20210617-vc-md100"
options.dddb_tag = "dddb-20210617"
options.input_raw_format = 0.5
options.evt_max = -1
# options.control_flow_file = 'control_flow.gv'
# options.data_flow_file = 'data_flow.gv'

# options output
options.output_file = "myhlt2.dst"
options.output_type = "ROOT"

# NOTE: proceed with the latest reconstruction, realt-time and not from file
with reconstruction.bind(
    from_file=False
), make_charged_protoparticles.bind(), allen_sequence.bind(
    sequence="hlt1_pp_veloSP"
):  # reasonably sure that muon ID enabled by default, https://gitlab.cern.ch/lhcb/MooreAnalysis/-/merge_requests/62
    config = run_moore(
        options,
        make_lines,
        public_tools=[
            stateProvider_with_simplified_geom()
        ],  # https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_line.html?highlight=stateprovider#running
        # Set allen_hlt1 to true if Allen HLT1 filtered files are processed
        # For Moore HLT1 filtered files, set allen_hlt1=False (default value)
        allen_hlt1=True,  # run HLT updtream; now get the HLT1 decisions
        exclude_incompatible=False,  # allow the thread-unsafe topo_b lines
    )
    dump_hlt2_configuration(config, "my_hlt2.tck.json")
