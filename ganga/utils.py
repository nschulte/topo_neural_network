"""Miscellaneous methods to sanity-check the ganga submissions
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from dirac_paths import bkk_paths
from typing import Dict, Callable
import pprint


def sanity_check(
    func: Callable[[Dict[str, str]], Dict[str, str]]
) -> Callable[[Dict[str, str]], Dict[str, str]]:
    """Decorator to perform sanity checks on the BKK paths container

    Parameters
    ----------
    func: Callable[Dict[str, str]
        Method to source the BKK container dict

    Returns
    -------
    wrapper: Callable
        The method that benefits from the decorator
    """

    def wrapper(*args, **kwargs):
        bkk_dict = func(*args, **kwargs)
        for k, v in bkk_dict.items():
            for mk in v.keys():
                # if non-empty value, check the magpol matching
                if v[mk]:
                    assert len(v[mk]) > 0, f"ValueError: empty string for {v}[{mk}]"
                    assert mk in v[mk], f"KeyError: magpol key mismatch in field: {k}"
            # check consistency of event numbers if non-empty
            if bkk_dict[k]["MagUp"] != "" and bkk_dict[k]["MagDown"] != "":
                assert (
                    bkk_dict[k]["MagUp"].split("/")[4]
                    == bkk_dict[k]["MagDown"].split("/")[4]
                ), "ValueError: event number mismatch"
        return bkk_dict

    return wrapper


@sanity_check
def source_bkk_dict(
    bkk_dict: Dict[str, str],
) -> Dict[str, str]:
    """Dummy method to load the BKK paths dict.
    Useful to implement sanity checks.

    Parameters
    ----------
    bkk_dict: Dict[str, str]
        Dictionary with per-channel Dirac Bookkeeping paths

    Returns
    -------
    Dict[str, str]
        Returns the original dict
    """
    return bkk_dict


if __name__ == "__main__":

    pprint.pprint(source_bkk_dict(bkk_paths))
