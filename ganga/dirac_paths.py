"""Container for the paths obtained from the Dirac paths
for the Upgrade MC samples
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from configparser import NoOptionError
from typing import Dict
import pprint

# container
bkk_paths = {
    # inclusive 2- & 3-body b topo
    # ----------------------------
    "3MuNu": {
        "MagUp": "evt+std://MC/Upgrade/12513020/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12513020/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "D0Ds": {
        "MagUp": "evt+std://MC/Upgrade/12195047/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12195047/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "D0KsPi": {
        "MagUp": "evt+std://MC/Upgrade/12165181/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "",
    },
    "DDKPi": {
        "MagUp": "evt+std://MC/Upgrade/11198098/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "",
    },
    "DPiPiPi": {
        "MagUp": "evt+std://MC/Upgrade/12265002/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "",
    },
    "DsMuNu": {
        "MagUp": "evt+std://MC/Upgrade/13774000/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13774000/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "DsPi": {
        "MagUp": "",
        "MagDown": "evt+std://MC/Upgrade/13164044/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/DIGI",
    },
    "DstD0": {
        "MagUp": "evt+std://MC/Upgrade/12195032/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12195032/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "DstTauNu": {
        "MagUp": "evt+std://MC/Upgrade/11160001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11160001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "JpsiPhiKs": {
        "MagUp": "evt+std://MC/Upgrade/11146114/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11146114/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "K6Mu": {
        "MagUp": "evt+std://MC/Upgrade/12117015/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12117015/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "KPiPi": {
        "MagUp": "evt+std://MC/Upgrade/12103025/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12103025/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "KstGamma": {
        "MagUp": "evt+std://MC/Upgrade/11102202/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11102202/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lc2625MuNu": {
        "MagUp": "evt+std://MC/Upgrade/15576011/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15576011/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "LcPPi": {
        "MagUp": "evt+std://MC/Upgrade/12165094/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12165094/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "PhiKst": {
        "MagUp": "evt+std://MC/Upgrade/11104020/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11104020/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "PiMuNu": {
        "MagUp": "evt+std://MC/Upgrade/11512011/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/11512011/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    # muon 2- & 3-body b topo: include tau, mu, Vcb, Vub
    # --------------------------------------------------
    "Lb_LcMuNu": {
        "MagUp": "evt+std://MC/Upgrade/15874041/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15874041/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Lb_PMuNu": {
        "MagUp": "evt+std://MC/Upgrade/15512014/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/15512014/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bc_JpsiTauNu": {
        "MagUp": "evt+std://MC/Upgrade/14643048/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-BcVegPyPythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/14643048/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-BcVegPyPythia8/Sim10aU1/XDIGI",
    },
    "Bc_JpsiMuNu": {
        "MagUp": "evt+std://MC/Upgrade/14543010/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-BcVegPyPythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/14543010/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-BcVegPyPythia8/Sim10aU1/XDIGI",
    },
    "Bs_KMuNu": {
        "MagUp": "evt+std://MC/Upgrade/13512010/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13512010/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bs_KTauNu": {
        "MagUp": "evt+std://MC/Upgrade/13512030/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/13512030/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_D0XMuNu": {
        "MagUp": "evt+std://MC/Upgrade/12873441/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12873441/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_D0TauNu": {
        "MagUp": "evt+std://MC/Upgrade/12573001/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12573001/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_Dst0TauNu": {
        "MagUp": "evt+std://MC/Upgrade/12562410/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12562410/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_PPTauNu": {
        "MagUp": "evt+std://MC/Upgrade/12513061/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12513061/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    "Bu_PPMuNu": {
        "MagUp": "evt+std://MC/Upgrade/12513051/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/12513051/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10aU1/XDIGI",
    },
    # minimum bias (and inclusive beauty)
    # -----------------------------------
    "minbias": {
        "MagUp": "evt+std://MC/Upgrade/30000000/Beam7000GeV-Upgrade-MagUp-Nu7.6-Pythia8/Sim10aU1/XDIGI",
        "MagDown": "evt+std://MC/Upgrade/30000000/Beam7000GeV-Upgrade-MagDown-Nu7.6-Pythia8/Sim10aU1/XDIGI",
    },
    # NOTE: not sure whether we want to train on `incl_b`, as opposed to an admixture of token exclusive modes
    # "incl_b" : {
    #     "MagUp" : "evt+std://MC/Upgrade/10000000/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up02/DIGI",
    #     "MagDown" : "evt+std://MC/Upgrade/10000000/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up02/DIGI",
    # },
}

if __name__ == "__main__":
    # print container
    print("BKK path container:")
    pprint.pprint(bkk_paths)
