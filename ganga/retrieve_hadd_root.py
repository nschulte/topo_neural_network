"""Per-job, retrieve the ROOT file, hadd and move to EOS"""

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

import os, sys
from typing import Union, List, Dict
import subprocess
import pprint
from pathlib import Path


def prune_dict(
    targets: Dict[int, str],
    min_id: Union[int, None] = None,
    max_id: Union[int, None] = None,
) -> Dict:
    """Assuming a dict of structure {j.id : j.name},
    remove the keys outside the specified range"""
    if min_id is None:
        min_id = min(targets.keys())
    if max_id is None:
        max_id = max(targets.keys())

    return {k: v for k, v in targets.items() if min_id <= k <= max_id}


def check_duplicates(jobs: List) -> bool:
    """Check if any jobs have the same name"""
    assert len(set(jobs)) == len(jobs), "KeyError: duplicate job names"


def target_jobs(expr: str = "to_root", **kwargs) -> List:
    """Collect the j.id of jobs producing ROOT files"""

    # NOTE: this could be a lot more prettier/efficient but I had a CR night shift...
    targets = {}  # {j.id: j.name}
    for j in jobs:
        if expr in j.name:
            targets[j.id] = j.name

    # check for duplicates
    check_duplicates(list(targets.values()))

    return prune_dict(targets, **kwargs)


# prerequisite
gridProxy.renew()

# excute one ganga to get the job ids
targets = target_jobs()

# store directory
EOSPATH = "/eos/lhcb/user/b/bldelane/public/Hlt2BTopo/scratch"

Path("tmp").mkdir(parents=True, exist_ok=True)
for id in targets.keys():
    name = jobs(id).name

    # create a file with all the output files
    channel = name.split("_")[-9]

    # retrieve the output files and save them to eos/channel
    lfns = jobs(id).backend.getOutputDataAccessURLs()
    for rf in range(len(lfns)):
        Path(f"{EOSPATH}/{channel}/{rf}").mkdir(parents=True, exist_ok=True)
        cmd = "lb-dirac dirac-dms-get-file %s -D %s/%s/%s" % (
            lfns[rf],
            EOSPATH,
            channel,
            rf,
        )
        print("Executing: %s" % cmd)
        os.system(cmd)
        print("Done\n")
