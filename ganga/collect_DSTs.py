"""collect the DST locations for every job, keeping track of its ID

__author__ = Blaise Delaney
__email__  = blaise.delaney@cern.ch
"""

import sys
from subprocess import run

gridProxy.renew()

for j_id in [
    41,
    42,  
    43,  
    44,  
    45,  
    46,  
    47,  
    48,  
    49,  
    50,  
    51,  
    52,  
    53,  
    54,  
    55,  
    56,  
    57,  
    58,  
    60,  
    62,          
]:
    mode = jobs(j_id).name.replace("_DST", "")

    original_stdout = sys.stdout # Save a reference to the original standard output
    with open("/afs/cern.ch/user/b/bldelane/work/public/forTopo/LFNs/%s"%mode, "w") as f:
        print(jobs(j_id).backend.getOutputDataAccessURLs(), file=f)
        run(["touch", "collect_LFNs.done"])

