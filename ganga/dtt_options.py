###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Read the output of .dst file an HLT2 job with DecayTreeTuple, 
using a MooreAnalysis local build, with the necessary mods to MC TupleTools in the build.

The Moore HLT2 output analysis tutorial was closely followed:
    - https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_analysis.html

"""
__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from Configurables import ApplicationMgr, DecayTreeTuple, LHCbApp, createODIN
from DecayTreeTuple import Configuration  # noqa: magic import to augment DecayTreeTuple
from DecayTreeTuple import DecayTreeTupleTruthUtils
from PhysSelPython.Selections import (
    AutomaticData,
    CombineSelection,
    SelectionSequence,
)
from GaudiConf import reading, IOExtension
from PyConf.application import configured_ann_svc
from typing import Any


def get_hlt2_unpackers(manifest_file: Any, is_simulation: bool = True) -> Any:
    """Configures algorithms for reading HLT2 output.

    This is a temporary measure until support for Run 3 HLT2 output is added to
    an LHCb application.

    Source: https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_analysis.html#reading-data-with-mooreanalysis
    """

    unpack_raw_event = reading.unpack_rawevent(
        bank_types=["ODIN", "DstData", "HltDecReports"]
    )

    reading_algs = [unpack_raw_event]
    mc_unpackers = []
    if is_simulation:
        mc_unpackers = reading.mc_unpackers()

    manifest = reading.load_manifest(manifest_file)
    locations = reading.make_locations(manifest, "/Event/HLT2")

    decoder = reading.decoder()

    unpackers = reading.unpackers(
        locations, manifest, decoder.OutputBuffers, mc=mc_unpackers
    )

    reading_algs += [decoder]
    reading_algs += mc_unpackers
    reading_algs += unpackers
    reading_algs += [createODIN()]

    return reading_algs


# truthmatching
relations = [
    "Relations/ChargedPP2MCP",
    "Relations/NeutralPP2MCP",
]
mc_tools = [
    "MCTupleToolKinematic",
    # ...plus any other MC tuple tools you'd like to use
]
# tupletools presently available in MooreAnalysis/Phys
_tools = [
    # "TupleToolSLTruth",
    "TupleToolMCBackgroundInfo",
    "TupleToolMCTruth",
    "TupleToolKinematic",
    "TupleToolPid",
    "TupleToolGeometry",
]


# book 2-body
# -----------
TwoBody = DecayTreeTuple(
    "TwoBody",
    Inputs=[
        AutomaticData(
            "/Event/HLT2/Hlt2TwoBodyTopo_Hlt1FilteredLine/Particles"
        ).outputLocation()
    ],
    InputPrimaryVertices="/Event/HLT2/Rec/Vertex/Primary",
    Decay="B0 -> ^[pi+]CC ^[pi-]CC",
)

# use pions as placeholders for the beauty children
TwoBody.addBranches(
    {
        "TwoBody": "B0",
        "Track1": "B0 -> [^pi+]CC [pi-]CC",
        "Track2": "B0 -> [pi+]CC [^pi-]CC",
    }
)
TwoBody.ToolList += _tools
DecayTreeTupleTruthUtils.makeTruth(TwoBody, relations, mc_tools, stream="/Event/HLT2")

# book 3-body
# -----------
ThreeBody = DecayTreeTuple(
    "ThreeBody",
    Inputs=["/Event/HLT2/Hlt2ThreeBodyTopo_Hlt1FilteredLine/Particles"],
    InputPrimaryVertices="/Event/HLT2/Rec/Vertex/Primary",
    Decay="B_s0 -> ^(B0 -> ^[pi+]CC ^[pi-]CC) ^[pi+]CC",
)

ThreeBody.addBranches(
    {
        "ThreeBody": "B_s0",
        "TwoBody": "B_s0 -> ^(B0 -> [pi+]CC [pi-]CC) [pi+]CC",
        "Track1": "B_s0 -> (B0 -> [^pi+]CC [pi-]CC) [pi+]CC",
        "Track2": "B_s0 -> (B0 -> [pi+]CC [^pi-]CC) [pi+]CC",
        "TrackB": "B_s0 -> B0 ^[pi+]CC",
    }
)

ThreeBody.ToolList += _tools
DecayTreeTupleTruthUtils.makeTruth(ThreeBody, relations, mc_tools, stream="/Event/HLT2")

# configure the application
# -------------------------
user_algs = [TwoBody, ThreeBody]
LHCbApp().TupleFile = "my_hlt2.root"
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().CondDBtag = "sim-20210617-vc-md100"
LHCbApp().DDDBtag = "dddb-20210617"


# # I/O; NOTE: comment out when running ganga
# inputFiles = ["my_hlt2.dst"]
# IOExtension().inputFiles(inputFiles, clear=True)

# Load the 'TCK' dumped from the Moore job, assuming the TCK file was named
# like the Moore output file
manifest_file = LHCbApp().TupleFile.replace(".root", "") + ".tck.json"
print("manifest_file", manifest_file)

# Configure the unpacking of data (we assume we want MC information)
# and the running of the user algorithms. The order is important.
ApplicationMgr().TopAlg = get_hlt2_unpackers(
    manifest_file=manifest_file, is_simulation=LHCbApp().Simulation
)
ApplicationMgr().TopAlg += user_algs
ApplicationMgr().ExtSvc += [configured_ann_svc(json_file=manifest_file)]
