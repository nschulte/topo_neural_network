###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Ganga options to use DTT to generate ROOT tuples with HLT1 filtering in place.

This must run in a dedicated MooreAnalsysis build, 
see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/ganga.html#build 

Instructions:
$ ganga submit_dtt.py --hlt2_jid <HLT2 job ID> [--sjobs <n (int)>] [--test]
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

import argparse, pathlib

parser = argparse.ArgumentParser(description="Decay identifier")
parser.add_argument("-t", "--test", help="Run on local backend", action="store_true")
parser.add_argument("-sj", "--sjobs", help="Number of subjobs", type=int, default=5)
parser.add_argument(
    "-i",
    "--hlt2_jid",
    help="HLT2 job ID whose .dst files must be taken as input",
    required=True,
)
opts = parser.parse_args()

# prerequisite
gridProxy.renew()

# job label
_name = f"{opts.hlt2_jid}_to_root"

# configure jobs app
# ------------------
j = Job()
myApp = GaudiExec()
myApp.directory = f"{pathlib.Path().resolve().parent}/MooreAnalysisDev_master"
j.application = myApp
j.application.platform = "x86_64_v2-centos7-gcc11-opt"
j.application.options = ["dtt_options.py"]

# generate the dataset inheriting from hlt1-filtered my_hlt2.dst
input_job = opts.hlt2_jid
_name += f"__{jobs(input_job).name}__"  # bookeeping for sanity checks; dundr to annotate origin job
j.name = _name
ds = LHCbDataset()  # instantiate the dataset object
print(
    f"Job {j.name} inheriting HLT1-filtered my_hlt2.dst input from job {input_job}"
)  # RFE: sanity check

# build dataset from completed subjobs only
tck_json_container = []  # collect the TCKs associated with the same channel
for sj in jobs(input_job).subjobs.select(status="completed"):
    print(f"Adding {input_job}.{sj.id} to {j.name}")
    ds.extend(sj.backend.getOutputDataLFNs())
    tck_json_container.append(sj.outputfiles[1].accessURL()[0].replace("file://", ""))

# I/O
# ---
j.inputdata = ds
j.outputfiles = [DiracFile("*.root")]

# assign tck
for json_file in tck_json_container:
    assert (
        "json" in json_file
    ), "InputError: json container must contain only json files"
j.inputfiles = [
    f"{tck_json_container[-1]}"
]  # one representative tck json for all subjobs like to the same channel

# submit config
# -------------
if opts.test:
    _name += "_testjob"
    j.backend = Local()
else:
    j.backend = Dirac()
    j.backend.diracOpts = 'j.setTag(["/cvmfs/lhcbdev.cern.ch/"])'
    if opts.sjobs > 1:
        j.splitter = SplitByFiles(
            filesPerJob=opts.sjobs, ignoremissing=True, bulksubmit=False
        )
    else:
        j.splitter = SplitByFiles(filesPerJob=1, ignoremissing=True, bulksubmit=False)

j.submit()
