"""Run over all channels and submit jobs accordingly

Instructions:
$ python gen_sub_sh.py [--test] [--exclude <channel ID>] [--dryrun] [--local]

where: 
    --test: run on local backend using KPiPi MC as input
    --exclude: exclude a channel from the submission, running over all other keys in BKK container
    --dryrun: config the ganga job but do not submit
    --local: run on local backend using over all booked channels
"""

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__email__ = ["blaise.delaney@cern.ch", "nicole.schulte@cern.ch"]

import argparse
from dirac_paths import bkk_paths
from typing import List
import subprocess, pathlib, os

parser = argparse.ArgumentParser(description="Decay identifier")
parser.add_argument(
    "-t", "--test", help="Run on KPiPi MC with local backend", action="store_true"
)
parser.add_argument("-L", "--local", help="Run with local backend", action="store_true")
parser.add_argument(
    "-x", "--exclude", help="Which modes to exclude from the BKK keys", nargs="+"
)  # generates a list of strings; use: -x channel1 channel2 ...
parser.add_argument(
    "-d",
    "--dryrun",
    help="Build jobs but do NOT submit",
    action="store_true",
)
opts = parser.parse_args()


def verify_excluded(
    exclude: List[str],
    unfiltered_channels: List[str],
) -> List[str]:
    """Validate the mode exclusion, if booked

    Parameters
    ----------
    exclude: List[str]
        Modes to be excluded, passed by command line

    unfiltered_channels: List[str]
        List of channels to be filtered

    Returns
    -------
    all_channels: List[str]
        Trimmed channel list, passing sanity checks
    """
    # check the exclude list for validity
    for x in exclude:
        assert x in unfiltered_channels, f"KeyError: {x} not in BKK container"
    # update al_channels by removing the excluded channels
    all_channels = [x for x in unfiltered_channels if x not in exclude]
    assert len(unfiltered_channels) != len(
        all_channels
    ), "ItemError: the desired channels have not been excluded from the submission"

    return all_channels


def pipe(
    cmd: str,
    sh_file: str,
) -> None:
    """Print ganga submission command to run.sh file

    Parameters
    ----------
    cmd: str
        Command to be run

    sh_file: str
        File to write the command to
    """
    with open(sh_file, "w") as f:
        print(f"#!/bin/bash\n{cmd}", file=f)
    # make it executable
    os.chmod(sh_file, 0o755)
    print(f"SUCCESS: wrote {sh_file}")


# execute generated file - this should work but does not give a live print
# out output; switch to simply running the run.sh shell script
run_shfile = lambda sh_file: subprocess.run(
    ["bash", sh_file], capture_output=True, text=True
)

# source all the channel IDs
all_channels = list(bkk_paths.keys())

# exclude channels if specified
if opts.exclude:
    all_channels = verify_excluded(
        exclude=opts.exclude, unfiltered_channels=all_channels
    )


# run shell scripts and log
pathlib.Path("submission_logs").mkdir(parents=True, exist_ok=True)
if opts.test:
    # submit on local backend
    cmd = "ganga submit_hlt1.py --channel KPiPi --test 2>&1 | tee submission_logs/test_on_KPiPi.log"
    # write to bash script
    pipe(cmd, "run.sh")
else:
    cmd = ""
    for ch in all_channels:
        cmd += f"ganga submit_hlt1.py -c {ch}"
        if opts.local:
            cmd += " --test"  # local backend
        if opts.dryrun:
            cmd += " --dryrun"  # do not submit
        cmd += f" 2>&1 | tee submission_logs/{ch}.log && \n"  # log and proceed only if previous command was successful
    cmd += "echo ===== SUCCESS ====="  # print success message
    # write to bash script
    pipe(cmd, "run.sh")

# # run the bash script
# run_shfile("run.sh")
