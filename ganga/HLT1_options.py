###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Running hlt1 is required to persist the hlt1 trigger decisions.

Based on:
- https://gitlab.cern.ch/amathad/bachelors-project-uzh-2022/-/blob/master/Hlt1_Hlt2_DV_options/Hlt1/run_myhlt1_MagDown.py
- Help provided by N. Skidmore: https://gitlab.cern.ch/-/snippets/2259

Instructions:
$ Moore/run gaudirun.py <path/to/HLT1_options.py>
"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

from Moore.config import options, allen_control_flow
from RecoConf.hlt1_allen import call_allen_decision_logger
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.hlt1_allen import allen_gaudi_node_barriers as allen_sequence
from AllenCore.generator import make_transposed_raw_banks
from RecoConf.decoders import default_ft_decoding_version

# # job options - comment out when Ganga-submitting
# # -----------------------------------------------
# # input files and conditions
# _tmp_path = "/home/blaised/private/stack/Moore/Hlt/Hlt2Conf/python/Hlt2Conf/lines/topological_b/" # local path to downloaded LFNs
# options.input_files = [
#     _tmp_path + "tmp/00147893_00000002_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000006_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000012_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000019_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000023_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000028_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000029_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000033_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000046_1.xdigi",
#     #_tmp_path + "tmp/00147893_00000049_1.xdigi",
# ]
options.data_type = "Upgrade"
options.simulation = True
options.conddb_tag = "sim-20210617-vc-md100"
options.dddb_tag = "dddb-20210617"
options.input_type = "ROOT"
options.input_raw_format = 0.5
# https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/different_samples.html?highlight=default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)
options.evt_max = -1
# generate a dst file
options.output_file = "myhlt1.dst"
options.output_type = "ROOT"

# run the application - with many thanks to N. Skidmmore for the help
# see https://gitlab.cern.ch/-/snippets/2259
# -------------------------------------------------------------------
# Run Allen for MC without Retina clusters ("hlt1_pp_veloSP") and log HLT1 decisions
with allen_sequence.bind(sequence="hlt1_pp_veloSP"):
    # note that the `transpose_banks.bind` part has been removed as redundant, see https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/1718
    config = configure_input(options)
    allen_node, allen_barriers = allen_control_flow(options)
    top_cf_node = CompositeNode(
        "MooreAllenWithLogger",
        combine_logic=NodeLogic.LAZY_AND,
        children=[allen_node, call_allen_decision_logger()],
        force_order=True,
    )
    config.update(configure(options, top_cf_node, barrier_algorithms=allen_barriers))
