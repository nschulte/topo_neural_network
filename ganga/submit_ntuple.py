__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

import sys

print(sys.argv[1])
if len(sys.argv) > 2:
    print(sys.argv[2])

gridProxy.renew()
test = False
name = str(sys.argv[1]) + "_tuple"

if test:
    name = "testjob"

options = "/afs/cern.ch/user/b/bldelane/work/public/forTopo/Tupling/TOPOopts.py"

j = Job()
j.name = name
myApp = GaudiExec()
myApp.directory = (
    "/afs/cern.ch/user/b/bldelane/work/public/forTopo/MooreAnalysisDev_master"
)

j.application = myApp
j.application.platform = "x86_64_v2-centos7-gcc11-opt"

j.application.options = [options]

ds = LHCbDataset()
if len(sys.argv) > 2:
    jobForInput = int(sys.argv[2])
print(f"Job {name} inheriting from DST job {jobForInput}")
assert sys.argv[1] == jobs(jobForInput).name.replace("_DST", "")

for sj in jobs(jobForInput).subjobs.select(status="completed"):
    ds.extend(sj.backend.getOutputDataLFNs())

j.inputdata = ds
j.outputfiles = [LocalFile("my_hlt2.root")]
j.inputfiles = ["my_hlt2.tck.json"]

if test:
    j.backend = Local()
else:
    j.backend = Dirac()
    j.backend.diracOpts = 'j.setTag(["/cvmfs/lhcbdev.cern.ch/"])'
    j.splitter = SplitByFiles(filesPerJob=1, ignoremissing=True, bulksubmit=False)

j.submit()
