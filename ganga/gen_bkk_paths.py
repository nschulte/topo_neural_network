"""Generate the bookeeping paths for the decay channels"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

import re, pprint
from typing import Callable
from dirac_paths import bkk_paths
from utils import source_bkk_dict


def extract_evtn(path: str) -> str:
    """Extract the first numerical sequence from raw dirac path

    Parameters
    ----------
    path: str
        Dirac GUI bookeeping path

    Returns
    -------
    str
        Event number identifier
    """
    return re.findall("\d+", path)[0]


def to_bkk_path(
    dirac_str: str,
) -> str:
    """Process the dirac path to generate bookeeping path

    Parameters
    ----------
    dirac_str: str
        Path obtained from Dirac GUI

    Returns
    -------
    dirac_str: str
        Updated path with structure amended to comply with ganga syntax
    """
    dirac_str = dirac_str.replace("evt+std:/", "", 1)
    try:
        _evtn = extract_evtn(dirac_str)
        if "XDIGI" in dirac_str:
            dirac_str = (
                dirac_str.replace(f"Upgrade/{_evtn}", "Upgrade").replace("XDIGI", "")
                + f"{_evtn}/XDIGI"
            )
        else:
            dirac_str = (
                dirac_str.replace(f"Upgrade/{_evtn}", "Upgrade").replace("DIGI", "")
                + f"{_evtn}/DIGI"
            )
    except:
        pass

    return dirac_str


def process(
    input_dict: dict,
) -> dict:
    """Generate the bookeeping paths compatible with ganga job

    Parameters
    ----------
    input_dict: dict
        Container for the dirac paths

    Returns
    -------
        Dict of per-channel paths compatible with ganga syntax
    """
    for mode_val in input_dict.values():
        for m in (
            "MagUp",
        ):  # HACK: by convention, consider one polarity only; MagUp by convetion
            mode_val[m] = to_bkk_path(mode_val[m])

    # HACK: DsPi is only MagDown
    input_dict["DsPi"]["MagUp"] = to_bkk_path(input_dict["DsPi"]["MagDown"])

    return input_dict


def main() -> Callable:
    """Closure for to_bkk_path, so that it can bea standalone callable
    with no args or kwargs

    Returns
    -------
    Callable
        `process()` function executed to return the dict of processed paths
    """
    # load the BKK paths whilst performing sanity checks
    dpaths = source_bkk_dict(bkk_paths)
    return process(dpaths)


if __name__ == "__main__":
    pprint.pprint(main())
