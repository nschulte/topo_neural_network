"""Ganga options to assemble the HLT2 beauty candidates using HLT1-filtered simulated samples.

This must run in a dedicated Moore build, 
see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/ganga.html#build 

Instructions:
$ ganga submit_hlt2.py --hlt1_jid <HLT1 job ID> [--sjobs <n (int)>] [--test]
"""
import argparse
import pathlib
from typing import List, Union

__author__ = "Blaise Delaney, Nicole Schulte"
__email__ = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"

parser = argparse.ArgumentParser(description="Decay identifier")
parser.add_argument("-t", "--test", help="Run on local backend", action="store_true")
parser.add_argument("-sj", "--sjobs", help="Number of subjobs", type=int, default=5)
parser.add_argument(
    "-i",
    "--hlt1_jid",
    help="Job ID whose hlt1-filtered .dst files must be taken as input",
    required=True,
)
opts = parser.parse_args()

# prerequisite
gridProxy.renew()

# job label
_name = f"{opts.hlt1_jid}_to_HLT2cands_dst"

# configure jobs app
j = Job()
myApp = GaudiExec()
myApp.directory = f"{pathlib.Path().resolve().parent}/MooreDev_master"
j.application = myApp
j.application.platform = "x86_64_v2-centos7-gcc11-opt"
j.application.options = ["HLT2_options.py"]

# generate the dataset inheriting from hlt1-filtered .dst
input_job = opts.hlt1_jid
_name += f"__{jobs(input_job).name}__"  # bookeeping for sanity checks; dundr to annotate origin job
j.name = _name
ds = LHCbDataset()  # instantiate the dataset object
print(f"Job {j.name} inheriting HLT1-filtered .dst input from job {input_job}")
# RFE: sanity check

# build dataset from completed subjobs only
for sj in jobs(input_job).subjobs.select(status="completed"):
    print(f"Adding {input_job}.{sj.id} to {j.name}")
    ds.extend(sj.backend.getOutputDataLFNs())

# I/O
j.inputdata = ds
j.outputfiles = [DiracFile("*.dst"), LocalFile("*.json")]

# submit config
if opts.test:
    _name += "_testjob"
    j.backend = Local()
else:
    j.backend = Dirac()
    j.backend.diracOpts = 'j.setTag(["/cvmfs/lhcbdev.cern.ch/"])'
    if opts.sjobs > 1:
        j.splitter = SplitByFiles(
            filesPerJob=opts.sjobs, ignoremissing=True, bulksubmit=False
        )
    else:
        j.splitter = SplitByFiles(filesPerJob=1, ignoremissing=True, bulksubmit=False)

j.submit()
