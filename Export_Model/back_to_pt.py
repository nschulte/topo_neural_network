"""Export Json File to PyTorch Model"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__    = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
__modelpath__= "/ceph/users/nschulte/Trained_models"
__rootpath__ = "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/scratchN"
__mainpath__ = "/ceph/users/nschulte/topo_neural_network" # comment out as needed
#__mainpath__ = f"{__mainpath__}" # comment out as needed

import json
from collections import OrderedDict

from numpy import var
import torch
from torch import nn
from matplotlib.pyplot import broken_barh
from argparse import ArgumentParser
from pathlib import Path
import pandas as pd
import numpy as np


import sys
sys.path.insert(1,__mainpath__)
from pipeline.utils import read_config
from pipeline.mpl_config import *
from pipeline.train import get_model
with open("/ceph/users/nschulte/topo_neural_network/Export_Model/scratch/TwoBody_topo_sigmanet.json") as json_model:
    data = json.load(json_model, object_pairs_hook=OrderedDict) #StateDict in PyTorch is always an OrderedDict. Stick to that

del(data["variables_constraints"]) #dont need them for now
data = {k.replace('sigmanet.', ''): torch.tensor(v) for k, v in data.items()} #Remove sigmanet string from keynames in case this causes issues

def load_model(nn_config):
    if(torch.cuda.is_available()):
        device = torch.device("cuda")
        print("Running on cuda")
    else:
        device = torch.device("cpu")
        print("Running on cpu")

    MODEL = get_model(
        **nn_config
    ).to(device)

    MODEL.load_state_dict(data)

    return MODEL

if __name__ == '__main__':
    parser = ArgumentParser(description="Model configuration")
    parser.add_argument('-f','--features',  default=f"{__mainpath__}/pipeline/features.yml",
        help="features of the model")
    parser.add_argument('-b','--nbody',     required=False, choices=["TwoBody", "ThreeBody"],
        help="n-body implementation ['TwoBody', 'ThreeBody']", default = "TwoBody")
    parser.add_argument('-l','--Lambda',    required=False,
        help="Lipschitz constant value", default = "1.75")
    parser.add_argument('-M','--monotonic', action="store_false",
        help="engage monotonicity? [default: True]")
    parser.add_argument('-R','--robust', action="store_false",
        help="engage robustness? [default: True]")
    opts = parser.parse_args()

    if opts.Lambda!="None": # ugly hack
        opts.Lambda = float(opts.Lambda)

    #variables_constraints
    FEATURES = read_config(opts.features, _nbody=opts.nbody)
    nn_config = {
        "robust"    : opts.robust,
        "monotonic" : opts.monotonic,
        "_nbody"    : opts.nbody,
        "_features" : FEATURES,
        "LIP"       : opts.Lambda,
    }

    model = load_model(nn_config)

    phys_sample = pd.read_csv("/ceph/users/nschulte/topo_neural_network/standalone_inference/scratch/DsPi/Implementationtest_TwoBody.csv")
    print(phys_sample)
    #X = torch.tensor(phys_sample[FEATURES].to_numpy().astype(np.float32)).to("cpu")
    X = torch.tensor([0.502415,4.97842,4.54457,9.8016,4.94935,0.0714092,10.5117,0.189736]).to("cpu")

    prediction = model(X).cpu().detach().numpy()
    print("Response:", prediction)
    # phys_sample["preds_per_cand"] = torch.sigmoid(model(X)).cpu().detach().numpy()

    # phys_sample["response"] = model(X).cpu().detach().numpy()

    # assert (phys_sample["response"].max() < 100)
    # print(phys_sample["preds_per_cand"])
    # print(phys_sample)
