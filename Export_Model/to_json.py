"""Export Model to Json File"""

__author__ = "Blaise Delaney, Nicole Schulte"
__email__    = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
__modelpath__= "/ceph/users/nschulte/Trained_models"
__rootpath__ = "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/scratchN"
__mainpath__ = "/ceph/users/nschulte/topo_neural_network" # comment out as needed
#__mainpath__ = f"{__mainpath__}" # comment out as needed

from numpy import var
import torch
from torch import nn
from matplotlib.pyplot import broken_barh
from argparse import ArgumentParser
from pathlib import Path
import json
import pandas as pd
from monotonenorm import get_normed_weights
import numpy as np


import sys
sys.path.insert(1,__mainpath__)
from pipeline.utils import read_config
from pipeline.mpl_config import *
from pipeline.train import get_model


def load_model(nn_config):
    if(torch.cuda.is_available()):
        device = torch.device("cuda")
        print("Running on cuda")
    else:
        device = torch.device("cpu")
        print("Running on cpu")

    MODEL = get_model(
        **nn_config
    ).to(device)

    location = f"{__modelpath__}/{opts.nbody}_trained_model.pt"
    if(device == torch.device('cpu')):
        MODEL.load_state_dict(torch.load(location, map_location=torch.device('cpu'))) #Map Location needed if running on cpu
    else:
        MODEL.load_state_dict(torch.load(location))
    return MODEL

def assign_constraints(nbody): #in the future, we should link this directly to the pipeline, but for now this is okay
    if nbody == "TwoBody":
        variables_constraints = [
            [0.014,5.298],      #min_PT_final_state_tracks
            [1.000,16.384],     #sum_PT_final_state_tracks
            [1.379,12.489],     #min_FS_IPCHI2_OWNPV
            [1.388,14.494],     #max_FS_IPCHI2_OWNPV
            [0.152,15.921],     #TwoBody_PT
            [4.302e-13,6.758],  #TwoBody_ENDVERTEX_DOCAMAX
            [0.418, 19.061],    #TwoBody_FDCHI2_OWNPV
            [3.868e-11,9.999]   #TwoBody_ENDVERTEX_CHI2
        ]
    if nbody == "ThreeBody":
        variables_constraints = [
            [0.323,2.578],      #min_FS_IPCHI2_OWNPV
            [1.388,13.904],     #max_FS_IPCHI2_OWNPV
            [1.000,14.345],     #sum_PT_TRACK12
            [0.015,4.812],      #min_PT_TRACK12
            [1.075,20.198],     #sum_PT_final_state_tracks
            [0.015,3.360],      #min_PT_final_state_tracks
            [0,6.305],          #TwoBody_ENDVERTEX_DOCAMAX
            [2.834e-07,12.599], #ThreeBody_ENDVERTEX_DOCAMAX
            [2.468,17.763],     #TwoBody_FDCHI2_OWNPV
            [-5.697,14.314],    #TwoBody_IPCHI2_OWNPV
            [-2.042,17.948],    #ThreeBody_FDCHI2_OWNPV
            [ 0.855, 20.073],   #ThreeBody_PT
            [0.990,14.258],     #TwoBody_PT
            [3.055e-11,9.999],  #TwoBody_ENDVERTEX_CHI2
            [-13.301,9.556],    #ThreeBody_ENDVERTEX_CHI2

        ]
    return variables_constraints


if __name__ == '__main__':
    parser = ArgumentParser(description="Model configuration")
    parser.add_argument('-f','--features',  default=f"{__mainpath__}/pipeline/features.yml",
        help="features of the model")
    parser.add_argument('-b','--nbody',     required=False, choices=["TwoBody", "ThreeBody"],
        help="n-body implementation ['TwoBody', 'ThreeBody']", default = "TwoBody")
    parser.add_argument('-l','--Lambda',    required=False,
        help="Lipschitz constant value", default = "1.75")
    parser.add_argument('-M','--monotonic', action="store_false",
        help="engage monotonicity? [default: True]", default = True)
    parser.add_argument('-R','--robust', action="store_false",
        help="engage robustness? [default: True]", default = True)
    opts = parser.parse_args()

    if opts.Lambda!="None": # ugly hack
        opts.Lambda = float(opts.Lambda)


    #variables_constraints
    FEATURES = read_config(opts.features, _nbody=opts.nbody)
    nn_config = {
        "robust"    : opts.robust,
        "monotonic" : opts.monotonic,
        "_nbody"    : opts.nbody,
        "_features" : FEATURES,
        "LIP"       : opts.Lambda,
    }

    model = load_model(nn_config)
    print(model)


    # phys_sample = pd.read_csv("/ceph/users/nschulte/topo_neural_network/standalone_inference/scratch/DsPi/Implementationtest_TwoBody.csv")
    # print(phys_sample)
    # X = torch.tensor(phys_sample[FEATURES].to_numpy().astype(np.float32)).to("cpu")
    # X = torch.tensor([0.502415,4.97842,4.54457,9.8016,4.94935,0.0714092,10.5117,0.189736]).to("cpu")


    # prediction = model(X).cpu().detach().numpy()
    # print("Response:", prediction)

    state_dict = model.state_dict()
    print("Type:", type(state_dict))
    dict_keys = list(state_dict.keys())


    #rename keys according to stack requirements, beware of OrderedDict
    for key in dict_keys:
        state_dict["sigmanet."+key] = state_dict[key]
        del state_dict[key]

    weight_keys = [x for x in state_dict if "weight" in x]
    depth = len(weight_keys)
    assert depth > 0

    for k in state_dict:
        if k in weight_keys:
            state_dict[k] = get_normed_weights(
            state_dict[k],
            always_norm= False,
            kind = "one",
            alpha = 1.75 ** (1.0 / depth),
            vectorwise = True,
            ).tolist()
        else:
            state_dict[k] = state_dict[k].tolist()

    #get constrains for clamping and move this to the beginning of the dict
    variable_constrains  = assign_constraints(f"{opts.nbody}")
    state_dict.update({'variables_constraints': variable_constrains})
    state_dict.move_to_end('variables_constraints', last = False)

    with open(f"scratch/{opts.nbody}_topo_sigmanet.json", "w") as f: #for formatting, go in the json file and press strg+shift+I
        json.dump(state_dict, f)
