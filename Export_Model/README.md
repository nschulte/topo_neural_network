Files to export the trained pytorch (.pt) model to a json file which is later passed to Paramfiles.

To launch the script simply put in 

```
python to_json.py 
``` 

The default is set to TwoBody. If you want to export the ThreeBody model put in 

```
python to_json.py -b ThreeBody
``` 

The variable constrains are passed to the model in a hard-coded manner for now. 

To Do for later: 
- export the 5sigma windows of the model directly to a config file in the training and pass is here in the model
