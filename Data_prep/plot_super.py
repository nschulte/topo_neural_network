"""Plotting base"""

__author__ = "Blaise Delaney"
__email__ = "blaise.delaney@cern.ch"

from builtins import breakpoint
import numpy as np
import matplotlib.pyplot as plt
import boost_histogram as bh
from typing import List, Union, Tuple
from numpy.typing import ArrayLike
import mplhep as hep

#plt.style.use("science") # https://github.com/garrettj403/SciencePlots


"""Example for Nicole, 2021-09-01
<import stuff here>

fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(6, 6))
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_title("title")

data = np.random.normal(0, 1, 1000)
wts = np.random.normal(0, 1, 1000) # weights, common in analysis but not for this example

# plot data on upper pad
plot_data(
    data, 
    ax=ax1, 
    bins=20, 
    range=[-5, 5], 
    wts=wts, 
    isnorm=True, 
    label="data", 
col="black")

# plot normalised histogram on lower pad
norm_errorbar(
    data,
    bins=20,
    range=[-5, 5],
    _sw=wts,
    _ax=ax0,
    _isnorm=True,
    label="data",
    color="blue",
)
"""


def plot_data(
    data: ArrayLike,
    ax: plt.Axes,
    bins: int,
    range: Union[List[float], List[int]],
    wts: ArrayLike = None,
    isnorm: bool = False,
    label: str = "",
    col: str = "black",
    markersize=3,
    elinewidth=1,
    capsize=1,
    markeredgewidth=1,
    **kwargs,
) -> Tuple[ArrayLike, ...]:
    """plotting utility for the data

    Parameters
    ----------
    data: ArrayLike
        fitted observable

    ax: plt.Axes
        suplot

    bins: int
        Number of bins in errorbar hist

    range: Union[List[float], List[int]],
        Boundaries of the observable histogram

    wts: ArrayLike, default is None
        Weights applied. Must have the same shape as data

    isnorm: bool, default is False
        If True, normalise the observable distribution to attain unity area

    label: str, default is ''
        Label in legend

    col: str, default is 'black'
        Color of the data points in the plot

    **kwargs
        Styling keyword args passed to plt.errorbar
    """
    nh, xe = np.histogram(data, bins=bins, range=range)
    cx = 0.5 * (xe[1:] + xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram(bh.axis.Regular(bins, *range), storage=bh.storage.Weight())
        whist.fill(data, weight=wts)
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance ** 0.5

    if isnorm:
        norm = np.sum(nh)
        nh = nh / norm
        err = err / norm

    ax.errorbar(
        cx,
        nh,
        yerr=err,
        xerr=(xe[1] - xe[0]) / 2.0,
        fmt=".",
        color=col,
        markersize=3,
        elinewidth=elinewidth,
        capsize=capsize,
        markeredgewidth=markeredgewidth,
        label=label,
        **kwargs,
    )
    return nh, cx, err


def norm_hist(
    _branch,
    _sw=None,
    _ax=None,
    _isnorm=True,
    **kwargs,
) -> "hist":
    """plot normalised hist; accepts sweights"""
    norm_w = np.ones_like(_branch)
    if _isnorm:
        norm_w /= len(_branch)
    if _sw is not None:
        norm_w *= _sw  # not the sweighted hist, then normalised - sloppy but OK for now

    _ax.hist(_branch, weights=norm_w, **kwargs)


def norm_errobar(
    _branch,
    bins,
    range,
    label,
    histcolor,
    _sw=None,
    _ax=None,
    _isnorm=True,
    **kwargs,
) -> "errorbar":
    """plot normalised errorbar; accepts sweights"""
    norm_w = np.ones_like(_branch)
    if _isnorm:
        norm_w /= len(_branch)
    if _sw is not None:
        norm_w *= _sw  # not the sweighted hist, then normalised - sloppy but OK for now

    pop, e = np.histogram(_branch, weights=norm_w, bins=bins, range=range)
    #breakpoint()
    # assert abs(np.sum(pop) - 1.0) < 1e-5
    _ax.hist(_branch, weights=norm_w, bins=bins, range=range, color=histcolor, alpha=0.5)
    _ax.errorbar(
        x=(e[1:] + e[:-1]) / 2,
        y=pop,
        xerr=(e[1:] - e[:-1]) / 2,
        fmt=".",
        markersize=3,
        elinewidth=1.0,
        label=label,
        **kwargs,
    )


def plot_hist_err(
    data, ax, bins, range, wts=None, _isnorm=False, label=None, color=None, **kwargs
):
    nh, xe = np.histogram(data, bins=bins, range=range)
    cx = 0.5 * (xe[1:] + xe[:-1])
    err = nh**0.5
    if wts is not None:
        whist = bh.Histogram(bh.axis.Regular(bins, *range), storage=bh.storage.Weight())
        whist.fill(data, weight=wts)
        cx = whist.axes[0].centers
        nh = whist.view().value
        err = whist.view().variance ** 0.5

    if _isnorm:
        norm = np.sum(nh)
        _nh = nh
        _err = err
        # FIXME: somethimes the division by norm breaks because of type issues
        nh = _nh / norm
        err = _err / norm

    hep.histplot(nh, xe, yerr=err, ax=ax, color=color, label=label, **kwargs)

    return nh, cx, err
