#Load the Root Files and put them into a balanced sample

__author__        = "Blaise Delaney, Nicole Schulte"
__email__         = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
__modelpath__     = "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/Lip_M_NN"
__rootpath__      = "/eos/lhcb/user/b/bldelane/public/Hlt2BTopo/scratch"
#__mainpath__     = "/home/blaised/private/hlt2_topo/topo_neural_network" # comment out as needed
__mainpath__      = "/ceph/users/nschulte/topo_neural_network" # comment out as needed
__oldtuplespath__ = "/ceph/users/nschulte/new_tuples_topo/Dec21TopoTuples"


from operator import not_, truth
from tkinter import N
from typing import List, Dict, Tuple
import uproot3
from argparse import ArgumentParser
import pandas as pd
import yaml
import numpy as np
from termcolor2 import c
import matplotlib.pyplot as plt
from particle.pdgid import literals as lid
from particle import Particle
import pathlib



import warnings
warnings.simplefilter(action='ignore', category=FutureWarning) #ignore pandas warning about frame.append -> pandas.concat for now

import sys
sys.path.insert(1,__mainpath__)
from pipeline.utils import read_config
from Utils.channel_config import channels



class Load_Data:

    def dataframe(path: str,
                  columns: List[str],
                  nbody:str,
                  name:str,
                  new_tuples: bool=True
    ) -> pd.DataFrame:
        """Create a Dataframe of a given input root file; general function

        Parameters
        ----------
        path: str
            Path where the root files are stored
        columns: List[str]
            List of the branches of interested. Retrieved from yml configuration file

        Returns
        -------
        pandas.DataFrame
            DataFrame of the sample given the branches of interest
        """
        file = uproot3.open(path)
        if new_tuples:
            tree = file[f"{nbody}/DecayTree"]
        else:
            if name != 'minbias':
                tree = file[f'{nbody}']
            else:
                tree = file[f'{nbody}/DecayTree']

        for column in columns:
            if column not in tree:
                raise KeyError(f'Branch {column} not found in file {file}')
            else:
                continue
        if name != "minbias":
            df = tree.pandas.df(columns, flatten=False)
        else: df = tree.pandas.df(columns, flatten=False, entrystop=1000000)
        #Note: Flatten=False is super important here. Because there are some features that naturally have multiple
        #entries, like nbody_DAUGHTERS. This way, the structure of the entries is preserved (if something is an array,
        # it remains as an array)
        return df

    def read_modes(path: str
    ) -> Dict:
        """Read the Modes from configuration file

        Parameters
        ----------
        path: str
            Path where the yml config file is stored

        Returns
        -------
        Dict
            Dictionary with sample name and event type number
        """
        with open(path, 'r') as stream:
            yml_dict = yaml.load(stream, Loader=yaml.FullLoader)

        modes = yml_dict['Modes']
        return modes


    def import_data(branches:List[str],
                    modes: Dict,
                    nbody:str,
                    pickled: bool = False,
                    pkl_name: str =None,
                    path: str =None,
                    new_tuples: bool = True
    ) -> pd.DataFrame:
        """File specific importing of the tuples and putting them into a dataframe; concat them together to get
        a big dataframe containing all modes

        Parameters
        ----------
        branches: List[str]
            List of the branches of interested. Retrieved from yml configuration file
        modes: Dict
            Dictionary of the used mode containing name and event type number
        pickled: bool
            Option to put uprooted data into a pickle file; recommended for faster load later
        name: str
            For the pickled option; Name for the pickle file
        path: str
            For the pickled option; Path where the pickle file is supposed to be stored; Recommended: scratch folder
        Returns
        -------
        Dataframe
            Final Dataframe combining the functions from above containing all samples and branches specified in the yml files
        """
        all_data = pd.DataFrame(columns=branches)

        for name, event_type in modes.items():

            if new_tuples:
                filename = f"{__rootpath__}/{name}/{name}_hlt1filtered.root"
            else:
                print(c(f"Beware, I am looking at the old tuples right now!").red) 
                filename = f"{__oldtuplespath__}/{name}.root"

            print(c(f'{name} started processing').yellow)
            
            sample_dataframe = Load_Data.dataframe(filename, branches,nbody, name, new_tuples)
            sample_dataframe["name"] = name
            print(f"File {name} has {len(sample_dataframe)} entries")
            all_data = all_data.append(sample_dataframe)
            #apparently append will be removed from pandas soon. They suggest concat but I am
            #unsure about memory allocation

        if pickled and new_tuples:
            all_data.to_pickle(f"{path}{pkl_name}_newtuples.pkl")

        return all_data

    def load_pickled_data(pkl_name: str,
                          path: str,
    ) -> pd.DataFrame:

        dataframe = pd.read_pickle(f"{path}{pkl_name}.pkl")

        return dataframe

class Prepare_Data:

    def create_unique_events(dataframe: pd.DataFrame
    ) -> pd.DataFrame:
        """Add unique event information on a given input dataframe; for that run_number and event_number are needed in the dataframe

        Parameters
        ----------
        dataframe: pandas.DataFrame
            Input Dataframe for which the unique event information is needed

        Returns
        -------
        dataframe: pandas.DataFrame
            Returns a DataFrame which now contains unique event information
        """
        #events should appear multiple times in the dataframe as there are multiple candidates
        #in an event. This way, the amount of unique events are filtered out.
        event_ids = ['runNumber', 'eventNumber']
        unique_events = dataframe.groupby(event_ids)[event_ids].max()

        unique_events['unique_event'] = range(len(unique_events))
        dataframe = dataframe.join(unique_events['unique_event'], on=event_ids)
        assert(all(dataframe.groupby(event_ids).unique_event.nunique() == 1))

        #Check the amount of events and final composites in the minbias sample
        print("Unique Events in Minbias:", dataframe[dataframe.name == "minbias"].unique_event.nunique())
        print("Composites in Minbias:", len(dataframe[dataframe.name == "minbias"]))
        return dataframe

    def clean_minbias(dataframe: pd.DataFrame,
                     _nbody: str
    ) -> pd.DataFrame:
        """Clean the minbias sample from any beauty decay events for more purity in the training

        Parameters
        ----------
        dataframe: pd.DataFrame
            Input dataframe which requires cleaning of the minbias sample
        _nbody: str
            Passed in the Argumentparser; TwoBody and ThreeBody as possibility

        Returns
        -------
        dataframe: pd.DataFrame
            Returns DataFrame which now contains a clean minbias
        """
        #Replace -1 with 0 otherwise the computer will not understand that -1 corresponds to False
        SameBs_minbias = dataframe[f"{_nbody}_FromSameB"].replace({-1:0}, inplace = True) 
        SameBs_minbias = dataframe[dataframe.name == 'minbias'].groupby('unique_event')[f"{_nbody}_FromSameB"].max().astype(bool)
        SameBs_minbias = SameBs_minbias[SameBs_minbias].index
        dataframe = dataframe.set_index('unique_event').drop(SameBs_minbias).reset_index()
        print("Unique Events with FromSameB composites in MinBias:", len(SameBs_minbias))
        #This should be zero if the removal worked
        assert(dataframe[dataframe.name == "minbias"][f"{_nbody}_FromSameB"].sum() == 0)
        return dataframe

class Truth_Matching:

    def truth_match(dataframe: pd.DataFrame,
                    truth_dict: Dict,
                    _n_body: str
    ) -> pd.DataFrame:
        """Calculate the truth matching/is the candidate a signal candidate or not?

        Parameters
        ----------
        dataframe: pd.DataFrame
            Input dataframe containing all samples
        truth_dict: Dict
            Dictionary imported from config file containing the truth information
        _n_body: str
            TwoBody or ThreeBody
        Returns
        -------
        pd.Dataframe:
            Returns a new dataframe containing only the truth information and nothing else
        """
        sample_df_list = []

        for name in dataframe.name.unique():
            sample =  dataframe[dataframe.name == name].copy()
            pid_array = sample[f"{_n_body}_DAUGHTERS"]

            intermediate_check = []
            print(name)
            if name != 'minbias':
                print(c(f"Beginning Truth Matching for {name}").magenta)
                true_pid = np.array(truth_dict[f"{name}"]["truth"])
                print(c(f"Truth: {true_pid} for {name}").green)
                print(c(f"First PIDs: {pid_array[pid_array.str.len() != 0].head(20)}"))
                for i in range(len(pid_array)):
                    match = (np.array_equiv(abs(pid_array[i]), abs(true_pid)))#take the absolute as there are no negative pids in the tuples
                    intermediate_check.append(match)

            if name == 'minbias':
                for i in range(len(pid_array)):
                    intermediate_check.append(False)

            sample[f"{_n_body}_FromSignalB"] = intermediate_check
            print(f"Intermediate check tells me {sum(intermediate_check)} candidates are in there")
            print(f"Intermediate check tells me it has {len(intermediate_check)} combinations in there")
            sample_df_list.append(sample)
        
        truth_matched_df = pd.concat(sample_df_list)     
        return truth_matched_df


    def assign_labels(dataframe: pd.DataFrame,
                      _n_body: str
    ) -> pd.DataFrame:
        """Add FromSameB information to the dataframe as well as a label which is the same, but not as bool but as integer
        Parameters
        ----------
        dataframe: pd.DataFrame
            Input dataframe containing all samples
        matched_dataframe: pd.DataFrame
            Truth DataFrame form truth_match
        _n_body: str
            TwoBody or ThreeBody
        Returns
        -------
        pd.Dataframe:
            Returns the Input Dataframe with added information on the SignalB and label

        TO DO: this could be more refined. There is a lot of memory allocation here, so be careful; 
        """

        dataframe["label"] = dataframe[f"{_n_body}_FromSignalB"].astype(int)

        for name in dataframe.name.unique():
            print(f" sample {name} has {len(dataframe[(dataframe.name == name) & (dataframe.label == 1)])} signal candidates")
            if name == 'minbias':
                #make sure there is only one unique label in minbias. Which should be 0
                assert(dataframe[dataframe.name == "minbias"].label.nunique() == 1)
        return dataframe

    def pid_plot(
        nom_var:"var in tuple",
        truth_var:"truth_variable",
        df:"control dataframe",
        NBODY:"b-hadron branch name",
        target_pids:"set the dimension",   
        channel:"string",
        **kwargs   
    )->"plot":

        frame = df[df.name == channel]
        truth_frame = frame[[f"{NBODY}_{nom_var}", f"{NBODY}_{truth_var}"]].explode(f"{NBODY}_{nom_var}")
        s1 = truth_frame.index.to_series()
        s2 = s1.groupby(s1).cumcount()
        truth_frame.index = [truth_frame.index, s2]
        truth_frame.dropna(inplace=True) #many empty arrays in there
        target_pids = abs(target_pids)
        for i in range(0,len(target_pids),1):

            particle_level = truth_frame[f"{NBODY}_{nom_var}"].xs(i, axis=0, level=1, drop_level=False)
            particle_level = particle_level.replace({20413.0:"D1(H)+", 43.0:"Xu0", 104124.0:"Lambda_c(2625)+", 44.0:"Xu+", 4432.0:"Omega_cc+", 4434.0:"Omega*_cc+"}) #manually add particle that is not in the particle package
            for particle in particle_level.unique():
                try:
                    particle_level = particle_level.replace({particle : Particle.from_pdgid(particle).name}) #skip particles not in the package
                except Exception:  # for some reason ValueError does not catch all errors
                    particle_level = particle_level.replace({particle : str(particle)}) # count how many times a dodgy entry occurs instead of just pass. Errors may go unscrutinised otherwise.
    
            daughters = truth_frame[f"{NBODY}_{nom_var}"]
            truth = truth_frame[f"{NBODY}_{truth_var}"]
            selected_particle_level = (daughters[truth == True]).xs(i, axis=0, level=1, drop_level=False)
    
            selected_particle_level = selected_particle_level.replace({20413.0:"D1(H)+",  43.0:"Xu0", 104124.0:"Lambda_c(2625)+", 44.0:"Xu+", 4432.0:"Omega_cc+", 4434.0:"Omega*_cc+"}) #manually add particle that is not in the particle package
            for particle in selected_particle_level.unique():
                try:
                    selected_particle_level = selected_particle_level.replace({particle : Particle.from_pdgid(particle).name}) #skip particles not in the package
                except Exception: 
                    selected_particle_level = selected_particle_level.replace({particle : str(particle)})
                    print(c("WARNING: dodgy selected PID").yellow.on_red)
                    continue # here we just flag this, but we don't expect anything
                
            fig, ax = plt.subplots(figsize=(30, 15))
            ax.bar(particle_level.value_counts(sort=False).index, particle_level.value_counts(sort=False).values, align="center", color="deeppink", alpha=0.7, label=f"Particle{i} in Sample")
            ax.bar(selected_particle_level.value_counts(sort=False).index, 
                selected_particle_level.value_counts(sort=False).values, 
                align="center", 
                facecolor=None, edgecolor="tab:blue", hatch="\\", fill=False, 
                label=f"Truth Matched Particle{i}"
            )
            ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
            plt.xticks(
                rotation=45,
                horizontalalignment='right',
                fontweight='light',
                fontsize='x-large'
            )
            plt.legend()
            
            # create mode-specific dir
            pathlib.Path(f"Plots/new_tuples/{NBODY}/{channel}/Truth_Matching/").mkdir(parents=True, exist_ok=True)
            plt.savefig(f"Plots/new_tuples/{NBODY}/{channel}/Truth_Matching/{NBODY}_check_truth_particle{i}.png")
            ax.set_yscale("log")
            plt.savefig(f"Plots/new_tuples/{NBODY}/{channel}/Truth_Matching/{NBODY}_check_truth_particle{i}_log.png")
            plt.close()
        
        return 
    

class Balanced_Sample:

    def check_amount_events(dataframe: pd.DataFrame
    ) -> Dict:

        """Check how many Signal Candidates/Combinations are in each sample

        Parameters
        ----------
        dataframe: pd.DataFrame
            Input dataframe containing all samples

        Returns
        -------
        Dict:
            Dictionary containing sample name and the amount of signal candidates
        """

        dictionary = {}
        for name in dataframe[dataframe.name != 'minbias'].name.unique():
            total_events = dataframe[dataframe.name == name].unique_event.nunique()
            signal_candidates = dataframe[(dataframe.name == name) & (dataframe.label == 1)]
            print(f"Number of events in sample {name}: {total_events} \nNumber of signal composites in sample {name}: {len(signal_candidates)}")
            print(f"Truth Efficiency: signal in {name}: {len(signal_candidates)} out of {len(dataframe[(dataframe.name == name)])} composites results in efficiency: {(len(signal_candidates))/(len(dataframe[(dataframe.name == name)]))} ")
            dictionary[name] = len(signal_candidates)

        return dictionary


    def determine_minimal_population(population_dict: Dict
    ) -> Tuple[Dict, Dict]:

        """Determine the sample with the least population as well as the cut off, when a sample if to little
        populated to be used for training

        Parameters
        ----------
        population_dict: Dict
            Dictionary containing signal candidate information

        Returns
        -------
        Dict:
            dictionary with list of samples to be removed from the training
        Dict:
            dictionary with parameters for the balanced sample: How many events from each sample for training/testing
        """

        #Some samples simply do not provide enough candidates. Figure out the least amount you need for training and add this here
        not_enough_population = {name:sample_size for (name,sample_size) in population_dict.items() if sample_size < 20000} #30000
        remaining_samples = {name:sample_size for (name,sample_size) in population_dict.items() if sample_size > 20000} #30000

        print(c(f"Not sufficiently populated samples: {not_enough_population}").red)

        smallest_sample = min(remaining_samples, key=remaining_samples.get)
        minimal_value = population_dict[smallest_sample]

        print(c(f"Minimal populated sample used for the base of the balanced sample is {smallest_sample} with {minimal_value} signal candidates").green)

        #-200 because after determining the training sample, one has to clean up the remaining sample before
        #assembling the testing sample because you do not want events from the training in the testing data
        #so you need to create a puffer
        training_sample_size = np.round((minimal_value * 0.8) - 200).astype(int)
        test_sample_size = np.round((minimal_value * 0.2)).astype(int)

        minbias_training_size = (training_sample_size * len(remaining_samples))
        minbias_testing_size = (test_sample_size * len(remaining_samples))

        assert(minbias_training_size >= training_sample_size)
        balanced_dict = {
            "training_sample_size": training_sample_size,
            "test_sample_size": test_sample_size,
            "minbias_training_size": minbias_training_size,
            "minbias_testing_size": minbias_testing_size
        }

        print(c(f"Parameters for the balanced sample {balanced_dict}, with {len(remaining_samples)} sample out of {len(population_dict)} samples eligable for training").magenta)
        return not_enough_population, balanced_dict


    def create_balanced_sample(dataframe: pd.DataFrame,
                               drop_list: Dict,
                               parameters: Dict,
                               _n_body: str,
                               pickled: bool = True,
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """Assemble the balanced samples

        Parameters
        ----------
        dataframe: pd.DataFrame
            Input Dataframe containing all samples
        drop_list: Dict
            Dictionary containing a list of samples that do not have sufficient population for the training
        parameters: Dict
            Dictionary with balanced sample parameters (training sample size, test sample size, background sizes etc.)
        pickled: bool
            Do you want to save the dataframes as pickle files? Default: yes
        Returns
        -------
        pd.DataFrame:
            Training DataFrame
        pd.DataFrame:
            Testing DataFrame
        """
        #drop samples from dataframe that do not provide enough signal candidates
        for id_, name in drop_list.items():
            print(f"{id_} dropped from dataframe")
            unwanted_samples = dataframe[dataframe.name == id_].index
            dataframe = dataframe[~dataframe.index.isin(unwanted_samples)]

        training_dataframe = []
        testing_dataframe = []

        #assemble train dataframe
        for name in dataframe.name.unique():
            if name != "minbias":
                signal = dataframe[(dataframe.name == name) & (dataframe.label == 1)][:parameters["training_sample_size"]]
                training_dataframe.append(signal)
                print(f"Mode {name} provided  {len(signal)} composites for the training")
            else:
                bkg = dataframe[(dataframe.name == "minbias")& (dataframe.label == 0)][:parameters["minbias_training_size"]]
                print(f"Mode {name} provided  {len(bkg)} composites for the training")
                training_dataframe.append(bkg)
        training_dataframe = pd.concat(training_dataframe).sample(frac=1)
        #commented out for now as I do not have a minbias sample to test on
        assert(len(training_dataframe[training_dataframe.label == 1]) == len(training_dataframe[training_dataframe.label == 0]))

        #cleanup remaining candidates before assembling the test dataframe
        events_in_training = training_dataframe.unique_event.unique()
        without_events_in_training = dataframe[~dataframe['unique_event'].isin(events_in_training)]

        #assemble test dataset
        for name in dataframe.name.unique():
            if name != "minbias":
                rest_signal = without_events_in_training[(without_events_in_training.name == name)
                                                         & (without_events_in_training.label == 1)][:parameters["test_sample_size"]]
                print(f"Mode {name} provided  {len(rest_signal)} composites for the testing")
                testing_dataframe.append(rest_signal)
            else:
                test_bkg = without_events_in_training[(without_events_in_training.name == "minbias")
                                                     & (without_events_in_training.label == 0)][:parameters["minbias_testing_size"]]
                testing_dataframe.append(test_bkg)
                print(f"Mode {name} provided  {len(test_bkg)} composites for the testing")
        testing_dataframe = pd.concat(testing_dataframe).sample(frac=1)

        #comment in when minbias is available
        assert(len(testing_dataframe[testing_dataframe.label == 1]) == len(testing_dataframe[testing_dataframe.label == 0]))

        if pickled:
            training_dataframe.to_pickle(f"scratch/{_n_body}_Train.pkl")
            testing_dataframe.to_pickle(f"scratch/{_n_body}_Test.pkl")

        print(c(f"Balanced samples assembled, data is now prepared and ready to go").green)
        return training_dataframe, testing_dataframe



if __name__ == '__main__':
    parser = ArgumentParser(description="Data Preparation Configuration")
    parser.add_argument('--branches',  default=f"{__mainpath__}/Utils/BOI.yml",
        help="branches of interest in the tuples")
    parser.add_argument('--modes',  default=f"{__mainpath__}/Utils/modes.yml",
        help="modes used for the training")
    parser.add_argument('--sanity', default=f"{__mainpath__}/Utils/sanity_BOI.yml",
        help="Branches of Interest for sanity checks of distributions")
    parser.add_argument('-b','--nbody',     required=True, choices=["TwoBody", "ThreeBody"],
        help="n-body implementation ['TwoBody', 'ThreeBody']") 
    parser.add_argument('-p','--pickled',     required=True, choices=["True", "False"],
        help="Load Data From Root -> Chose False; Load Data from pickle files -> Chose True") 
    
    opts = parser.parse_args()


    #Get the Branches of Interest from Config File
    branches = read_config(opts.branches, _nbody = opts.nbody)
    #Get the Modes from Config File
    modes = Load_Data.read_modes(opts.modes)

    #Uproot the data and put them into a dataframe; If you want to pickle them for faster load, add pickled=True in the arguments as well as the path
    #and the desired name of the pickle files
    if opts.pickled == "False":
        all_data = Load_Data.import_data(branches,modes, nbody=opts.nbody, pickled=True, pkl_name=f"{opts.nbody}_raw", path="/eos/lhcb/user/n/nschulte/public/forBlaise/Hlt2BTopo/raw_pickle_data/", new_tuples=True)

    if opts.pickled == "True":
        all_data = Load_Data.load_pickled_data(pkl_name=f"{opts.nbody}_raw_newtuples", path="/eos/lhcb/user/n/nschulte/public/forBlaise/Hlt2BTopo/raw_pickle_data/")
    #Calculate the True information/Perform Truth Matching
    all_data = Truth_Matching.truth_match(all_data, channels, opts.nbody)

    #Assign the Label and FromSignalB Features to the Dataframe
    all_data = Truth_Matching.assign_labels(all_data, opts.nbody)

    #make some truth information plots to make sure
    # for mode in modes.keys():
    #     if mode != 'minbias':
    #         print(mode)
    #         Truth_Matching.pid_plot(nom_var = "DAUGHTERS",
    #                           truth_var="FromSignalB", 
    #                           target_pids=np.array(channels[f"{mode}"]["truth"]), 
    #                           channel=mode,
    #                           NBODY=opts.nbody,
    #                           df = all_data)

    #create unique events for the untouched data
    all_data = Prepare_Data.create_unique_events(all_data)

    #filter beauty events from minbias sample
    all_data = Prepare_Data.clean_minbias(all_data, opts.nbody)

    #Get the amount of signal events from each sample
    sample_dict = Balanced_Sample.check_amount_events(all_data)

    #Get the List of Samples that need to be dropped due to less population as well as all the parameters for the balanced sample
    drop_list, parameters_balanced_sample = Balanced_Sample.determine_minimal_population(sample_dict)

    #Finally, Assemble Training und Testing Sample and Pickle them
    training_sample, testing_sample = Balanced_Sample.create_balanced_sample(all_data, drop_list, parameters_balanced_sample, opts.nbody, pickled = True)