#Load the Root Files and put them into a balanced sample

__author__        = "Blaise Delaney, Nicole Schulte"
__email__         = "blaise.delaney@cern.ch, nicole.schulte@cern.ch"
__modelpath__     = "root://eoslhcb.cern.ch//eos/lhcb/user/b/bldelane/public/Hlt2BTopo/Lip_M_NN"
__rootpath__      = "/eos/lhcb/user/b/bldelane/public/forNicole/Hlt2BTopo/scratch"
__oldtuplespath__ = "/ceph/users/nschulte/new_tuples_topo/Dec21TopoTuples"
__plotpath__      = "/ceph/users/nschulte/topo_neural_network/Data_prep/Plots"
#__mainpath__     = "/home/blaised/private/hlt2_topo/topo_neural_network" # comment out as needed
__mainpath__      = "/ceph/users/nschulte/topo_neural_network" # comment out as needed


from ast import Load
from locale import normalize
from operator import not_, truth
from typing import List, Union
import uproot3
from argparse import ArgumentParser
import pandas as pd
import numpy as np
from termcolor2 import c
import matplotlib.pyplot as plt
from numpy.typing import ArrayLike
import mplhep as hep
from plot_super import norm_errobar, norm_hist, plot_data, plot_hist_err


import warnings
warnings.simplefilter(action='ignore', category=FutureWarning) #ignore pandas warning about frame.append -> pandas.concat for now

import sys
sys.path.insert(1,__mainpath__)
from pipeline.utils import read_config
from prepare_sample import Load_Data, Truth_Matching
from Utils.channel_config import channels
from hist import Hist
import hist
#from pipeline.mpl_config import *
from Utils.sanity_BOI_plot_config import TwoBody_sanity_config, ThreeBody_sanity_config
import pathlib


class Sanity_Checks:

    def show_single_distributions(
        dataframe: pd.DataFrame,
        branches:List[str],
        mode: str,
        nbody: str,
        new_tuples:bool
    ):
        dataframe_per_mode = dataframe[dataframe.name == mode]

        for branch in branches:
            if nbody == "TwoBody":
                range = TwoBody_sanity_config[f"{branch}"]["EDGES"]
                bins = TwoBody_sanity_config[f"{branch}"]["bins"]
                xlabel = TwoBody_sanity_config[f"{branch}"]["xlabel"]
            if nbody == "ThreeBody":
                range = ThreeBody_sanity_config[f"{branch}"]["EDGES"]
                bins = ThreeBody_sanity_config[f"{branch}"]["bins"]
                xlabel = ThreeBody_sanity_config[f"{branch}"]["xlabel"]
            fig, ax = plt.subplots()


            ax.hist(dataframe_per_mode[branch], range=range, bins=bins)

            ax.set_xlabel(xlabel)
            ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)

            print(c(f'Plotting {branch} for {mode} {nbody} right now').magenta)
            if new_tuples:
                pathlib.Path(f"{__plotpath__}/new_tuples/{nbody}/{mode}/Branch_Distribution").mkdir(parents=True, exist_ok=True)
                plt.savefig(f"{__plotpath__}/new_tuples/{nbody}/{mode}/Branch_Distribution/{mode}_{branch}.png")
                plt.savefig(f"{__plotpath__}/new_tuples/{nbody}/{mode}/Branch_Distribution/{mode}_{branch}.pdf")
            else:
                pathlib.Path(f"{__plotpath__}/old_tuples/{nbody}/{mode}/Branch_Distribution").mkdir(parents=True, exist_ok=True)
                plt.savefig(f"{__plotpath__}/old_tuples/{nbody}/{mode}/Branch_Distribution/{mode}_{branch}.png")
                plt.savefig(f"{__plotpath__}/old_tuples/{nbody}/{mode}/Branch_Distribution/{mode}_{branch}.pdf")               
            plt.close()
        
        return
        
    def bothbody_compare(
            twobody_dataframe: pd.DataFrame,
            threebody_dataframe: pd.DataFrame,
            mode: str,
            new_tuples:bool=True
        ):
            twobody_dataframe_per_mode = twobody_dataframe[(twobody_dataframe.name == mode) & (twobody_dataframe.label == 1)]
            threebody_dataframe_per_mode = threebody_dataframe[(threebody_dataframe.name == mode) & (threebody_dataframe.label == 1)]



            for branch in ("M", "MM", "P", "PT", "ENDVERTEX_DOCAMAX", "IPCHI2_OWNPV", "FDCHI2_OWNPV", "OWNPV_CHI2", "ENDVERTEX_CHI2"):
                print(c(f'Plotting {branch} for {mode} bothbodies right now').magenta)

                fig, axs = plt.subplots(2)

                norm_errobar(
                    twobody_dataframe_per_mode[f"TwoBody_{branch}"],
                    bins=TwoBody_sanity_config[f"TwoBody_{branch}"]["bins"],
                    range=TwoBody_sanity_config[f"TwoBody_{branch}"]["EDGES"],
                    color="deeppink",
                    label=(f"TwoBody_{branch}"),
                    histcolor="grey",
                    _ax=axs[0],
                    _isnorm=True
                )


                norm_errobar(
                    threebody_dataframe_per_mode[f"ThreeBody_{branch}"],
                    bins=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["bins"],
                    range=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["EDGES"],
                    color="dodgerblue",
                    histcolor="grey",
                    _ax=axs[1],
                    label=(f"ThreeBody_{branch}"),
                    _isnorm=True
                )
                
                axs[0].set_title(f"{mode}",fontsize=15)
                axs[1].set_xlabel(ThreeBody_sanity_config[f"ThreeBody_{branch}"]["xlabel"])
                axs[0].set_ylabel("Arbitrary Units")
                axs[1].set_ylabel("Arbitrary Units")
                axs[0].legend(fontsize=6)
                axs[1].legend(fontsize=6)
                axs[0].text(s="LHCb Unofficial", x=0.0, y=1.01, transform=axs[0].transAxes, fontsize=10)                
    
                if new_tuples:
                    pathlib.Path(f"{__plotpath__}/new_tuples/BothBodies/{mode}/Branch_Distribution").mkdir(parents=True, exist_ok=True)
                    plt.savefig(f"{__plotpath__}/new_tuples/BothBodies/{mode}/Branch_Distribution/{mode}_{branch}.png")
                    plt.savefig(f"{__plotpath__}/new_tuples/BothBodies/{mode}/Branch_Distribution/{mode}_{branch}.pdf")
                else:
                    pathlib.Path(f"{__plotpath__}/old_tuples/BothBodies/{mode}/Branch_Distribution").mkdir(parents=True, exist_ok=True)
                    plt.savefig(f"{__plotpath__}/old_tuples/BothBodies/{mode}/Branch_Distribution/{mode}_{branch}.png")
                    plt.savefig(f"{__plotpath__}/old_tuples/BothBodies/{mode}/Branch_Distribution/{mode}_{branch}.pdf")               
                
                plt.close()

            return


    def twobody_compare(
            new_mc_twobody_df: pd.DataFrame,
            old_mc_twobody_df: pd.DataFrame,
            mode: str,
        ):
            twobody_new_mc = new_mc_twobody_df[(new_mc_twobody_df.name == mode) & (new_mc_twobody_df.label == 1)]
            twobody_old_mc = old_mc_twobody_df[(old_mc_twobody_df.name == mode) & (old_mc_twobody_df.TwoBody_fromSignalB == True)]

            for branch in ("M", "MM", "P", "PT", "ENDVERTEX_DOCAMAX", "IPCHI2_OWNPV", "FDCHI2_OWNPV", "OWNPV_CHI2", "ENDVERTEX_CHI2"):
                print(c(f'Plotting {branch} for {mode} TwoBody right now').magenta)

                fig, axs = plt.subplots(2)

                norm_errobar(
                    twobody_new_mc[f"TwoBody_{branch}"],
                    bins=TwoBody_sanity_config[f"TwoBody_{branch}"]["bins"],
                    range=TwoBody_sanity_config[f"TwoBody_{branch}"]["EDGES"],
                    color="deeppink",
                    label=(f"TwoBody_{branch} HLT1 filtered"),
                    _ax=axs[0],
                    histcolor="grey",
                    _isnorm=True
                )


                norm_errobar(
                    twobody_old_mc[f"TwoBody_{branch}"],
                    bins=TwoBody_sanity_config[f"TwoBody_{branch}"]["bins"],
                    range=TwoBody_sanity_config[f"TwoBody_{branch}"]["EDGES"],
                    color="dodgerblue",
                    _ax=axs[1],
                    histcolor="grey",
                    label=(f"TwoBody_{branch} HLT1 emulated"),
                    _isnorm=True
                )
                
                axs[0].set_title(f"{mode}",fontsize=15)
                axs[1].set_xlabel(TwoBody_sanity_config[f"TwoBody_{branch}"]["xlabel"])
                axs[0].set_ylabel("Arbitrary Units")
                axs[1].set_ylabel("Arbitrary Units")
                axs[0].legend(fontsize=6)
                axs[1].legend(fontsize=6)
                axs[0].text(s="LHCb Unofficial", x=0.0, y=1.01, transform=axs[0].transAxes, fontsize=10)                

                pathlib.Path(f"{__plotpath__}/HLT1_Effect/{mode}/TwoBody_Branch_Distribution").mkdir(parents=True, exist_ok=True)
                plt.savefig(f"{__plotpath__}/HLT1_Effect/{mode}/TwoBody_Branch_Distribution/{mode}_{branch}.png")
                plt.savefig(f"{__plotpath__}/HLT1_Effect/{mode}/TwoBody_Branch_Distribution/{mode}_{branch}.pdf")
                plt.close()

            return

    def threebody_compare(
            new_mc_threebody_df: pd.DataFrame,
            old_mc_threebody_df: pd.DataFrame,
            mode: str,
        ):
            threebody_new_mc = new_mc_threebody_df[(new_mc_threebody_df.name == mode) & (new_mc_threebody_df.label == 1)]
            threebody_old_mc = old_mc_threebody_df[(old_mc_threebody_df.name == mode) & (old_mc_threebody_df.ThreeBody_fromSignalB == True)]

            for branch in ("M", "MM", "P", "PT", "ENDVERTEX_DOCAMAX", "IPCHI2_OWNPV", "FDCHI2_OWNPV", "OWNPV_CHI2", "ENDVERTEX_CHI2"):
                print(c(f'Plotting {branch} for {mode} ThreeBody right now').magenta)

                fig, axs = plt.subplots(2)

                norm_errobar(
                    threebody_new_mc[f"ThreeBody_{branch}"],
                    bins=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["bins"],
                    range=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["EDGES"],
                    color="deeppink",
                    label=(f"ThreeBody_{branch} HLT1 filtered"),
                    histcolor="grey",
                    _ax=axs[0],
                    _isnorm=True
                )


                norm_errobar(
                    threebody_old_mc[f"ThreeBody_{branch}"],
                    bins=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["bins"],
                    range=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["EDGES"],
                    color="dodgerblue",
                    histcolor="grey",
                    _ax=axs[1],
                    label=(f"ThreeBody_{branch} HLT1 emulated"),
                    _isnorm=True
                )
                
                axs[0].set_title(f"{mode}",fontsize=15)
                axs[1].set_xlabel(ThreeBody_sanity_config[f"ThreeBody_{branch}"]["xlabel"])
                axs[0].set_ylabel("Arbitrary Units")
                axs[1].set_ylabel("Arbitrary Units")
                axs[0].legend(fontsize=6)
                axs[1].legend(fontsize=6)
                axs[0].text(s="LHCb Unofficial", x=0.0, y=1.01, transform=axs[0].transAxes, fontsize=10)                

                pathlib.Path(f"{__plotpath__}/HLT1_Effect/{mode}/ThreeBody_Branch_Distribution").mkdir(parents=True, exist_ok=True)
                plt.savefig(f"{__plotpath__}/HLT1_Effect/{mode}/ThreeBody_Branch_Distribution/{mode}_{branch}.png")
                plt.savefig(f"{__plotpath__}/HLT1_Effect/{mode}/ThreeBody_Branch_Distribution/{mode}_{branch}.pdf")
                plt.close()
          
            return

    def bothbody_bothmcs(
            new_mc_threebody_df: pd.DataFrame,
            old_mc_threebody_df: pd.DataFrame,
            new_mc_twobody_df: pd.DataFrame,
            old_mc_twobody_df: pd.DataFrame,            
            mode: str,
        ):
            threebody_new_mc = new_mc_threebody_df[(new_mc_threebody_df.name == mode) & (new_mc_threebody_df.label == 1)]
            threebody_old_mc = old_mc_threebody_df[(old_mc_threebody_df.name == mode) & (old_mc_threebody_df.ThreeBody_fromSignalB == True)]

            twobody_new_mc = new_mc_twobody_df[(new_mc_twobody_df.name == mode) & (new_mc_twobody_df.label == 1)]
            twobody_old_mc = old_mc_twobody_df[(old_mc_twobody_df.name == mode) & (old_mc_twobody_df.TwoBody_fromSignalB == True)]

            for branch in ("M", "MM", "P", "PT", "ENDVERTEX_DOCAMAX", "IPCHI2_OWNPV", "FDCHI2_OWNPV", "OWNPV_CHI2", "ENDVERTEX_CHI2"):
                print(c(f'Plotting {branch} for {mode} both mcs both bodies right now').magenta)

                fig, axs = plt.subplots(2)
                norm_errobar(
                    threebody_new_mc[f"ThreeBody_{branch}"],
                    bins=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["bins"],
                    range=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["EDGES"],
                    color="deeppink",
                    label=(f"ThreeBody_{branch} HLT1 filtered"),
                    histcolor="deeppink",
                    _ax=axs[0],
                    _isnorm=True
                )

                norm_errobar(
                    threebody_old_mc[f"ThreeBody_{branch}"],
                    bins=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["bins"],
                    range=ThreeBody_sanity_config[f"ThreeBody_{branch}"]["EDGES"],
                    color="dodgerblue",
                    histcolor="dodgerblue",
                    _ax=axs[0],
                    label=(f"ThreeBody_{branch} HLT1 emulated"),
                    _isnorm=True
                )

                norm_errobar(
                    twobody_new_mc[f"TwoBody_{branch}"],
                    bins=TwoBody_sanity_config[f"TwoBody_{branch}"]["bins"],
                    range=TwoBody_sanity_config[f"TwoBody_{branch}"]["EDGES"],
                    color="deeppink",
                    label=(f"TwoBody_{branch} HLT1 filtered"),
                    _ax=axs[1],
                    histcolor="deeppink",
                    _isnorm=True
                )

                norm_errobar(
                    twobody_old_mc[f"TwoBody_{branch}"],
                    bins=TwoBody_sanity_config[f"TwoBody_{branch}"]["bins"],
                    range=TwoBody_sanity_config[f"TwoBody_{branch}"]["EDGES"],
                    color="dodgerblue",
                    _ax=axs[1],
                    histcolor="dodgerblue",
                    label=(f"TwoBody_{branch} HLT1 emulated"),
                    _isnorm=True
                )

                axs[0].set_title(f"{mode}",fontsize=15)
                axs[1].set_xlabel(ThreeBody_sanity_config[f"ThreeBody_{branch}"]["xlabel"])
                axs[0].set_ylabel("Arbitrary Units")
                axs[1].set_ylabel("Arbitrary Units")
                axs[0].legend(fontsize=6)
                axs[1].legend(fontsize=6)
                axs[0].text(s="LHCb Unofficial", x=0.0, y=1.01, transform=axs[0].transAxes, fontsize=10)                

                pathlib.Path(f"{__plotpath__}/HLT1_Effect/{mode}/BothBody_Branch_Distribution").mkdir(parents=True, exist_ok=True)
                plt.savefig(f"{__plotpath__}/HLT1_Effect/{mode}/BothBody_Branch_Distribution/{mode}_{branch}.png")
                plt.savefig(f"{__plotpath__}/HLT1_Effect/{mode}/BothBody_Branch_Distribution/{mode}_{branch}.pdf")
                plt.close()
          
            return

    def bothbody_bothmcs_tracks(
            new_mc_threebody_df: pd.DataFrame,
            old_mc_threebody_df: pd.DataFrame,
            new_mc_twobody_df: pd.DataFrame,
            old_mc_twobody_df: pd.DataFrame,            
            mode: str,
        ):
            threebody_new_mc = new_mc_threebody_df[(new_mc_threebody_df.name == mode) & (new_mc_threebody_df.label == 1)]
            threebody_old_mc = old_mc_threebody_df[(old_mc_threebody_df.name == mode) & (old_mc_threebody_df.ThreeBody_fromSignalB == True)]

            twobody_new_mc = new_mc_twobody_df[(new_mc_twobody_df.name == mode) & (new_mc_twobody_df.label == 1)]
            twobody_old_mc = old_mc_twobody_df[(old_mc_twobody_df.name == mode) & (old_mc_twobody_df.TwoBody_fromSignalB == True)]

            for branch in ("Track1_P", "Track1_PT", "Track1_ORIVX_DOCAMAX", "Track1_IPCHI2_OWNPV"):
                print(c(f'Plotting {branch} for {mode} both mcs both bodies Track Dists right now').magenta)

                fig, axs = plt.subplots(2)
                norm_errobar(
                    threebody_new_mc[f"{branch}"],
                    bins=ThreeBody_sanity_config[f"{branch}"]["bins"],
                    range=ThreeBody_sanity_config[f"{branch}"]["EDGES"],
                    color="deeppink",
                    label=(f"ThreeBody_{branch} HLT1 filtered"),
                    histcolor="deeppink",
                    _ax=axs[0],
                    _isnorm=True
                )

                norm_errobar(
                    threebody_old_mc[f"{branch}"],
                    bins=ThreeBody_sanity_config[f"{branch}"]["bins"],
                    range=ThreeBody_sanity_config[f"{branch}"]["EDGES"],
                    color="dodgerblue",
                    histcolor="dodgerblue",
                    _ax=axs[0],
                    label=(f"ThreeBody_{branch} HLT1 emulated"),
                    _isnorm=True
                )

                norm_errobar(
                    twobody_new_mc[f"{branch}"],
                    bins=TwoBody_sanity_config[f"{branch}"]["bins"],
                    range=TwoBody_sanity_config[f"{branch}"]["EDGES"],
                    color="deeppink",
                    label=(f"TwoBody_{branch} HLT1 filtered"),
                    _ax=axs[1],
                    histcolor="deeppink",
                    _isnorm=True
                )

                norm_errobar(
                    twobody_old_mc[f"{branch}"],
                    bins=TwoBody_sanity_config[f"{branch}"]["bins"],
                    range=TwoBody_sanity_config[f"{branch}"]["EDGES"],
                    color="dodgerblue",
                    _ax=axs[1],
                    histcolor="dodgerblue",
                    label=(f"TwoBody_{branch} HLT1 emulated"),
                    _isnorm=True
                )

                axs[0].set_title(f"{mode}",fontsize=15)
                axs[1].set_xlabel(ThreeBody_sanity_config[f"{branch}"]["xlabel"])
                axs[0].set_ylabel("Arbitrary Units")
                axs[1].set_ylabel("Arbitrary Units")
                axs[0].legend(fontsize=6)
                axs[1].legend(fontsize=6)
                axs[0].text(s="LHCb Unofficial", x=0.0, y=1.01, transform=axs[0].transAxes, fontsize=10)                

                pathlib.Path(f"{__plotpath__}/HLT1_Effect/{mode}/BothBody_Track_Distribution").mkdir(parents=True, exist_ok=True)
                plt.savefig(f"{__plotpath__}/HLT1_Effect/{mode}/BothBody_Track_Distribution/{mode}_{branch}.png")
                plt.savefig(f"{__plotpath__}/HLT1_Effect/{mode}/BothBody_Track_Distribution/{mode}_{branch}.pdf")
                plt.close()
          
            return


    def transform_log_variables(
        dataframe: pd.DataFrame,
        nbody: str
    ) -> pd.DataFrame:

        for branch in ("Track1_IPCHI2_OWNPV", f"{nbody}_IPCHI2_OWNPV", f"{nbody}_FDCHI2_OWNPV", f"{nbody}_OWNPV_CHI2", f"{nbody}_ENDVERTEX_CHI2"):
            print(c(f"Logging variable {branch} right now").green)

            dataframe.loc[dataframe[branch] <= 0, branch] = 1e-5
            assert(dataframe[branch].all()>0)
            dataframe[branch] = np.log(dataframe[branch].apply(pd.to_numeric))             
        
        return dataframe


if __name__ == '__main__':
    parser = ArgumentParser(description="Configuration for Distribution Plots")
    parser.add_argument('--modes',  default=f"{__mainpath__}/Utils/plotting_modes.yml",
        help="modes used for the training")
    parser.add_argument('--sanity', default=f"{__mainpath__}/Utils/sanity_BOI.yml",
        help="Branches of Interest for sanity checks of distributions")
    parser.add_argument('-b','--nbody',     required=False, choices=["TwoBody", "ThreeBody"],
        help="n-body implementation ['TwoBody', 'ThreeBody']") #change this to required True later; annoying for developing rn
    opts = parser.parse_args()

    #Get the Modes from Config File
    modes = Load_Data.read_modes(opts.modes)

    #Sanity Checks! Define the branches you would like to look at 
    #sanity_branches = read_config(opts.sanity, _nbody = opts.nbody)
    sanity_branches_twobody = read_config(opts.sanity, _nbody = "TwoBody")
    sanity_branches_threebody = read_config(opts.sanity, _nbody = "ThreeBody")
    old_twobody_branches = read_config(opts.sanity, _nbody = "Former_TwoBody")
    old_threebody_branches = read_config(opts.sanity, _nbody = "Former_ThreeBody")

    #Uproot the data and put them into a dataframe; If you want to pickle them for faster load, add pickled=True in the arguments as well as the path
    #and the desired name of the pickle files
    #First import the new tuples with the hlt1 filtering
    #new_data = Load_Data.import_data(sanity_branches,modes, nbody=opts.nbody, pickled=False, pkl_name=None, path=None, new_tuples=True)
    #for mode, event_type in modes.items():
        #Sanity_Checks.show_single_distributions(dataframe=new_data, branches=sanity_branches, mode=mode, nbody=opts.nbody, new_tuples=True)


    #Then import the old tuples with hlt1 emulating cuts 
    #old_data = Load_Data.import_data(sanity_branches, modes, nbody=opts.nbody, pickled=False, pkl_name=None, path=None, new_tuples=False)
    #And plot their distribution
    #for mode, event_type in modes.items():
        #Sanity_Checks.show_single_distributions(dataframe=old_data, branches=sanity_branches, mode=mode, nbody=opts.nbody, new_tuples=False)
    
    twobody_data_new = Load_Data.import_data(sanity_branches_twobody,modes, nbody="TwoBody", pickled=False, pkl_name=f"{opts.nbody}_raw", path="scratch/", new_tuples=True)
    twobody_data_new = Truth_Matching.truth_match(twobody_data_new, channels, _n_body="TwoBody")
    twobody_data_new = Truth_Matching.assign_labels(twobody_data_new, _n_body="TwoBody")
    twobody_data_new = Sanity_Checks.transform_log_variables(twobody_data_new, nbody="TwoBody")

    threebody_data_new = Load_Data.import_data(sanity_branches_threebody,modes, nbody="ThreeBody", pickled=False, pkl_name=None, path=None, new_tuples=True)
    threebody_data_new = Truth_Matching.truth_match(threebody_data_new, channels, _n_body="ThreeBody")
    threebody_data_new = Truth_Matching.assign_labels(threebody_data_new, _n_body="ThreeBody")
    threebody_data_new = Sanity_Checks.transform_log_variables(threebody_data_new, nbody="ThreeBody")


    twobody_data_old = Load_Data.import_data(old_twobody_branches, modes, nbody="TwoBody", pickled=False, pkl_name=None, path=None, new_tuples=False)
    twobody_data_old = Sanity_Checks.transform_log_variables(twobody_data_old, nbody="TwoBody")

    threebody_data_old = Load_Data.import_data(old_threebody_branches, modes, nbody="ThreeBody", pickled=False, pkl_name=None, path=None, new_tuples=False)
    threebody_data_old = Sanity_Checks.transform_log_variables(threebody_data_old, nbody="ThreeBody")


    for mode, event_type in modes.items():
        #Sanity_Checks.show_single_distributions(dataframe=new_data, branches=sanity_branches, mode=mode, nbody=opts.nbody, new_tuples=True)
        Sanity_Checks.bothbody_compare(twobody_dataframe=twobody_data_new, threebody_dataframe=threebody_data_new, mode=mode)
        Sanity_Checks.twobody_compare(new_mc_twobody_df=twobody_data_new, old_mc_twobody_df=twobody_data_old, mode=mode)
        Sanity_Checks.threebody_compare(new_mc_threebody_df=threebody_data_new, old_mc_threebody_df=threebody_data_old, mode=mode)
        Sanity_Checks.bothbody_bothmcs(new_mc_threebody_df=threebody_data_new,
                                      old_mc_threebody_df=threebody_data_old,
                                      new_mc_twobody_df=twobody_data_new, 
                                      old_mc_twobody_df=twobody_data_old, 
                                      mode=mode)
        Sanity_Checks.bothbody_bothmcs_tracks(new_mc_threebody_df=threebody_data_new,
                                       old_mc_threebody_df=threebody_data_old,
                                       new_mc_twobody_df=twobody_data_new, 
                                       old_mc_twobody_df=twobody_data_old, 
                                       mode=mode)
