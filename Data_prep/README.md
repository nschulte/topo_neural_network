This file contains everything needed in order to create a balanced sample. 

Additional features that are not in the branches directly are not calculated here as this is done in the pipeline. If the root files are called according to the sample names, no issues should arise. This script is tested on old root files where it runs perfectly. 

Uprooting takes a while, especially with the minbias.

Launch this script simply with: 

```
python prepare_samples.py -b TwoBody -p False
``` 
The -p Flag corresponds to whether you have already pickled the raw data or not. If you have run the script once and pickled the data, you can simply say -p True. If you run this for the first time and would like to pickle the data for a faster load later, please do -p False.

Details on the functions and the single steps can be found in the file.

To check the difference between the old samples (HLT1 Emulation) and new samples (Actual HLT1), you need to specify the desired branches in Utils/sanity_BOI.yml and the 
plot config in Utils/sanity_BOI_plot_config.py.

Then your script will create the plots for the old samples and the new samples per mode per branch. To get the ThreeBody, please add -b ThreeBody

To launch write

```
python distribution_checks.py
````

Still needed: 
Histograms for Truth Matching 
