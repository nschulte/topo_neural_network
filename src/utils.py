"""I/O and plotting utils"""

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]

from builtins import breakpoint
from tqdm import tqdm
from typing import List, Dict, Union, List, Any
from pathlib import Path
import yaml
import time
import uproot
import pandas as pd


def timing(func):
    """decorator to print function execution time [ms]"""

    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = func(*args, **kwargs)
        time2 = time.time()
        print(
            "{:s} function took {:.3f} ms".format(
                func.__name__, (time2 - time1) * 1000.0
            )
        )

        return ret

    return wrap


def check_argpath(func):
    """decorator to check input path exists"""

    def wrapper(feats_path, _key, **kwargs):
        try:
            path = Path(feats_path)
        except IOError:
            print("Incorrect input path")
        features = func(feats_path, _key)
        return features

    return wrapper


@check_argpath
def read_feats(
    feats_path: str,
    _key: str,
) -> Union[list, dict]:
    """read in the feature yaml file after checking it exists"""
    with open(feats_path, "r") as stream:
        in_dict = yaml.load(stream, Loader=yaml.FullLoader)
    try:
        feature_list = in_dict[_key]
    except ValueError:
        print(f"'{_key}' key not in dict")

    return feature_list


def yml_to_dict(
    _path: "yml file",
) -> dict:
    """Read in a yaml file into a dict"""
    with open(_path, "r") as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)


def load_root(
    file: str,
    library: str,
    branches: Union[List[str], None] = None,
    cut: Union[str, None] = None,
    name: Union[str, None] = None,
    max_entries: Union[int, None] = None,
    batch_size: Union[str, None] = "200 MB",
    **kwargs,
) -> pd.DataFrame:
    """wrapper for uproot.iterate() to load ROOT files into a pandas DataFrame"""

    events = uproot.open(f"{file}")

    # if pandas, batch and concatenate
    if library == "pd":
        bevs = events.num_entries_for(batch_size, branches, entry_stop=max_entries)
        tevs = events.num_entries
        nits = round(tevs / bevs + 0.5)
        aggr = []
        for batch in tqdm(
            events.iterate(
                expressions=branches,
                cut=cut,
                library=library,
                entry_stop=max_entries,
                step_size=batch_size,
                **kwargs,
            ),
            total=nits,
            ascii=True,
            desc=f"batches loaded",
        ):
            aggr.append(batch)

        df = pd.concat(aggr)

    # else, load into awkward or numpy objects
    else:
        df = events.arrays(
            expressions=branches,
            cut=cut,
            library=library,
            entry_stop=max_entries,
            **kwargs,
        )

    # assign TeX label to df for plotting
    df.name = name

    print("\nSUCCESS: ", df.name, f" loaded with {len(df)} entries")
    return df


@timing
def load_ntuple(
    file_path: str,
    nbody_key: str,  # ['TwoBody', 'ThreeBody']
    tree_name: str = "DecayTree",
    branches: Union[List[str], None] = None,
    library: str = "ak",  # default to awkward
    cut: Union[str, None] = None,
    name: Union[str, None] = None,
    max_entries: Union[int, None] = None,
    batch_size: Union[str, None] = "50 KB",
    **kwargs,
) -> Any:
    """load file using pkl or uproot, depending on file extension"""
    ext = Path(file_path).suffix
    if ext == ".pkl":
        df = pd.read_pickle(file_path)
    elif ext == ".root":
        full_path = f"{file_path}:{nbody_key}/{tree_name}"
        df = load_root(
            file=full_path,
            library=library,
            branches=branches,
            cut=cut,
            name=name,
            max_entries=max_entries,
            batch_size=batch_size,
            **kwargs,
        )
    else:
        raise ValueError("File extension not recognised")

    return df
