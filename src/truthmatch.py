"""Container of all functionalities related to truth matching"""

import pandas as pd
from typing import Any, Dict, Union, Tuple
from numpy.typing import ArrayLike
import awkward as ak
import hist
from .utils import yml_to_dict
from config.channels import channels
import numpy as np

__authors__ = ["Blaise Delaney", "Nicole Schulte"]
__emails__ = ["blaise.delaney at cern.ch", "nicole.schulte at cern.ch"]


class TruthMatcherObject:
    """Class to match a FS particle to the correct PDG ID"""

    def __init__(
        self,
        sample: pd.DataFrame,
        truth_config_id: str,
    ) -> None:
        self._sample: pd.DataFrame = sample
        self._truth_config_id: str = truth_config_id
        self.truth_sel = channels[truth_config_id]["truth"]
        self.label = channels[truth_config_id]["label"]

    def __str__(self):
        return f"""
        TruthMatcher object:
            Truth Config ID: {self._truth_config_id}
            Truth Selection: {self.truth_sel}
            Label: {self.label}
        """

    def __repr__(self):
        return f"TruthMatcher({self._truth_config_id}, {self.label})"

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        pass

    @property
    def sample(self) -> pd.DataFrame:
        return self._sample

    @property
    def truth_config_id(self) -> str:
        return self._truth_config_id

    @staticmethod
    def assign_name(dataframe: pd.DataFrame, label: str) -> pd.DataFrame:
        """Add TeX label to df for plotting

        Paramaters
        ----------
        dataframe : pd.DataFrame
            Dataframe to add label to

        label : str
            TeX label assigned to dataframe.name, used for plottig

        Returns
        -------
        pd.DataFrame
            Dataframe with `name` attribute set to `label`
        """
        dataframe.name = rf"{{{label}}}"

        return dataframe

    @staticmethod
    def apply_evt_mask(
        dataset: Union[pd.DataFrame, ArrayLike], branch: str, evt_mask: ArrayLike
    ) -> Union[pd.DataFrame, ArrayLike]:
        """Apply selection mask to the {Two,Three}Body_DAUGHTERS array

        Parameters
        ----------
        dataframe : Union[pd.DataFrame, ArrayLike]
            Dataframe to event-mask

        branch : str
            Branch to apply event mask to

        evt_mask : ArrayLike
            Array of PIDs to required to match elementwise

        Returns
        -------
        pd.DataFrame
            Dataframe with event mask applied
        """
        pre_sel_len = len(dataset)
        sel_df = dataset[dataset[branch] == evt_mask]
        assert len(sel_df) >= pre_sel_len, "Event mask not applied correctly"

        return sel_df

    def truthmatch(
        self,
        nbody_key: str,
        exclusive: bool = True,
    ) -> ak.Array:
        """Implement truthmatching inclusively or esclusively

        Parameters
        ----------
        nbody_key : str
            Key to the n-body tree to truth match [`TwoBody` or `ThreeBody`]

        truth_key : str
            Key identifier in the config truth matching dictionary

        exclusive: bool
            Truthmatching is done inclusively or exclusively
            - If True: remove all non-excluisive-signal resonances and enforce nominal-final state multiplicity
            - If False: take a conservative approach and filter out events with number final-state tracks below the nominal

        Returns
        -------
        Any
            Event conrtainer with truth-matched signal
        """

        # take in the dataset. If pandas DataFrame, convert to akward array object
        dataset = self.sample
        if isinstance(self.sample, pd.DataFrame):
            dataset = ak.from_pandas(self.sample)

        # check it is an akward object
        assert isinstance(
            dataset, ak.Array
        ), "TypeError: dataset must be an akward array object"

        # filter for nonzero children
        nonzero_children = ak.count(dataset[f"{nbody_key}_DAUGHTERS"], axis=1) > 0
        df = dataset[nonzero_children]

        # book hist container
        h = (
            hist.Hist.new.Reg(100, 0, 100, name="#PDG IDs")
            .Double()
            .fill(
                ak.count(df[f"{nbody_key}_DAUGHTERS"], axis=1),
            )
        )

        # impose the candidate multiplicity of the most common occurrence
        min_ncands_sel = ak.count(df[f"{nbody_key}_DAUGHTERS"], axis=1) == ak.argmax(
            h.view()
        )
        df = df[min_ncands_sel]

        if exclusive is True:
            # firstly, ensure the exact length-matching against truth
            df = df[
                ak.count(df[f"{nbody_key}_DAUGHTERS"], axis=1) == len(self.truth_sel)
            ]

            # HACK: in order to avoid looping through topo candidates, we don't check elementwise, but
            # - mask by verifying that the sum of the PIDs is the same as truth
            # - iterate along the pid slot, masking away mismatches
            mask = ak.sum(df[f"{nbody_key}_DAUGHTERS"], axis=1) == ak.sum(
                np.abs(self.truth_sel)
            )
            # iterative truthmatching, masking per-track-slot, as opposed to per-candidate
            for i in range(
                len(self.truth_sel)
            ):  # NOTE: the only loop here is over the track slots (O(10)), as opposed to per-candidate (>~O(10^4))
                df = df[df[f"{nbody_key}_DAUGHTERS"][..., i] == abs(self.truth_sel[i])]

        # check it is an akward object
        assert isinstance(
            df, ak.Array
        ), "TypeError: truthmatched must be an akward array object"

        print(f"Truthmatched {len(df)} {nbody_key} events")
        return df

    def truthmatch_allbody(self, exclusive: bool) -> Tuple[ak.Array, ak.Array]:
        """Truthmatch both the TwoBody and ThreeBody trees

        Parameters
        ----------
        exclusive: bool
            Truthmatching is done inclusively or exclusively

        Returns
        -------
        Tuple[ak.Array, ak.Array]
            Truthmatched TwoBody and ThreeBody trees
        """
        twobody_tm = self.truthmatch(
            nbody_key="TwoBody",
            exclusive=exclusive,
        )
        threebody_tm = self.truthmatch(
            nbody_key="ThreeBody",
            exclusive=exclusive,
        )

        return twobody_tm, threebody_tm

    def assign_labels(dataframe: pd.DataFrame, _n_body: str) -> pd.DataFrame:
        """Add FromSameB information to the dataframe as well as a label which is the same, but not as bool but as integer
        Parameters
        ----------
        dataframe: pd.DataFrame
            Input dataframe containing all samples
        matched_dataframe: pd.DataFrame
            Truth DataFrame form truth_match
        _n_body: str
            TwoBody or ThreeBody
        Returns
        -------
        pd.Dataframe:
            Returns the Input Dataframe with added information on the SignalB and label

        TO DO: this could be more refined. There is a lot of memory allocation here, so be careful;
        """

        dataframe["label"] = dataframe[f"{_n_body}_FromSignalB"].astype(int)

        for name in dataframe.name.unique():
            print(
                f" sample {name} has {len(dataframe[(dataframe.name == name) & (dataframe.label == 1)])} signal candidates"
            )
            if name == "minbias":
                # make sure there is only one unique label in minbias. Which should be 0
                assert dataframe[dataframe.name == "minbias"].label.nunique() == 1
        return dataframe

    @staticmethod
    def write(dataset: Union[ak.Array, ArrayLike, pd.DataFrame]) -> None:
        """Write the truth-matched dataset to a file

        Parameters
        ----------
        dataset : Union[ak.Array, ArrayLike, pd.DataFrame]
            Truth-matched dataset to write to file
        """
        pass

    def pid_plot(
        nom_var: "var in tuple",
        truth_var: "truth_variable",
        df: "control dataframe",
        NBODY: "b-hadron branch name",
        target_pids: "set the dimension",
        channel: "string",
        **kwargs,
    ) -> "plot":

        frame = df[df.name == channel]
        truth_frame = frame[[f"{NBODY}_{nom_var}", f"{NBODY}_{truth_var}"]].explode(
            f"{NBODY}_{nom_var}"
        )
        s1 = truth_frame.index.to_series()
        s2 = s1.groupby(s1).cumcount()
        truth_frame.index = [truth_frame.index, s2]
        truth_frame.dropna(inplace=True)  # many empty arrays in there
        target_pids = abs(target_pids)
        for i in range(0, len(target_pids), 1):

            particle_level = truth_frame[f"{NBODY}_{nom_var}"].xs(
                i, axis=0, level=1, drop_level=False
            )
            particle_level = particle_level.replace(
                {
                    20413.0: "D1(H)+",
                    43.0: "Xu0",
                    104124.0: "Lambda_c(2625)+",
                    44.0: "Xu+",
                    4432.0: "Omega_cc+",
                    4434.0: "Omega*_cc+",
                }
            )  # manually add particle that is not in the particle package
            for particle in particle_level.unique():
                try:
                    particle_level = particle_level.replace(
                        {particle: Particle.from_pdgid(particle).name}
                    )  # skip particles not in the package
                except Exception:  # for some reason ValueError does not catch all errors
                    particle_level = particle_level.replace(
                        {particle: str(particle)}
                    )  # count how many times a dodgy entry occurs instead of just pass. Errors may go unscrutinised otherwise.

            daughters = truth_frame[f"{NBODY}_{nom_var}"]
            truth = truth_frame[f"{NBODY}_{truth_var}"]
            selected_particle_level = (daughters[truth == True]).xs(
                i, axis=0, level=1, drop_level=False
            )

            selected_particle_level = selected_particle_level.replace(
                {
                    20413.0: "D1(H)+",
                    43.0: "Xu0",
                    104124.0: "Lambda_c(2625)+",
                    44.0: "Xu+",
                    4432.0: "Omega_cc+",
                    4434.0: "Omega*_cc+",
                }
            )  # manually add particle that is not in the particle package
            for particle in selected_particle_level.unique():
                try:
                    selected_particle_level = selected_particle_level.replace(
                        {particle: Particle.from_pdgid(particle).name}
                    )  # skip particles not in the package
                except Exception:
                    selected_particle_level = selected_particle_level.replace(
                        {particle: str(particle)}
                    )
                    print(c("WARNING: dodgy selected PID").yellow.on_red)
                    continue  # here we just flag this, but we don't expect anything

            fig, ax = plt.subplots(figsize=(30, 15))
            ax.bar(
                particle_level.value_counts(sort=False).index,
                particle_level.value_counts(sort=False).values,
                align="center",
                color="deeppink",
                alpha=0.7,
                label=f"Particle{i} in Sample",
            )
            ax.bar(
                selected_particle_level.value_counts(sort=False).index,
                selected_particle_level.value_counts(sort=False).values,
                align="center",
                facecolor=None,
                edgecolor="tab:blue",
                hatch="\\",
                fill=False,
                label=f"Truth Matched Particle{i}",
            )
            ax.set_title(r"LHCb Preliminary", loc="left", fontsize=30)
            plt.xticks(
                rotation=45,
                horizontalalignment="right",
                fontweight="light",
                fontsize="x-large",
            )
            plt.legend()

            # create mode-specific dir
            pathlib.Path(f"Plots/new_tuples/{NBODY}/{channel}/Truth_Matching/").mkdir(
                parents=True, exist_ok=True
            )
            plt.savefig(
                f"Plots/new_tuples/{NBODY}/{channel}/Truth_Matching/{NBODY}_check_truth_particle{i}.png"
            )
            ax.set_yscale("log")
            plt.savefig(
                f"Plots/new_tuples/{NBODY}/{channel}/Truth_Matching/{NBODY}_check_truth_particle{i}_log.png"
            )
            plt.close()

        return


if __name__ == "__main__":
    pass
    # load data
