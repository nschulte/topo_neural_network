import torch 
import numpy as np
import pandas as pd
import time
import pickle
from torch.utils.data import TensorDataset, DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn import functional as F
from torch.nn.functional import binary_cross_entropy_with_logits
from sklearn.metrics import roc_auc_score 
from monotonenorm import SigmaNet, GroupSort, direct_norm


new_path = "/ceph/users/nschulte/new_tuples_topo/TOPO_NEWMODES/"

torch.manual_seed(42)

if(torch.cuda.is_available()):
        device = torch.device("cuda")
        print("Running on cuda")
else:
        device = torch.device("cpu")
        print("Running on cpu")

print("Opening Pickles")
train_sample = pd.read_csv(new_path + 'TwoBody_Balanced_Train.csv')
test_sample = pd.read_csv(new_path + 'TwoBody_Balanced_Test.csv')

print("Sucessfully imported Dataframes")

features = [
    "TwoBody_DIRA",
    "TwoBody_OWNPV_DOCAMAX",
    "TwoBody_ENDVERTEX_CHI2",
    "TwoBody_ETA",
    "TwoBody_FDCHI2_OWNPV",
    "TwoBody_IPCHI2_OWNPV",
    "TwoBody_Mcorr",
    "TwoBody_PT",
    "TwoBody_P",
    "Track1_IPCHI2_OWNPV",
    "Track1_PT",
    "Track1_P",
]
event_ids = ['runNumber', 'eventNumber']
label_name = ['TwoBody_fromSignalB']
same_Bs = ['TwoBody_FromSameB']
multiplicity = ['TwoBody_nCharged', 'TwoBody_nNeutral']


print("Making Data to Tensors")
X_train = torch.tensor(train_sample[features].values.astype(np.float32))
y_train = torch.tensor(train_sample.label.values.astype(np.float32))[:, None]
X_test  = torch.tensor(test_sample[features].values.astype(np.float32))
y_test  = torch.tensor(test_sample.label.values.astype(np.float32))[:, None]

print("Move Data to Device")
X_train = X_train.to(device)
X_test  = X_test.to(device)
y_train = y_train.to(device)
y_test  = y_test.to(device)


def get_model(monotonic, config):
    def lipschitz_norm(module):
        #return module
        return direct_norm(
            module,  # the layer to constrain
            always_norm=True,
            kind = "one",  # |W|_1 constraint type
            alpha=config["LIP"] ** (1 / 4),  # norm of the layer (LIP ** (1/nlayers))
        )

    model = torch.nn.Sequential(
        lipschitz_norm(torch.nn.Linear(12, 32)),
        GroupSort(2),
        lipschitz_norm(torch.nn.Linear(32, 64)),
        GroupSort(2),
        lipschitz_norm(torch.nn.Linear(64, 32)),
        GroupSort(2),
        lipschitz_norm(torch.nn.Linear(32, 1)),
    )

    if monotonic:
        model = SigmaNet(
            model,
            sigma=config["LIP"],
            monotone_constraints=[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        )
    return model

import ray
from ray import tune
from torch.optim.lr_scheduler import ReduceLROnPlateau
from ray.tune.schedulers import ASHAScheduler


def train(model, optimizer, train_loader):
    total_batch = len(train_loader)
    model.train()
    loss_train = 0
    epoch_train_loss = 0
    
    for batch_idx, (data,target) in enumerate(train_loader):
        if batch_idx * len(data) > BATCH_SIZE:
            return
        optimizer.zero_grad()
        output = model(data)
        loss_train = binary_cross_entropy_with_logits(output, target)
        epoch_train_loss += loss_train
        loss_train.backward()
        print(f"The train loss is {loss_train}")
        optimizer.step()
        #auc_train = roc_auc_score(target.detach().numpy(), output.detach().numpy())
    mean_loss = (epoch_train_loss / total_batch)
    #mean_roc = (auc_train/ total_batch)
    #mean_acc = (np.mean((output.cpu().detach().numpy() > 0.5) == target.cpu().detach().numpy()))  
    
    return mean_loss

def test(model, test_loader):
    
    model.eval()
    correct = 0
    total = 0
    
    with torch.no_grad():
        for data, target in test_loader:
            prediction = model(data)
            loss_eval = binary_cross_entropy_with_logits(prediction, target)
            _, predicted = torch.max(prediction.data, 1)
            total += target.size(0)
            correct += (prediction == target).sum().item()
    #auc_test = roc_auc_score(target.cpu().detach().numpy(), prediction.cpu().detach().numpy())
    #ROC_Test_array.append(auc_test)
    acc = correct/total
       
    return acc, loss_eval


def train_test(config):
    train_data = TensorDataset(X_train, y_train)
    test_data = TensorDataset(X_test, y_test)
    train_loader = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=False) 
    test_loader = DataLoader(test_data, shuffle=False)
    
    model = get_model(monotonic, config).to(device)
    
    optimizer = torch.optim.Adam(model.parameters(), lr=0.00338098)
    
    Train_loss_array = []
    ROC_Train_array = []
    ACC_train = []
    Test_loss_array = []
    ROC_Test_array = []
    ACC_test = []
    
    for epoch in range(EPOCHS):
        mean_loss = train(model, optimizer, train_loader)
        test_acc, test_loss = test(model,test_loader)
        
        Train_loss_array.append(mean_loss)
        #ROC_Train_array.append(train_roc)
        #ACC_train.append(train_acc)
        Test_loss_array.append(test_loss)
        #ROC_Test_array.append(test_auc)
        ACC_test.append(test_acc)
        
        tune.report(mean_accuracy=test_acc, loss=test_loss)


space = {
    "LIP": tune.uniform(0, 10)
}

monotonic = True 
EPOCHS = 150
BATCH_SIZE = 500


analysis = tune.run(train_test, 
                    config = space,
                    scheduler = ASHAScheduler(metric="mean_accuracy", mode="max")
                   )

analysis_dfs = analysis.trial_dataframes
ax=None
for d in analysis_dfs.values():
    ax = d.mean_accuracy.plot(ax=ax, legend=False)
    ax.set_xlabel("Epochs")
    ax.set_ylabel("Mean Accuracy")
plt.show()

[d.mean_accuracy.plot() for d in analysis_dfs.values()]