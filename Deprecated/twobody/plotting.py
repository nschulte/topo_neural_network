import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sklearn.metrics
import pickle
import seaborn as sns
import itertools

new_path = "/ceph/users/nschulte/new_tuples_topo/TOPO_NEWMODES/"
print("Opening Pickle")
test_sample = pd.read_pickle(new_path + 'TwoBody_Train_Results.pkl')
train_preds = pd.read_pickle(new_path + 'TwoBody_Train_sample_predictions.pkl')


print("Sucessfully imported Dataframes")

Parameterfile = open(new_path + 'twobody_performance_parameters.npy', 'rb')
print("Unpacking Numpy Arrays")
params=np.load(Parameterfile, allow_pickle=True)
print("Unpacking complete")
Parameterfile.close()
print("Successfully loaded Parameterfile")

Train_loss_array = params[0]
Test_loss_array = params[1]
ROC_Train_array = params[2]
ROC_Test_array = params[3]
ACC_Test = params[4]
ACC_train = params[5]

all_features = [
    "TwoBody_DIRA",
    "TwoBody_OWNPV_DOCAMAX",
    "TwoBody_ENDVERTEX_CHI2",
    "TwoBody_ETA",
    "TwoBody_FDCHI2_OWNPV",
    "TwoBody_IPCHI2_OWNPV",
    "TwoBody_Mcorr",
    "TwoBody_PT",
    "TwoBody_P",
    "Track1_IPCHI2_OWNPV",
    "Track1_PT",
    "Track1_P",
]

train_features=[
  'min_PT_final_state_tracks',
  'sum_PT_final_state_tracks',
  'min_FS_IPCHI2_OWNPV',
  'max_FS_IPCHI2_OWNPV',
  'TwoBody_PT',
  'TwoBody_Mcorr',
  'TwoBody_ENDVERTEX_DOCAMAX',
  'TwoBody_FDCHI2_OWNPV',
  'TwoBody_ENDVERTEX_CHI2'
]

event_ids = ['runNumber', 'eventNumber']
label_name = ['TwoBody_fromSignalB']
same_Bs = ['TwoBody_FromSameB']
multiplicity = ['TwoBody_nCharged', 'TwoBody_nNeutral']

id_name_map = {
    12513020: '3MuNu',
    12165181: 'D0KsPi',
    12163451: 'DPi_KPiPi0',
    11264001: 'D-pi',
    12195032: 'DstD0',
    10000000: 'incB',
    12117015: 'K6Mu',
    11102005: 'Kpi',
    11102202: 'KstGamma',
    12165094: 'LcPPi',
    11104020: 'PhiKst',
    12195047: 'D0Ds',
    11198098: 'DDKPi',
    12265002: 'DPiPIPi',
    13164044: 'DsPi',
    11160001: 'DstTauNu',
    11146114: 'JpsiPhiKs',
    12103025: 'KPiPi',
    15576011: 'Lc2625MuNu',
    30000000: 'minbias',
    11512011: 'PiMuNu'
}

def plot_roc(truth, preds, outputpath=None):
    import sklearn.metrics
    roc_auc = sklearn.metrics.roc_auc_score(truth, preds)
    fpr, tpr, _ = sklearn.metrics.roc_curve(truth, preds)
    fig, ax = plt.subplots()
    ax.plot(fpr, tpr, label=f'ROC AUC: {roc_auc:.3f}', color = '#f781bf',linewidth=3.0)
    ax.plot([0, 1], [0, 1], color='grey', linestyle='--', label='Random choice', linewidth=2.0)
    ax.set_xlabel('False positive rate',fontsize=17)
    ax.set_ylabel('True positive rate',fontsize=17)
    ax.set_xlim(right=1)
    ax.set_ylim(bottom=0)
    ax.set_xticks(np.arange(0, 1, 0.05), minor=True)
    ax.set_yticks(np.arange(0, 1, 0.05), minor=True)
    ax.grid(linestyle='--')
    plt.xticks(fontsize=17)
    plt.yticks(fontsize=17)
    ax.legend(loc='lower right',fontsize=17)
    if outputpath:
        plt.savefig(outputpath + '.pdf',dpi=300, bbox_inches = "tight")
    plt.close()

def plot_loss(train_loss, test_loss, outputpath=None):
    fig, ax = plt.subplots()
    ax.plot(Train_loss_array, color="dodgerblue", label="Training", alpha=0.7, linewidth=2.0)
    ax.plot(Test_loss_array, color="deeppink", label="Testing", alpha=0.6, linewidth=2.0)
    ax.set_xlabel('Epoch',fontsize=17)
    ax.set_ylabel('Loss',fontsize=17)
    ax.legend(loc='best',fontsize=17)
    ax.grid(linestyle='--')
    plt.xticks(fontsize=17)
    plt.yticks(fontsize=17)
    if outputpath:
        #!mkdir outputpath
        plt.savefig(outputpath + 'Loss.pdf',dpi=300, bbox_inches = "tight")
    plt.close()

def plot_response(predictions, outputpath=None):
    train_signal_counts,train_signal_bin_edges, train_signal_patches = plt.hist(train_preds[train_preds.label == 1].preds_per_cand, alpha=0.7, bins=100, log=True, density=True)
    test_background_counts,train_background_bin_edges, train_background_patches = plt.hist(train_preds[train_preds.label == 0].preds_per_cand, alpha=0.7, bins=100, log=True, density=True)

    plt.clf()
    plt.hist(test_sample[test_sample.label == 1].preds_per_cand, color = '#f781bf', alpha=0.5, bins=100, log=True, label="Test Signal Prediction", density=True)
    plt.hist(test_sample[test_sample.label == 0].preds_per_cand, color = '#377eb8', alpha=0.6, bins=100, log=True, label="Test Background Prediction", density=True)
    plt.plot(train_signal_bin_edges[:-1]+ 0.5*(train_signal_bin_edges[1:] - train_signal_bin_edges[:-1]), train_signal_counts, '.', color = '#984ea3',label = "Train Signal Prediction", alpha=0.7)
    plt.plot(train_background_bin_edges[:-1]+ 0.5*(train_background_bin_edges[1:] - train_background_bin_edges[:-1]), test_background_counts, '.', color = 'black',label = "Train Background Prediction", alpha=0.5)
    plt.legend(loc='best', fontsize=12)
    plt.xlabel("MVA Response", fontsize=16)
    plt.ylabel("Rate", fontsize= 16)
    plt.xticks(fontsize=17)
    plt.yticks(fontsize=17)
    if outputpath:
        plt.savefig(outputpath + '.pdf',dpi=300, bbox_inches = "tight")
    plt.close()

main_path = "/ceph/users/nschulte/NN_plots/New_MC/"
print('plotting loss')
plot_loss(Train_loss_array, Test_loss_array, outputpath = main_path+"Loss/TwoBody/Twobody_Constrained")
print('plotting response')
plot_response(test_sample, outputpath=main_path+"Response/TwoBody/Twobody_Response_Constrained")

#per candidate
print('per candidate test set')
plot_roc(test_sample.label, test_sample.preds_per_cand,outputpath = main_path+"Performance/TwoBody/Twobody_overall_per_combination_constrained")

# per event
print('per event test set')
plot_roc(test_sample.groupby('unique_event').label.max(), test_sample.groupby('unique_event').preds_per_cand.max(),outputpath = main_path+"Performance/TwoBody/Twobody_overall_per_event_Constrained")


for name in test_sample.name.unique():
    try:
        print(name)
        pe_x = test_sample[(((test_sample.name == name) & (test_sample.event_label == 1)) | ((test_sample.name == 'minbias') & (test_sample.event_label == 0)))]
        #per event
        print('per event')
        plot_roc(pe_x.groupby('unique_event').label.max(), pe_x.groupby('unique_event').preds_per_cand.max(),outputpath =main_path+ "Performance/TwoBody/Twobody_"+name+"_per_event_Constrained")
    except:
        pass


for name in test_sample.name.unique():
    try:
        print(name)
        pe_x = test_sample[(((test_sample.name == name) & (test_sample.event_label == 1)) | ((test_sample.name == 'minbias') & (test_sample.event_label == 0)))]
        # per candidate
        print('per candidate')
        plot_roc(pe_x.label, pe_x.preds_per_cand,outputpath = main_path+"Performance/TwoBody/Twobody_"+name+"_per_combination_Constrained")
    except:
        pass

twobody = test_sample
twobody = twobody.set_index(event_ids)
name_map = dict(zip(twobody.name.unique(), range(len(twobody.name.unique()))))

import sklearn.metrics
twobody_auc_dict = {}
for name in twobody.name.unique():
    try:
        per_channel = twobody[(((twobody.name == name) & (twobody.label == 1)) | ((twobody.name == 'minbias') & (twobody.label == 0)))]
        roc_auc = sklearn.metrics.roc_auc_score(per_channel.label, per_channel.preds_per_cand)
        twobody_auc_dict[name] = roc_auc
    except:
        pass
twobody_auc_dict = dict(sorted(twobody_auc_dict.items(), key=lambda item: item[1], reverse = True))

cut_range = np.linspace(0, 1,501)


decay_map={'3MuNu':r'$B^+ \to \mu^+ \mu^- \mu^+ \nu_\mu$',
'D0KsPi':r'$B^+ \to \bar{D}^0 K_s^0 \pi^+$ ',
'DPi_KPiPi0':r'$B^+ \to \bar{D}^0  \pi^+$',
'D-pi':r'$B^0 \to D^- \pi^+ $',
'DstD0':r'$B^+ \to D^{\star +}\left(2010\right) \tilde{D}^0 $',
'K6Mu':r'$B^+ \to K^+ \mu^+ \mu^- \mu^+ \mu^- \mu^+ \mu^-$',
'KstGamma':r'$B^0 \to K^{\star 0} \gamma$',
'LcPPi':r'$B^+ \to \bar{\Lambda}_c^- p^+ \pi^+$ ',
'PhiKst':r'$B^0 \to \phi\left(1020\right)K^{\star 0}\left(892\right) $',
'D0Ds':r'$B^+ \to D^0 D_s^+$',
'DDKPi':r'$B^0 \to D^0 \tilde{D}^0 K^+ \pi^- $',
'DPiPIPi':r'$B^+ \to \tilde{D}^0 \pi^+ \pi^- \pi^+$',
'DsPi':r'$B_s^0 \to D_s^- \pi^+$',
'DstTauNu':r'$B^0 \to D^{\star -} \tau^+ \nu_\tau$',
'JpsiPhiKs':r'$B^0 \to J/\psi\left(1\mathrm{S}\right) K_{s}^0 \phi\left(1020\right)$',
'KPiPi':r'$B^+ \to K^+ \pi^+ \pi^-$',
'Lc2625MuNu':r'$\Lambda_b^0 \to \Lambda_c^+ \left(2625\right) \mu^- \bar{\nu}_\mu$' ,
'PiMuNu':r'$B^0 \to \pi^- \mu^+ \nu_\mu$' ,
'Kpi':r'$B^0 \to K^\pm \pi^\mp$',
'minbias':'Minimum Bias'}

twobody_tos_efficiency_overall =[(twobody[twobody.label == 1]['preds_per_cand'] > x).groupby(event_ids).max().mean() for x in cut_range]
twobody_tos_efficiency_overall_background =[(twobody[twobody.name =='minbias']['preds_per_cand'] > x).groupby(event_ids).max().mean() for x in cut_range]

fig, ax = plt.subplots()
palette = itertools.cycle(sns.color_palette("PuRd_r", 19))
for name in twobody_auc_dict.keys():
    if name != 'minbias':
        twobody_signal_channels = twobody[twobody.name ==name]
        twobody_tos = ([(twobody_signal_channels['preds_per_cand'] > x).groupby(event_ids).max().mean() for x in cut_range])
        ax.scatter(cut_range, twobody_tos, label = f"{decay_map[name]}", s=1, color=next(palette))
ax.scatter(cut_range, twobody_tos_efficiency_overall, label = "Overall", color="black", s=1)
ax.scatter(cut_range, twobody_tos_efficiency_overall_background, label ="Background", color="dodgerblue",s=1)
ax.set_xlabel('Cut Value',fontsize=17)
ax.set_ylabel('TOS Efficiency',fontsize=17)
ax.legend(loc='upper left', prop={'size': 9.9},ncol=2, bbox_to_anchor=(1.01, 1.03),fancybox=True,markerscale=4)
plt.title("TwoBody TOS Efficiency", fontsize=13)
csfont = {'fontname':'Times New Roman'}
plt.text(0.76, 0.99, 'LHCb Unofficial', {'size': 12}, **csfont)
plt.xticks(fontsize=17)
plt.yticks(fontsize=17)
plt.savefig(main_path+'Performance/TwoBody/Twobody_TOS_Eff_Cut_NN.pdf',dpi=300, bbox_inches = "tight")
plt.close()

cut_range = np.linspace(0, 1,501)

twobody_tos_efficiency_overall =[(twobody[twobody.label == 1]['preds_per_cand'] > x).groupby(event_ids).max().mean() for x in cut_range]

fig, ax = plt.subplots()
palette = itertools.cycle(sns.color_palette("PuRd_r", 19))
for name in twobody_auc_dict.keys():
    if name != 'minbias':
        twobody_signal_channels = twobody[twobody.name ==name]
        twobody_tos = ([(twobody_signal_channels['preds_per_cand'] > x).groupby(event_ids).max().mean() for x in cut_range])
        ax.scatter(cut_range, twobody_tos, label = f"{decay_map[name]}", s=5, color=next(palette))
ax.scatter(cut_range, twobody_tos_efficiency_overall, label = "Overall", color="black", s=5)
ax.set_xlabel('Cut Value',fontsize=17)
ax.set_ylabel('TOS Efficiency',fontsize=17)
plt.locator_params(axis='x', nbins=5)
ax.set_xlim(0.8,1.0)
ax.set_ylim(0.7,1.0)
ax.legend(loc='upper left', prop={'size': 10},ncol=2, bbox_to_anchor=(1.01, 1.03),fancybox=True,markerscale=2)
plt.title("TwoBody TOS Efficiency",fontsize=13)
csfont = {'fontname':'Times New Roman'}
plt.text(0.949, 0.98, 'LHCb Unofficial', {'size': 12}, **csfont)
plt.xticks(fontsize=17)
plt.yticks(fontsize=17)
plt.savefig(main_path+'Performance/TwoBody/Twobody_TOS_Eff_Cut_NN_Zoomed.pdf',dpi=300, bbox_inches = "tight")
plt.close()