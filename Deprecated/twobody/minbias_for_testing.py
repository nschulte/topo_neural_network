import pandas as pd
from sklearn.preprocessing import QuantileTransformer
import pickle
import time
import uproot


data_path = "/ceph/users/nschulte/new_tuples_topo/Dec21TopoTuples/"
new_path = "/ceph/users/nschulte/new_tuples_topo/TOPO_NEWMODES/"

features = [
    'TwoBody_ENDVERTEX_CHI2',
    'TwoBody_ENDVERTEX_DOCAMAX',
    'TwoBody_OWNPV_CHI2',
    'TwoBody_OWNPV_DOCAMAX',
    'TwoBody_IPCHI2_OWNPV',
    'TwoBody_FDCHI2_OWNPV',
    'TwoBody_P',
    'TwoBody_PT',
#    'TwoBody_FromSameB',
    'TwoBody_FromSameD',
#    'TwoBody_nNeutral',
#    'TwoBody_nCharged',
    'TwoBody_n_DAUGHTERS',
    'Track1_OWNPV_CHI2',
    'Track1_OWNPV_DOCAMAX',
    'Track1_IPCHI2_OWNPV',
    'Track1_ORIVX_CHI2',
    'Track1_ORIVX_DOCAMAX',
    'Track1_P',
    'Track1_PT',
    'Track2_OWNPV_CHI2',
    'Track2_OWNPV_DOCAMAX',
    'Track2_IPCHI2_OWNPV',
    'Track2_ORIVX_CHI2',
    'Track2_ORIVX_DOCAMAX',
    'Track2_P',
    'Track2_PT',
    'TwoBody_Mcorr',
    'TwoBody_DIRA',
    'TwoBody_ETA'
]
event_ids = ['runNumber', 'eventNumber']
label_name = ['TwoBody_fromSignalB']
same_Bs = ['TwoBody_FromSameB']
multiplicity = ['TwoBody_nCharged', 'TwoBody_nNeutral']

id_name_map = {
    30000000: 'minbias'
}

#Use this part if you need to upload at the data from Root File. Takes longer
def dataframe(path,name, columns=None, **kwargs):
        f = uproot.open(path)
        t = f['TwoBody/DecayTree']
        df = t.pandas.df(columns or '*', **kwargs)
        return df

all_data = pd.DataFrame(columns=features + ['label', 'name', 'EventInSequence'] + event_ids + same_Bs +multiplicity)
for id_,name in id_name_map.items():
        filename = str(name) + '.root'
        print(f'{name} started processing')
        #keep this commented out for now until I include the IncB sample
        #to_append = dataframe(data_path + filename,name, features + ['EventInSequence'] + event_ids + label_name)
        if (name != 'minbias' and name != 'incB'):
            to_append = dataframe(data_path + filename,name, features + ['EventInSequence'] + event_ids + label_name + same_Bs+multiplicity)
            to_append.rename(columns={'TwoBody_fromSignalB':'label'}, inplace = True)
            to_append['label'] = to_append['label'].astype(float)
            print(f'{name} finished processing')
        elif name == 'minbias':
            start_time = time.process_time()
            to_append = dataframe(data_path + filename,name, features + ['EventInSequence'] + event_ids + same_Bs+multiplicity)
            mid_time = time.process_time() - start_time 
            print(f"Time needed to open and append the Minbias {mid_time}")
            to_append['label'] = 0.0
            end_time = time.process_time() - mid_time
            print(f"Time to Append Label {end_time}")
            print(f'{name} finished processing')
        else:
            print(f'{name} not regarded for now')
            continue
        to_append['name'] = name
        all_data = all_data.append(to_append)

all_data.to_pickle(new_path + 'twobody_minbias_castboost_dataframe.pkl')