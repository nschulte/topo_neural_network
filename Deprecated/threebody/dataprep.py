import pandas as pd
from sklearn.preprocessing import QuantileTransformer
import pickle
import time
import uproot
import numpy as np
import math

data_path = "/ceph/users/nschulte/new_tuples_topo/Dec21TopoTuples/"
new_path = "/ceph/users/nschulte/new_tuples_topo/TOPO_NEWMODES/"

all_features = [
 'ThreeBody_ENDVERTEX_CHI2',
 'ThreeBody_ENDVERTEX_DOCAMAX',
 'ThreeBody_OWNPV_CHI2',
 'ThreeBody_OWNPV_DOCAMAX',
 'ThreeBody_IPCHI2_OWNPV',
 'ThreeBody_FDCHI2_OWNPV',
 'ThreeBody_P',
 'ThreeBody_PT',
 'TwoBody_ENDVERTEX_CHI2',
 'TwoBody_ENDVERTEX_DOCAMAX',
 'TwoBody_OWNPV_CHI2',
 'TwoBody_OWNPV_DOCAMAX',
 'TwoBody_IPCHI2_OWNPV',
 'TwoBody_FDCHI2_OWNPV',
 'TwoBody_ORIVX_CHI2',
 'TwoBody_ORIVX_DOCAMAX',
 'TwoBody_FDCHI2_ORIVX',
 'TwoBody_P',
 'TwoBody_PT',
 'Track1_OWNPV_CHI2',
 'Track1_OWNPV_DOCAMAX',
 'Track1_IPCHI2_OWNPV',
 'Track1_ORIVX_CHI2',
 'Track1_ORIVX_DOCAMAX',
 'Track1_P',
 'Track1_PT',
 'Track2_OWNPV_CHI2',
 'Track2_OWNPV_DOCAMAX',
 'Track2_IPCHI2_OWNPV',
 'Track2_ORIVX_CHI2',
 'Track2_ORIVX_DOCAMAX',
 'Track2_P',
 'Track2_PT',
 'TrackB_OWNPV_CHI2',
 'TrackB_OWNPV_DOCAMAX',
 'TrackB_IPCHI2_OWNPV',
 'TrackB_ORIVX_CHI2',
 'TrackB_ORIVX_DOCAMAX',
 'TrackB_P',
 'TrackB_PT',
 'ThreeBody_Mcorr',
 'TwoBody_Mcorr',
 'ThreeBody_DIRA',
 'TwoBody_DIRA',
 'ThreeBody_ETA',
 'TwoBody_ETA'
 ]

train_features=[
  "min_FS_IPCHI2_OWNPV",
  "max_FS_IPCHI2_OWNPV",
  "sum_PT_TRACK12",
  "min_PT_TRACK12",
  "sum_PT_final_state_tracks",
  "min_PT_final_state_tracks",
  "TwoBody_ENDVERTEX_DOCAMAX",
  "ThreeBody_ENDVERTEX_DOCAMAX",
  "TwoBody_FDCHI2_OWNPV",
  "TwoBody_IPCHI2_OWNPV",
  "ThreeBody_FDCHI2_OWNPV",
  "TwoBody_Mcorr",
  "ThreeBody_Mcorr",
  "ThreeBody_PT",
  "TwoBody_PT",
  "TwoBody_ENDVERTEX_CHI2",
  "ThreeBody_ENDVERTEX_CHI2"
]

event_ids = ['runNumber', 'eventNumber']
label_name = ['ThreeBody_fromSignalB']
same_Bs = ['ThreeBody_FromSameB','TwoBody_FromSameB']
same_Ds = ['ThreeBody_FromSameD', 'TwoBody_FromSameD']
multiplicity = ['ThreeBody_nCharged', 'ThreeBody_nNeutral', 'TwoBody_nCharged', 'TwoBody_nNeutral']

id_name_map = {
    12513020: '3MuNu',
    12165181: 'D0KsPi',
    12163451: 'DPi_KPiPi0',
    11264001: 'D-pi',
    12195032: 'DstD0',
    10000000: 'incB',
    12117015: 'K6Mu',
    11102005: 'Kpi',
    11102202: 'KstGamma',
    12165094: 'LcPPi',
    11104020: 'PhiKst',
    12195047: 'D0Ds',
    11198098: 'DDKPi',
    12265002: 'DPiPIPi',
    13164044: 'DsPi',
    11160001: 'DstTauNu',
    11146114: 'JpsiPhiKs',
    12103025: 'KPiPi',
    15576011: 'Lc2625MuNu',
    30000000: 'minbias',
    11512011: 'PiMuNu'
}

def dataframe(path,name, columns=None, **kwargs):
    f = uproot.open(path)
    if name != 'minbias':
        t = f['ThreeBody']
    else:
        t = f['ThreeBody/DecayTree']
    df = t.pandas.df(columns or '*', **kwargs)
    return df

#all_data = pd.DataFrame(columns=all_features + ['label', 'name', 'EventInSequence'] + event_ids + same_Bs + multiplicity+same_Ds)
#for id_,name in id_name_map.items():
#        filename = str(name) + '.root'
#        print(f'{name} started processing')
#        #keep this commented out for now until I include the IncB sample
#        #to_append = dataframe(data_path + filename,name, all_features + ['EventInSequence'] + event_ids + label_name)
#        if (name != 'minbias' and name != 'incB'):
#            to_append = dataframe(data_path + filename,name, all_features + ['EventInSequence'] + event_ids + label_name + same_Bs + multiplicity+same_Ds)
#            to_append.rename(columns={'ThreeBody_fromSignalB':'label'}, inplace = True)
#            to_append['label'] = to_append['label'].astype(float)
#            print(f'{name} finished processing')
#        elif name == 'minbias':
#            start_time = time.process_time()
#            to_append = dataframe(data_path + filename,name, all_features + ['EventInSequence'] + event_ids + same_Bs + multiplicity + same_Ds)[:1000000]
#            mid_time = time.process_time() - start_time
#            print(f"Time needed to open and append the Minbias {mid_time}")
#            to_append['label'] = 0.0
#            end_time = time.process_time() - mid_time
#            print(f"Time to Append Label {end_time}")
#            print(f'{name} finished processing')
#        else:
#            print(f'{name} not regarded for now')
#            continue
#        to_append['name'] = name
#        all_data = all_data.append(to_append)
#
#all_data.to_pickle(new_path + 'threebody_catboost_dataframe.pkl')

#Read all data from pickle to make it faster
all_data = pd.read_pickle(new_path + 'threebody_catboost_dataframe.pkl')

#Replace label with True and False
replace_data = all_data["ThreeBody_FromSameB"].replace({-1:False, 1:True}, inplace = True)

#Group by unique events and export that information to the dataframe
ue = all_data.groupby(event_ids)[event_ids].max()
ue['unique_event'] = range(len(ue))
all_data = all_data.join(ue['unique_event'], on=event_ids)
assert(all(all_data.groupby(event_ids).unique_event.nunique() == 1))

#Check the amount of events and final tracks in the minbias sample
print("Unique Events in Minbias:", all_data[all_data.name == "minbias"].unique_event.nunique())
print("Tracks in Minbias:", len(all_data[all_data.name == "minbias"]))

#Remove the events that are from a Beauty decay from the minbias
SameBs_minbias = all_data[all_data.name == "minbias"].groupby('unique_event').ThreeBody_FromSameB.max().astype(bool)
SameB_tracks = all_data[all_data.name == "minbias"].ThreeBody_FromSameB.astype(bool)
SameBs_minbias = SameBs_minbias[SameBs_minbias].index
SameB_tracks = SameB_tracks[SameB_tracks]
all_data = all_data.set_index('unique_event').drop(SameBs_minbias).reset_index()
print("Unique Events with FromSameB tracks in MinBias:", len(SameBs_minbias))
print("Number of Tracks FromSameB:", len(SameB_tracks))

#This should be zero if the removal worked
check_for_rest = all_data[all_data.name == "minbias"]. ThreeBody_FromSameB.sum()
print(f"Signal in Minbias Left: {check_for_rest}")

 #Find Signal in Input Sample and Compare with "FromSameB"
for name in all_data.name.unique():
    filtered = all_data[all_data.name == name]
    signal = filtered[filtered.label == 1]
    fromBs = filtered[filtered.ThreeBody_FromSameB == 1]
    both = filtered[(filtered.ThreeBody_FromSameB == 1) & (filtered.label == 1)]
    print(f"{name} \n Amount of Signal Label: {len(signal)} \n Amount of FromSameBs: {len(fromBs)}, \n Both Applied: {len(both)}")

#Check for Events in Minbias after removal
print("Unique Events in Minbias:", all_data[all_data.name == "minbias"].unique_event.nunique())
print("Tracks in Minbias:", len(all_data[all_data.name == "minbias"]))

#Check amount of signal Events in each Sample
for name in all_data.name.unique():
    amount = all_data[all_data.name == name].unique_event.nunique()
    signal = all_data[(all_data.name == name) & (all_data.label == 1)]
    print(f"Number of events in sample {name}: {amount} \nNumber of signal tracks in sample {name}: {len(signal)}")

#Asign an event label
all_data = all_data.join(all_data.groupby('unique_event').label.max().rename('event_label'), on =['unique_event'])

#Not all samples are needed for the threebody training
drop_list = {
    12513020: '3MuNu',
    12117015: 'K6Mu',
    11102005: 'Kpi',
    11102202: 'KstGamma',
    12103025: 'KPiPi',
    11512011: 'PiMuNu'
}
for id_, name in drop_list.items():
    unwanted_samples = all_data[all_data.name == name].index
    all_data = all_data[~all_data.index.isin(unwanted_samples)]

#Make Balanced Train Sample
train_sample = []
for name in all_data.name.unique():
    if name != "minbias":
        signal = all_data[(all_data.name == name) & (all_data.label == 1)][:27150]
        train_sample.append(signal)
    else:
        bkg = all_data[(all_data.name == "minbias")& (all_data.label == 0)][:352950]
        train_sample.append(bkg)
train_sample = pd.concat(train_sample).sample(frac=1)

#Remove Train Events to make sure no rest event is landing in the testing
events_in_training = train_sample.unique_event.unique()
without_events_in_training = all_data[~all_data['unique_event'].isin(events_in_training)]

#Assemble Test Sample, also Balanced
test_sample = []
for name in all_data.name.unique():
    if name != "minbias":
        rest_signal = without_events_in_training[(without_events_in_training.name == name)
                                                 & (without_events_in_training.label == 1)][:6750]
        test_sample.append(rest_signal)
    else:
        test_bkg = without_events_in_training[(without_events_in_training.name == "minbias")
                                             & (without_events_in_training.label == 0)][:87750]
        test_sample.append(test_bkg)
test_sample = pd.concat(test_sample).sample(frac=1)


#Make Sure Train and Test Sample have the Same amount of Final State Tracks in each Signal Sample
for name in train_sample.name.unique():
    if name != "minbias":
        print(f'number of candidates in training set for {name}', train_sample[train_sample.name == name].label.sum())
        print(f'number of candidates in test set for {name}', test_sample[test_sample.name == name].label.sum())


_gev_vars = [
    "sum_PT_final_state_tracks",
    "min_PT_final_state_tracks",
    "max_PT_final_state_tracks",
    "sum_PT_TRACK12",
    "min_PT_TRACK12",
    "max_PT_TRACK12",
    "TwoBody_PT",
    "TwoBody_Mcorr",
    "ThreeBody_Mcorr",
    "ThreeBody_PT"
    ]

_log_vars = [
    "min_FS_IPCHI2_OWNPV",
    "max_FS_IPCHI2_OWNPV",
    "TwoBody_FDCHI2_OWNPV",
    "ThreeBody_FDCHI2_OWNPV",
    "TwoBody_IPCHI2_OWNPV",
    "ThreeBody_IPCHI2_OWNPV",
    "ThreeBody_ENDVERTEX_CHI2", # the 3body is O(100), hence log to map to range ~1
    ]

# generate missing features, before preprocessing
for df in [train_sample, test_sample]:

    df["min_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV","TrackB_IPCHI2_OWNPV"]].min(axis=1)
    df["max_FS_IPCHI2_OWNPV"] = df[["Track1_IPCHI2_OWNPV","Track2_IPCHI2_OWNPV","TrackB_IPCHI2_OWNPV"]].max(axis=1)
    assert(df.min_FS_IPCHI2_OWNPV.all()<=df.max_FS_IPCHI2_OWNPV.all()) # sanity check

    # seems to be missing from the balanced samples, need to benchmark against the max
    df['sum_PT_TRACK12'] = df[['Track1_PT', 'Track2_PT']].sum(axis=1)
    df['max_PT_TRACK12'] = df[['Track1_PT', 'Track2_PT']].max(axis=1)
    df["min_PT_TRACK12"] = df[["Track1_PT","Track2_PT"]].min(axis=1)
    df["min_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].min(axis=1)
    df["max_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].max(axis=1)
    df["sum_PT_final_state_tracks"] = df[["Track1_PT", "Track2_PT", "TrackB_PT"]].sum(axis=1)


    for g in _gev_vars:
        df[g] /= 1000.
    for l in _log_vars:
        df.loc[df[l] <= 0, l] = 1e-5
        assert(df[l].all()>0)
        df[l] = np.log(df[l].to_numpy())
    df["TwoBody_ENDVERTEX_DOCAMAX"] *= 10.
    df["ThreeBody_ENDVERTEX_DOCAMAX"] *= 10.

#Scale Data
print("START SCALING")
def constrain(
    feat:"feature/column/branch",
    n_sigma=5, #"+/- n std dev of retention window",
)->"dataframe[train_features]":
    """retain if falling within a window about mean, post scaling / logging if needed"""

    mean = np.mean(feat)
    std  = np.std(feat)

    retained = feat[(feat>=mean-n_sigma*std) & (feat<=mean+n_sigma*std)]
    feat[(feat<mean-n_sigma*std)] = np.min(retained)
    feat[(feat>mean+n_sigma*std)] = np.max(retained)

    return feat

tn_scaled = train_sample
tt_scaled = test_sample

print("NOTE:running preprocessing with RENTION WINDOW ON")
train_sample[train_features] = tn_scaled[train_features].apply(lambda x: constrain(x), axis=0)
test_sample[train_features] = tt_scaled[train_features].apply(lambda x: constrain(x), axis=0)


#Save Samples
train_sample.to_pickle(new_path + 'ThreeBody_Balanced_Train_MScaled.pkl')
test_sample.to_pickle(new_path + 'ThreeBody_Balanced_Test_MScaled.pkl')
