import torch
import numpy as np
import pandas as pd
import time
import pickle
from torch.utils.data import TensorDataset, DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn import functional as F
from torch.nn.functional import binary_cross_entropy_with_logits
from sklearn.metrics import roc_auc_score
from monotonenorm import SigmaNet, GroupSort, direct_norm


new_path = "/ceph/users/nschulte/new_tuples_topo/TOPO_NEWMODES/"

torch.manual_seed(42)

if(torch.cuda.is_available()):
        device = torch.device("cuda")
        print("Running on cuda")
else:
        device = torch.device("cpu")
        print("Running on cpu")

print("Opening Pickles")
train_sample = pd.read_pickle(new_path + 'ThreeBody_Balanced_Train_MScaled.pkl')
test_sample = pd.read_pickle(new_path + 'ThreeBody_Balanced_Test_MScaled.pkl')

print("Sucessfully imported Dataframes")

features=[
  "min_FS_IPCHI2_OWNPV",
  "max_FS_IPCHI2_OWNPV",
  "sum_PT_TRACK12",
  "min_PT_TRACK12",
  "sum_PT_final_state_tracks",
  "min_PT_final_state_tracks",
  "TwoBody_ENDVERTEX_DOCAMAX",
  "ThreeBody_ENDVERTEX_DOCAMAX",
  "TwoBody_FDCHI2_OWNPV",
  "TwoBody_IPCHI2_OWNPV",
  "ThreeBody_FDCHI2_OWNPV",
  "TwoBody_Mcorr",
  "ThreeBody_Mcorr",
  "ThreeBody_PT",
  "TwoBody_PT",
  "TwoBody_ENDVERTEX_CHI2",
  "ThreeBody_ENDVERTEX_CHI2"
]

event_ids = ['runNumber', 'eventNumber']
label_name = ['ThreeBody_fromSignalB']
same_Bs = ['ThreeBody_FromSameB']
multiplicity = ['ThreeBody_nCharged', 'ThreeBody_nNeutral', 'TwoBody_nCharged', 'TwoBody_nNeutral']

print("Making Data to Tensors")
X_train = torch.tensor(train_sample[features].values.astype(np.float32))
y_train = torch.tensor(train_sample.label.values.astype(np.float32))[:, None]
X_test  = torch.tensor(test_sample[features].values.astype(np.float32))
y_test  = torch.tensor(test_sample.label.values.astype(np.float32))[:, None]

print("Move Data to Device")
X_train = X_train.to(device)
X_test  = X_test.to(device)
y_train = y_train.to(device)
y_test  = y_test.to(device)

def get_model(monotonic):
    def lipschitz_norm(module):
        return direct_norm(
            module,  # the layer to constrain
            always_norm=False,
            kind = "one",  # |W|_1 constraint type
            alpha=LIP ** (1 / 4),  # norm of the layer (LIP ** (1/nlayers))
        )

    model = torch.nn.Sequential(
        lipschitz_norm(torch.nn.Linear(len(features), 32)),
        GroupSort(2),
        lipschitz_norm(torch.nn.Linear(32, 64)),
        GroupSort(2),
        lipschitz_norm(torch.nn.Linear(64, 32)),
        GroupSort(2),
        lipschitz_norm(torch.nn.Linear(32, 1)),
    )

    if monotonic:
        model = SigmaNet(
            model,
            sigma=LIP,
            monotone_constraints=[1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0],
        )
    return model

monotonic = True
LIP = 1.75
EPOCHS = 120
BATCH_SIZE = 15_000

print("Move Model to Device")
model = get_model(monotonic).to(device)

train_data = TensorDataset(X_train, y_train)
train_loader = DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=False)
optimizer = torch.optim.Adam(model.parameters(), lr=0.00338098)
scheduler = ReduceLROnPlateau(optimizer, 'min', patience=10)

Train_loss_array = []
Test_loss_array = []
ROC_Train_array = []
ROC_Test_array = []
ACC_Test = []
ACC_train = []

with open('logfile.txt', 'w') as logfile:
    logfile.seek(0)

    print("Start Training")
    start = time.time()
    for i in range(EPOCHS):
        epoch_train_loss = 0
        epoch_auc_train = 0
        total_batch = len(train_loader)
        print(f"Epoch Number {i} ---------------------------------------------------", file=logfile)
        model.train()
        for data, target in train_loader:
            if device == "cuda":
                torch.cuda.synchronize()
            optimizer.zero_grad()
            output = model(data)
            if device == "cuda":
                torch.cuda.synchronize()
            loss_train = binary_cross_entropy_with_logits(output,target)
            loss_train.backward()
            epoch_train_loss += loss_train
            auc_train = roc_auc_score(target.cpu().detach().numpy(), output.cpu().detach().numpy())
            epoch_auc_train += auc_train
            epoch_time = time.time() - start
            curr_lr = optimizer.param_groups[0]['lr']
            optimizer.step()
            print(f"{i}:  AUC Score:{auc_train}   Loss: {loss_train}, time needed {epoch_time}, LR: {curr_lr}", file=logfile)
        scheduler.step(loss_train)
        Train_loss_array.append(epoch_train_loss.cpu().detach().numpy() / total_batch)
        ROC_Train_array.append(epoch_auc_train/ total_batch)
        acc_train = np.mean((output.cpu().detach().numpy() > 0.5) == target.cpu().detach().numpy())
        print(f"Epoche {i} Mean AUC: {epoch_auc_train/ total_batch}")
        ACC_train.append(acc_train)


        model.eval()
        epoch_eval_loss = 0
        with torch.no_grad():
            prediction = model(X_test)
            train_predictions = model(X_train)
            truth = y_test
            loss_eval = binary_cross_entropy_with_logits(prediction,truth)
            epoch_eval_loss += loss_eval
            auc_test = roc_auc_score(truth.cpu().detach().numpy(), prediction.cpu().detach().numpy())
            acc_test = np.mean((prediction.cpu().detach().numpy() > 0.5) == truth.cpu().detach().numpy())
        Test_loss_array.append(epoch_eval_loss.cpu().detach().numpy())
        ROC_Test_array.append(auc_test)
        ACC_Test.append(acc_test)

end = time.time() - start
print(f"Total running time: {end}")

scaled_predictions = torch.sigmoid(prediction)
scaled_train_predictions = torch.sigmoid(train_predictions)

predictions = np.zeros(len(test_sample))
predictions = scaled_predictions.cpu().detach().numpy()
test_preds = test_sample
test_preds['preds_per_cand'] = predictions

train_predictions = np.zeros(len(train_sample))
train_predictions = scaled_train_predictions.cpu().detach().numpy()
train_preds = train_sample
train_preds['preds_per_cand'] = train_predictions


print("Move Testframe to Pickle")
test_preds.to_pickle(new_path + 'ThreeBody_Train_Results.pkl')
train_preds.to_pickle(new_path + 'ThreeBody_Train_sample_predictions.pkl')


performance_params = [
    Train_loss_array,
    Test_loss_array,
    ROC_Train_array,
    ROC_Test_array,
    ACC_Test,
    ACC_train,
]
print("Dumping Parameters to Numpy")
with open(new_path + 'threebody_performance_parameters.npy', 'wb') as outputfile:
    np.save(outputfile, performance_params)
