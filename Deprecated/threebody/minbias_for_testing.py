import pandas as pd
from sklearn.preprocessing import QuantileTransformer
import pickle
import time
import uproot
import numpy as np


data_path = "/ceph/users/nschulte/new_tuples_topo/Dec21TopoTuples/"
new_path = "/ceph/users/nschulte/new_tuples_topo/TOPO_NEWMODES/"
model_path = "/ceph/users/nschulte/topological-turbo-sp/analysis/TOPO_Training_Evaluation/models/new_mc/"

from catboost import CatBoostClassifier
threebody_clf=CatBoostClassifier()
threebody_clf = threebody_clf.load_model(model_path + 'threebody_catboost_model')

features = [
 'ThreeBody_FDCHI2_OWNPV',
 'TwoBody_ENDVERTEX_DOCAMAX',
 'TwoBody_IPCHI2_OWNPV',
 'TwoBody_FDCHI2_OWNPV',
 'Track1_OWNPV_CHI2',
 'Track1_IPCHI2_OWNPV',
 'Track1_PT',
 'Track2_IPCHI2_OWNPV',
 'Track2_ORIVX_CHI2',
 'Track2_PT',
 'TrackB_OWNPV_CHI2',
 'TrackB_IPCHI2_OWNPV',
 'TrackB_PT',
 'ThreeBody_Mcorr',
 'TwoBody_Mcorr',
 'TwoBody_FromSameB',
 'ThreeBody_IPCHI2_OWNPV',
 'Track2_OWNPV_CHI2',
 'TwoBody_ENDVERTEX_CHI2',
 'ThreeBody_ENDVERTEX_CHI2',
]
train_features=[
  'Track1_logIPCHI2_OWNPV',
  'Track2_logIPCHI2_OWNPV',
  'TrackB_logIPCHI2_OWNPV',
  'ThreeBody_Mcorr',
  'sum_PT_all_final_state_tracks', # track1, track2, trackB
  'max_PT_all_final_state_tracks', # track1, track2, trackB
  'TwoBody_FDCHI2_OWNPV',
  'sum_PT_TRACK12', # track1, track2
  'max_PT_TRACK12', # track1, track2
  'TwoBody_ENDVERTEX_DOCAMAX',
  'Track1_OWNPV_CHI2',
  'TrackB_OWNPV_CHI2',
  'TwoBody_Mcorr',
  'Track2_OWNPV_CHI2',
  'ThreeBody_FDCHI2_OWNPV',
  'ThreeBody_IPCHI2_OWNPV',
  'TwoBody_IPCHI2_OWNPV',
  'ThreeBody_ENDVERTEX_CHI2',
]
event_ids = ['runNumber', 'eventNumber']
label_name = ['ThreeBody_fromSignalB']
same_Bs = ['ThreeBody_FromSameB']
multiplicity = ['ThreeBody_nCharged', 'ThreeBody_nNeutral', 'TwoBody_nCharged', 'TwoBody_nNeutral']
same_Ds = ['ThreeBody_FromSameD', 'TwoBody_FromSameD']

id_name_map = {
    30000000: 'minbias'
}

def dataframe(path,name, columns=None, **kwargs):
    f = uproot.open(path)
    t = f['ThreeBody/DecayTree']
    df = t.pandas.df(columns or '*', **kwargs)
    return df

all_data = pd.DataFrame(columns=features + ['label', 'name', 'EventInSequence'] + event_ids + same_Bs + multiplicity + same_Ds)
for id_,name in id_name_map.items():
        filename = str(name) + '.root'
        print(f'{name} started processing')
        #keep this commented out for now until I include the IncB sample
        #to_append = dataframe(data_path + filename,name, features + ['EventInSequence'] + event_ids + label_name)
        if (name != 'minbias' and name != 'incB'):
            to_append = dataframe(data_path + filename,name, features + ['EventInSequence'] + event_ids + label_name + same_Bs + multiplicity + same_Ds)
            to_append.rename(columns={'ThreeBody_fromSignalB':'label'}, inplace = True)
            to_append['label'] = to_append['label'].astype(float)
            print(f'{name} finished processing')
        elif name == 'minbias':
            start_time = time.process_time()
            to_append = dataframe(data_path + filename,name, features + ['EventInSequence'] + event_ids + same_Bs + multiplicity + same_Ds)[:153688834]
            mid_time = time.process_time() - start_time 
            print(f"Time needed to open and append the Minbias {mid_time}")
            to_append['label'] = 0.0
            end_time = time.process_time() - mid_time
            print(f"Time to Append Label {end_time}")
            print(f'{name} finished processing')
        else:
            print(f'{name} not regarded for now')
            continue
        to_append['name'] = name
        all_data = all_data.append(to_append)



#all_data.to_csv(new_path + 'threebody_minbias_catboost_dataframe.csv')

ue = all_data.groupby(event_ids)[event_ids].max()
ue['unique_event'] = range(len(ue))
all_data = all_data.join(ue['unique_event'], on=event_ids)
assert(all(all_data.groupby(event_ids).unique_event.nunique() == 1))

replace_data = all_data["ThreeBody_FromSameB"].replace({-1:False, 1:True}, inplace = True)

print("Unique Events in Minbias:", all_data.unique_event.nunique())
print("Combinations in Minbias:", len(all_data))

all_data['Track1_logIPCHI2_OWNPV'] = np.log(all_data[['Track1_IPCHI2_OWNPV']])
all_data['Track2_logIPCHI2_OWNPV'] = np.log(all_data[['Track2_IPCHI2_OWNPV']])
all_data['TrackB_logIPCHI2_OWNPV'] = np.log(all_data[['TrackB_IPCHI2_OWNPV']])
all_data['sum_PT_all_final_state_tracks'] = all_data[['Track1_PT', 'Track2_PT','TrackB_PT']].sum(axis=1)
all_data['max_PT_all_final_state_tracks'] = all_data[['Track1_PT', 'Track2_PT','TrackB_PT']].max(axis=1)
all_data['sum_PT_TRACK12'] = all_data[['Track1_PT', 'Track2_PT']].sum(axis=1)
all_data['max_PT_TRACK12'] = all_data[['Track1_PT', 'Track2_PT']].max(axis=1)

threebody_minbias_prediction = threebody_clf.predict_proba(all_data[train_features])

all_data['preds_per_cand'] = threebody_minbias_prediction[:,1]

all_data.to_pickle(data_path + 'threebody_whole_minbias_predicted_first_part.pkl')