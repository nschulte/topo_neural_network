import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

infile = r"/interactive_storage/nschulte/stack/Ratesestimates/27July/Rates.log"

with open(infile) as file:
    file = file.readlines()[9:] #skip header lines

def extract_numbers(s): #taken from stackoverflow to avoid annoying regexing
    numbers = []
    current = ''
    for c in s.lower() + '!':
        if (c.isdigit() or
            (c == 'e' and ('e' not in current) and (current not in ['', '.', '-', '-.'])) or
            (c == '.' and ('e' not in current) and ('.' not in current)) or
            (c == '+' and current.endswith('e')) or
            (c == '-' and ((current == '') or current.endswith('e')))):
            current += c
        else:
            if current not in ['', '.', '-', '-.']:
                if current.endswith('e'):
                    current = current[:-1]
                elif current.endswith('e-') or current.endswith('e+'):
                    current = current[:-2]
                numbers.append(current)
            if c == '.' or c == '-':
                current = c
            else:
                current = ''

    # Convert from strings to actual python numbers.
    numbers = [float(t) if ('.' in t or 'e' in t) else int(t) for t in numbers]

    return numbers

def convert_numbers(file):
    values_in_line = []

    for line in file:
        s = extract_numbers(line)
        indices_to_remove = {0,5,6} #remove the 2 from HLT2, and exclusive values which are 0 by default
        s = [v for i, v in enumerate(s) if i not in indices_to_remove]
        if (len(s) == 4): #skip empty arrays
            s[0] = float('0.' + str(s[0]))
            s[1] = float('0.' + str(s[1]))
            values_in_line.append(s)
        else:
             continue

    df = pd.DataFrame(values_in_line)
    df.columns = ['TwoBody_Cut', 'ThreeBody_Cut', 'Inclusive_Rate', 'Sigma_rate']
    print(df['TwoBody_Cut'][0])
    return df

def plot_rates():
    df = convert_numbers(file)

    rates = df.pivot(index='TwoBody_Cut', columns='ThreeBody_Cut', values='Inclusive_Rate')

    fig, ax = plt.subplots(figsize=(9,7)) #super ugly at this point, needs fixing
    ax = sns.heatmap(rates, annot=False,  fmt='.4f', cmap = "PuRd")
    ax.collections[0].colorbar.set_label("Rate [kHz]", fontsize=15)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.set_xlabel("ThreeBody MVA Cut", fontsize=16)
    ax.set_ylabel("TwoBody MVA Cut", fontsize=16)
    fig.set_tight_layout(True)
    plt.show()



if __name__ == '__main__':
    plot_rates()
#what is left in the array is now:
#TwoBody Cut, Threebody Cut, Rate in kHz, Error on the Rate in kHz
