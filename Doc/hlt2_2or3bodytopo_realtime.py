###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using real time reco. Produces hlt2_2or3bodytopo_realtime.mdf

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_2or3bodytopo_realtime.py
"""
from Moore import options, run_moore
from Moore.tcks import dump_hlt2_configuration
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from PRConfig.FilesFromDirac import get_access_urls
from Hlt2Conf.lines.topological_b import all_lines

# input_files = [
# 	'root://x509up_u5240@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00152976/0000/00152976_00000029_1.xdigi'
# ]

options.set_input_and_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')
options.input_raw_format = 4.3

# options.input_files = input_files
# options.input_type = 'ROOT'
# options.input_raw_format = 0.5
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

options.evt_max = 1000
# options.simulation = True
# options.data_type = 'Upgrade'
# options.dddb_tag = 'dddb-20210617'
# options.conddb_tag = 'sim-20210617-vc-md100'

options.output_file = 'hlt2_2or3bodytopo_realtime.mdf'
options.output_type = 'MDF'

from RecoConf.decoders import default_ft_decoding_version
ft_decoding_version = 6  #4,6
default_ft_decoding_version.global_bind(value=ft_decoding_version)


def make_lines():
    return [x(persistreco=False) for x in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
dump_hlt2_configuration(config, "hlt2_2or3bodytopo_realtime.tck.json")
