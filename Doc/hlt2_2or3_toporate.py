###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from HltEfficiencyChecker.config import run_moore_with_tuples
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom

options.set_input_and_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')

options.input_raw_format = 4.3
options.evt_max = 1000

options.ntuple_file = "Topo_RatesTuples_2707.root"

from RecoConf.hlt1_tracking import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=2)

with reconstruction.bind(from_file=False):
    run_moore_with_tuples(
        options, False, public_tools=[stateProvider_with_simplified_geom()])