the __init__ file contains the topo line the way it is currently implemented on our Topo_Neural_Net branch on Moore. But it has the addition of the Loop over various MVA Cut combination lines in the end (which is never to be pushed on Moore due to resource intensive calculations!). This loop needs to be added to the init file in moore before running the moore analysis script.

The hlt2_2or3body_realtime file needs to be adjusted with the right input (an up to date minbias which is hlt1 filtered) but whats important is the all_lines part and how it is called (x(persistreco=True) for x in all_lines.values()). But this file is not neccessary for the calculation of the rates. 

The hlt2_2or3_toporate and topoline file need to be added to the MooreAnalysis folder ```HltEfficiencyChecker/options/```.
Then you run everything with 

```
MooreAnalysis/run gaudirun.py MooreAnalysis/HltEfficiencyChecker/options/hlt2_2or3_topoline.py MooreAnalysis/HltEfficiencyChecker/options/hlt2_2or3_toporate.py
```

and then

```
MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_calculate_rates.py Topo_RatesTuples_2707.root --using-hlt1-filtered-MC --json Hlt2_rates.json > Rates.log
``` 
You need to save the output in a log file in order to be able to run 

https://gitlab.cern.ch/nschulte/topo_neural_network/-/blob/further_dev/Line_Evaluation/export_rates.py

For further information see: https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hltefficiencychecker.html
